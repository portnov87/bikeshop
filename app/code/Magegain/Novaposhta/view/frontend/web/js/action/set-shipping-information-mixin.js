/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
            shippingAddress['extension_attributes']['carrier_department'] = $("#carrier_department").val();
            shippingAddress['extension_attributes']['carrier_city'] = $("#carrier_city").val();
            shippingAddress['extension_attributes']['carrier_address'] = $("#carrier_address").val();
            shippingAddress['extension_attributes']['carrier_address_ukrposhta'] = $("#carrier_address_ukrposhta").val();

            shippingAddress['extension_attributes']['carrier_city_justin'] = $("#carrier_city_justin").val();
            shippingAddress['extension_attributes']['carrier_department_justin'] = $("#carrier_department_justin").val();
            shippingAddress['extension_attributes']['carrier_postcode_international'] = $("#carrier_postcode_international").val();

            // pass execution to original action ('Magento_Checkout/js/action/set-shipping-information')
            return originalAction();
        });
    };
});