define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Ui/js/form/element/abstract',
    'mage/url'
], function ($, ko, Component, quote, Abstract, url) {
    'use strict';

    ko.bindingHandlers.deAutoComplete = {

        init: function (element, valueAccessor) {
            var settings = valueAccessor();
            var selectedOption = settings.selected;
            var options = settings.options;
            var updateElementValueWithLabel = function (event, ui) {
                // Stop the default behavior
                event.preventDefault();

                $(element).val(ui.item.label);

                // Update our SelectedOption observable
                if (typeof ui.item !== "undefined") {
                    selectedOption(ui.item);
                }
            };

            $(element).autocomplete({
                source: options,
                select: function (event, ui) {
                    updateElementValueWithLabel(event, ui);
                }
            });

        }

    };


    ko.bindingHandlers.shippingCityAutoComplete = {

        init: function (element, valueAccessor) {
            Promise.resolve(
                $.ajax({
                    type: 'POST',
                    url: '/novaposhta/ajax/cities',
                    dataType: 'json'
                })
            ).then(function (result_list) {
                var settings = valueAccessor();

                var selectedOption = settings.selected;
                var options =  JSON.parse(result_list);
                console.log('options city');
                console.log(options);

                var updateElementValueWithLabel = function (event, ui) {
                    // Stop the default behavior
                    event.preventDefault();
                    $(element).val(ui.item.label);

                    if (typeof ui.item !== "undefined") {
                        // ui.item - id|label|...
                        selectedOption(ui.item);
                        //selectedValue(ui.item.value);
                    }
                };

                $(element).autocomplete({
                    //source: options,
                    source: function(req, responseFn) {
                        $.ajax({
                            url: '/novaposhta/ajax/cities',
                            data: {
                                q: req.term
                            },
                            //contentType: "application/json",
                            type: "POST",
                            dataType: 'json',
                            error : function () {
                                alert("An error have occurred.");
                            },
                            success : function (data) {

                                var items = JSON.parse(data);
                                console.log(items);
                                responseFn(items);
                            }
                        });


                    },
                    select: function (event, ui) {
                        updateElementValueWithLabel(event, ui);
                    }
                });

            });
        }
    };

   /* ko.bindingHandlers.shippingCityAutoComplete = {

        init: function (element, valueAccessor) {
            var settings = valueAccessor();
            var selectedOption = settings.selected;
            var options = settings.options;
            console.log('options');
            console.log(options);
            var updateElementValueWithLabel = function (event, ui) {
                // Stop the default behavior
                event.preventDefault();

                $(element).val(ui.item.label);

                // Update our SelectedOption observable
                if (typeof ui.item !== "undefined") {
                    selectedOption(ui.item);
                }
            };

            $(element).autocomplete({
                source: options,
                select: function (event, ui) {
                    updateElementValueWithLabel(event, ui);
                }
            });

        }

    };*/

    return Abstract.extend({

        selectedDepartment: ko.observable(''),
        selectedCity: ko.observable(''),
        selectedMethod: ko.computed(function () {
            var method = quote.shippingMethod();
            var selectedMethod = method != null ? method.carrier_code+'_'+method.method_code : null;
            console.log('selectedMethod '+selectedMethod);
            return selectedMethod;
        }, this),
        getCities: function (request, response) {


            $.ajax({
                url: url.build('novaposhta/ajax/cities/'),
                data: JSON.stringify({
                    q: request.term,
                }),
                contentType: "application/json",
                type: "POST",
                dataType: 'json',
                error: function () {
                    alert("An error have occurred.");
                },
                success: function (data) {

                    var items = JSON.parse(data);
                    response(items);
                }
            });
        },
        getDepartments: function (request, response) {
            var cityValue = $('[name="carrier_city"]').val();
            $.ajax({
                url: url.build('novaposhta/ajax/departments'),
                data: JSON.stringify({
                    q: request.term,
                    city: cityValue
                }),
                contentType: "application/json",
                type: "POST",
                dataType: 'json',
                error: function () {
                    alert("An error have occurred.");
                },
                success: function (data) {
                    var items = JSON.parse(data);
                    response(items);
                }
            });
        }
    });
});