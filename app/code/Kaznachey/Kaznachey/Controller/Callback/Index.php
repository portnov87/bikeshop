<?php

namespace Kaznachey\Kaznachey\Controller\Callback;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Kaznachey\Kaznachey\Model\Api\CallbackApi;
use Kaznachey\Kaznachey\Helper\Data as Helper;


class Index extends Action
{
    /** @var CallbackApi  */
    protected $_callbackApi;

    /** @var Helper  */
    protected $_helper;

    public function __construct(Context $context, CallbackApi $callbackApi, Helper $helper)
    {
        parent::__construct($context);
        $this->_callbackApi = $callbackApi;
        $this->_helper = $helper;
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $data = json_decode($this->getRequest()->getContent(), true);
        try {
            if (is_array($data)
                && $this->_helper->isEnabled()
                && $this->_callbackApi->process($data)) {;
                $data = [
                    'status' => 'success',
                ];
            } else {
                throw new \Exception(__('cannot process request'));
            }
        } catch (\Exception $e) {
            $data = [
                'status' => 'error',
            ];
        }
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result->setData($data);
        return $result;
    }
}