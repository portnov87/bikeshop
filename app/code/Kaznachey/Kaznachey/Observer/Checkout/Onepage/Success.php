<?php

namespace Kaznachey\Kaznachey\Observer\Checkout\Onepage;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Checkout\Model\Type\Onepage as CheckoutOnepage;
use Kaznachey\Kaznachey\Helper\Data as Helper;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Framework\Message\ManagerInterface as MessageManager;


class Success implements ObserverInterface
{
    /** @var  CheckoutOnepage  */
    protected $_checkoutOnepage;

    /** @var  Helper */
    protected $_helper;

    /** @var  OrderManagementInterface */
    protected $_orderManagement;

    /** @var MessageManager  */
    protected $_messageManager;

    public function __construct(CheckoutOnepage $checkoutOnepage,
                                Helper $helper,
                                OrderManagementInterface $orderManagement,
                                MessageManager $messageManager)
    {
        $this->_checkoutOnepage = $checkoutOnepage;
        $this->_helper = $helper;
        $this->_orderManagement = $orderManagement;
        $this->_messageManager = $messageManager;
    }

    public function execute(Observer $observer)
    {
        $resultParam = strtolower($this->_helper->getRequest()->getParam('Result'));
        if (($resultParam == 'fail' || $resultParam == 'failed') && $this->_helper->isEnabled()) {
            $checkoutSession = $this->_checkoutOnepage->getCheckout();
            $order = $checkoutSession->getLastRealOrder();
            if ($this->_helper->checkOrderIsKaznacheyPayment($order)) {
                $this->_orderManagement->cancel($order->getId());
                $checkoutSession->restoreQuote();
                $this->_messageManager->addErrorMessage(__('Payment failed. Please try again later.'));
                $checkoutFailUrl = $this->_helper->getUrlBuilder()->getUrl('checkout/cart');
                $this->getResponse()->setRedirect($checkoutFailUrl);
            }
        }
        return $this;
    }

    protected function getResponse()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Framework\App\ResponseInterface');
    }
}