<?php

namespace Kaznachey\Kaznachey\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;
use Magento\Sales\Model\Order\Address as OrderAddress;
use Magento\Framework\Exception\PaymentException;
use Kaznachey\Kaznachey\Model\Payment as KaznacheyPayment;


class Data extends AbstractHelper
{
    const XML_PATH_IS_ENABLED = 'payment/kaznachey_payment/active';
    const XML_PATH_TITLE = 'payment/kaznachey_payment/title';
    const XML_PATH_PUBLIC_KEY = 'payment/kaznachey_payment/public_key';
    const XML_PATH_PRIVATE_KEY = 'payment/kaznachey_payment/private_key';
    const XML_PATH_EXCHANGE_RATES = 'payment/kaznachey_payment/exchange_rates';
    const XML_PATH_ORDER_DESCRIPTION = 'payment/kaznachey_payment/order_description';
    const XML_PATH_LANGUAGE = 'payment/kaznachey_payment/language';
    const XML_PATH_SUCCESS_PAYMENT_ORDER_STATUS = 'payment/kaznachey_payment/success_payment_order_status';
    const XML_PATH_PAYMENT_CURRENCY = 'payment/kaznachey_payment/payment_currency';

    /** @var  PaymentHelper */
    protected $_paymentHelper;

    /** @var  AddressRenderer */
    protected $_addressRenderer;

    /** @var null|array */
    protected $_allowCurrencies = null;

    public function __construct(Context $context,
                                PaymentHelper $paymentHelper,
                                AddressRenderer $addressRenderer)
    {
        parent::__construct($context);
        $this->_paymentHelper = $paymentHelper;
        $this->_addressRenderer = $addressRenderer;
    }

    public function isEnabled()
    {
        if ($this->scopeConfig->getValue(static::XML_PATH_IS_ENABLED, ScopeInterface::SCOPE_STORE)) {
            if ($this->getPublicKey() && $this->getPrivateKey()) {
                return true;
            }
            $this->getLogger()->error(__('module is turned off, because public or private key is not set'));
        }
        return false;
    }

    public function getTitle()
    {
        return trim($this->scopeConfig->getValue(
            static::XML_PATH_TITLE,
            ScopeInterface::SCOPE_STORE
        ));
    }

    public function getLanguage()
    {
        return $this->scopeConfig->getValue(
            static::XML_PATH_LANGUAGE,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getPublicKey()
    {
        return trim($this->scopeConfig->getValue(
            static::XML_PATH_PUBLIC_KEY,
            ScopeInterface::SCOPE_STORE
        ));
    }

    public function getPrivateKey()
    {
        return trim($this->scopeConfig->getValue(
            static::XML_PATH_PRIVATE_KEY,
            ScopeInterface::SCOPE_STORE
        ));
    }

    public function getPaymentCurrency()
    {
        return strtoupper(trim($this->scopeConfig->getValue(
            static::XML_PATH_PAYMENT_CURRENCY,
            ScopeInterface::SCOPE_STORE
        )));
    }

    public function getSuccessPaymentOrderStatus()
    {
        return $this->scopeConfig->getValue(
            static::XML_PATH_SUCCESS_PAYMENT_ORDER_STATUS,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getOrderDescription(Order $order = null)
    {
        $description = trim($this->scopeConfig->getValue(
            static::XML_PATH_ORDER_DESCRIPTION,
            ScopeInterface::SCOPE_STORE
        ));
        $params['#'] = ''; //remove hash from description
        if ($order && $order->getId()) {
            $params['{order_id}'] = $order->getIncrementId();
        }
        return strtr($description, $params);
    }

    public function getSupportedCurrencies()
    {
        if (is_null($this->_allowCurrencies)) {
            $configTable = $this->scopeConfig->getValue(static::XML_PATH_EXCHANGE_RATES, ScopeInterface::SCOPE_STORE);
            $result = [];
            if ($configTable) {
                $data = $this->unserializeConfigTable($configTable, ['currency', 'value']);
                foreach ($data as $index => $item) {
                    $value = (double)$item['value'];
                    $currency = strtoupper(trim($item['currency']));
                    if (!$currency) {
                        $this->getLogger()->critical(__('invalid currency code in configuration'));
                        continue;
                    }
                    if ($value <= 0) {
                        $this->getLogger()->critical(__('invalid currency value in configuration'));
                        continue;
                    }
                    $result[$currency] = $value;
                }
            }
            $result[$this->getPaymentCurrency()] = 1; //exchange rate for kaznachey payment currency
            $this->_allowCurrencies = $result;
        }
        return $this->_allowCurrencies;
    }

    public function getPriceInPaymentCurrency($price, $currentCurrency)
    {
        $currencies = $this->getSupportedCurrencies();
        $currentCurrency = strtoupper($currentCurrency);
        if (isset($currencies[$currentCurrency])) {
            return $price * $currencies[$currentCurrency];
        }
        throw new PaymentException(__('could not convert price to another currency.'));
    }

    public function getLogger()
    {
        return $this->_logger;
    }

    public function getUrlBuilder()
    {
        return $this->_urlBuilder;
    }

    public function getEventManager()
    {
        return $this->_eventManager;
    }

    public function getRequest()
    {
        return $this->_getRequest();
    }

    public function checkOrderIsKaznacheyPayment(OrderInterface $order)
    {
        $method = $order->getPayment()->getMethod();
        $methodInstance = $this->_paymentHelper->getMethodInstance($method);
        return $methodInstance instanceof KaznacheyPayment;
    }

    public function getFormattedAddress(OrderAddress $address, $type = 'default')
    {
        return $this->_addressRenderer->format($address, $type);
    }

    protected function unserializeConfigTable($configTable, array $columns)
    {
        $configTableResults = unserialize($configTable);
        $result = [];
        if (is_array($configTableResults)) {
            foreach ($configTableResults as $item) {
                $row = [];
                foreach ($columns as $column) {
                    $row[$column] = isset($item[$column]) ? $item[$column] : null;
                }
                $result[] = $row;
            }
        }
        return $result;
    }
}