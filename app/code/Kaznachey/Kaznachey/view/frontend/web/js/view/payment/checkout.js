define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'kaznachey_payment',
                component: 'Kaznachey_Kaznachey/js/view/payment/method-renderer/kaznachey'
            }
        );
        return Component.extend({});
    }
);