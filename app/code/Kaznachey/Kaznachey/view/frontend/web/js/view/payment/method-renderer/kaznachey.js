define(
    [
        'Magento_Payment/js/view/payment/cc-form',
        'jquery',
        'ko',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Payment/js/model/credit-card-validation/validator'
    ],
    function (Component, $, ko) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false,
            payment: null,
            paySystems: ko.observableArray([]),
            paySystemId: ko.observable(),
            termToUse: ko.observable(false),
            defaults: {
                template: 'Kaznachey_Kaznachey/payment/checkout'
            },
            initialize: function () {
                this.payment = window.checkoutConfig.payment.kaznacheyPayment;
                this._super();
                if (this.isActive()
                    && this.payment.paySystems
                    && this.payment.paySystems.length) {
                    this.paySystems(this.payment.paySystems);
                    this.termToUse(this.payment.termToUse);
                    if (this.payment.paySystemId) {
                        this.paySystemId(this.payment.paySystemId);
                    } else {
                        this.paySystemId(this.payment.paySystems[0]['id']);
                    }
                }
            },
            getCode: function () {
                return this.payment.paymentCode;
            },
            isActive: function () {
                return this.payment.enabled;
            },
            validate: function () {
                var form = $('#' + this.getCode() + '-form');
                return form.validation() && form.validation('isValid');
            },
            afterPlaceOrder: function () {
                if (!this.isActive()) {
                    return;
                }
                var paySystemId = this.paySystemId();
                if (!paySystemId) {
                    return;
                }
                $.post(this.payment.urlPaymentForm, {
                    'paySystemId': paySystemId
                }).done(function (data) {
                    if (!data.status) {
                        return
                    }
                    if (data.status == 'success' && data.content) {
                        $('body').append('<div id="kaznacheyPaymentSubmitForm">' + data.content + '</div>');
                    } else if (data.redirect) {
                        window.location = data.redirect;
                    }
                });
            }
        });
    }
);