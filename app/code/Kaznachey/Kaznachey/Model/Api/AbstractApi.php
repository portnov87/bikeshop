<?php

namespace Kaznachey\Kaznachey\Model\Api;

use Kaznachey\Kaznachey\Helper\Data as Helper;


abstract class AbstractApi
{
    /** @var  Helper */
    protected $_helper;

    public function __construct(Helper $helper)
    {
        $this->_helper = $helper;
    }

    protected function formatPrice($price)
    {
        return number_format($price, 2, '.', '');
    }

    protected function hashSignature($signature)
    {
        return hash_hmac('md5', $signature, strtoupper($this->_helper->getPrivateKey()));
    }
}