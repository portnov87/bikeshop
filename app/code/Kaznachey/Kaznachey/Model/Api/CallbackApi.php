<?php

namespace Kaznachey\Kaznachey\Model\Api;

use Kaznachey\Kaznachey\Helper\Data as Helper;
use Kaznachey\Kaznachey\Model\Api\KaznacheyApi;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction as DbTransaction;
use Kaznachey\Kaznachey\Model\Api\AbstractApi;
use \Magento\Sales\Model\Order\Payment\Transaction;


class CallbackApi extends AbstractApi
{
    /** @var  KaznacheyApi */
    protected $_api;

    /** @var  Order */
    protected $_order;

    /** @var  OrderRepositoryInterface */
    protected $_orderRepository;

    /** @var  InvoiceService */
    protected $_invoiceService;

    /** @var  DbTransaction */
    protected $_dbTransaction;

    public function __construct(Helper $helper,
                                KaznacheyApi $api,
                                Order $order,
                                OrderRepositoryInterface $orderRepository,
                                InvoiceService $invoiceService,
                                DbTransaction $dbTransaction)
    {
        parent::__construct($helper);
        $this->_helper = $helper;
        $this->_api = $api;
        $this->_order = $order;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceService = $invoiceService;
        $this->_dbTransaction = $dbTransaction;
    }

    public function process(array $data)
    {
        if (!$this->validateSignature($data)) {
            return false;
        }
        if (!isset($data['MerchantInternalPaymentId'])) {
            return false;
        }
        $orderId = $data['MerchantInternalPaymentId'];
        $order = $this->_order->loadByIncrementId($orderId);
        if ($order && $order->getId() && $this->_helper->checkOrderIsKaznacheyPayment($order)) {
            $historyMessage = [];
            $state = null;
            /** @var \Magento\Sales\Model\Order\Payment $payment */
            $payment = $order->getPayment();
            $payment->setTransactionId('kaznachey-' . $data['OrderId']);
            $historyMessage[] = __('Kaznachey order id %1.', $data['OrderId']);
            if (isset($data['ErrorCode']) && $data['ErrorCode'] == 0) {
                if ($order->canInvoice()) {
                    $invoice = $this->_invoiceService->prepareInvoice($order);
                    $invoice->register()->pay();
                    $this->_dbTransaction->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    );
                    $historyMessage[] = __('Invoice #%1 created.', $invoice->getIncrementId());
                    $state = $this->_helper->getSuccessPaymentOrderStatus();
                } else {
                    $historyMessage[] = __('Error during creation of invoice.');
                }
                $payment->setIsTransactionClosed(1);
            } else {
                $payment->setIsTransactionClosed(0);
                $errorCode = isset($data['ErrorCode']) ? $data['ErrorCode'] : isset($data['Error']) ? $data['Error'] : -1;
                $errorMessage = [];
                foreach (['ErrorDecription', 'ErrorDescription', 'ErrorCode', 'Error', 'DebugMessage'] as $key) {
                    if (isset($data[$key])) {
                        $errorMessage[] = sprintf('%s: %s', $key, $data[$key]);
                    }
                }
                $errorMessage = implode(' ', $errorMessage);
                $historyMessage[] = __('Kaznachey code %1.', $errorCode);
                $historyMessage[] = __('Kaznachey message %1.', $errorMessage);
            }
            if (count($historyMessage)) {
                $order->addStatusHistoryComment(implode(' ', $historyMessage))
                    ->setIsCustomerNotified(true);
            }
            if ($state) {
                $order->setState($state);
                $order->setStatus($state);
            }
            $payment->addTransaction(Transaction::TYPE_ORDER);
            $this->_dbTransaction->addObject($payment);
            $this->_dbTransaction->save();
            $this->_orderRepository->save($order);
            return true;
        } else {
            return false;
        }
    }

    public function validateSignature(array $request)
    {
        $keys = ['ErrorCode', 'MerchantInternalPaymentId', 'MerchantInternalUserId', 'CustomMerchantInfo',
            'OrderSum', 'RecivedSum', 'OrderId', 'Currency', 'PaymentToken'];
        foreach ($keys as $key) {
            if (!isset($request[$key])) {
                return false;
            }
        }
        $requestSignature = strtoupper(
            $request['ErrorCode'] . ';' .
            $request['MerchantInternalPaymentId'] . ';' .
            $request['MerchantInternalUserId'] . ';' .
            $request['CustomMerchantInfo'] . ';' .
            $this->formatPrice($request['OrderSum']) . ';' .
            $this->formatPrice($request['RecivedSum']) . ';' .
            $request['OrderId'] . ';' .
            $request['Currency'] . ';' .
            $request['PaymentToken'] . ';' .
            $this->_helper->getPrivateKey() . ';');
        $requestSignature = $this->hashSignature($requestSignature);
        if (isset($request['SignatureEx'])) {
            return $request['SignatureEx'] == $requestSignature;
        } else if (isset($request['Signature'])) {
            return $request['Signature'] == $requestSignature;
        } else {
            return false;
        }
    }
}
