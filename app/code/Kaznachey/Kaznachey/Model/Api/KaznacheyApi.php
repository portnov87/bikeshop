<?php

namespace Kaznachey\Kaznachey\Model\Api;

use Magento\Sales\Model\Order;
use Magento\Catalog\Helper\Product as ProductHelper;
use Kaznachey\Kaznachey\Helper\Data as Helper;
use Kaznachey\Kaznachey\Model\Api\AbstractApi;


class KaznacheyApi extends AbstractApi
{
    const API_VERSION = '3.0';
    const PROTOCOL = 'http';
    const HOST = 'payment.kaznachey.net';
    const PORT = 80;

    /** @var  ProductHelper\ */
    protected $_productHelper;

    /** @var  array */
    protected $_supportedCurrencies;

    protected $_merchantInfo = false;
    protected $_serverUrl;
    protected $_merchantSecretKey;
    protected $_merchantGuid;

    public function __construct(Helper $helper, ProductHelper $productHelper)
    {
        parent::__construct($helper);
        $this->_productHelper = $productHelper;
        $this->_supportedCurrencies = array_keys($helper->getSupportedCurrencies());
        $this->_serverUrl = static::PROTOCOL . '://' . static::HOST. (static::PORT != '' ? ':' . static::PORT : '') . '/api/PaymentGatewayV3/';
        $this->_merchantSecretKey = $helper->getPrivateKey();
        $this->_merchantGuid = $helper->getPublicKey();
    }

    public function getSupportedCurrencies()
    {
        return $this->_supportedCurrencies;
    }

    public function getMerchantInfo()
    {
        if ($this->_merchantInfo === false) {
            $signature = strtoupper($this->_merchantGuid .';'. $this->_merchantSecretKey.';');
            $request = [
                'MerchantGuid' => $this->_merchantGuid,
                'Signature' => $this->hashSignature($signature)
            ];
            $response = $this->sendRequest(json_encode($request), 'GetMerchantInformation');
            $this->_merchantInfo = $this->prepareResponse($response);
        }
        return $this->_merchantInfo;
    }

    public function createPaymentFromOrder(Order $order, $paySystemId)
    {
        /** @var Order\Address $customerAddress */
        $customerAddress = $order->getBillingAddress();
        $products = [];
        $orderItems = $order->getAllVisibleItems();
        $index = 1;
        /** @var \Magento\Sales\Model\Order\Item $orderItem */
        foreach ($orderItems as $orderItem) {
            $product = $orderItem->getProduct();
            $products[] = [
                'ProductName' => $product->getName(),
                'ImageUrl' => $this->_productHelper->getSmallImageUrl($product),
                'ProductItemsNum' => $orderItem->getQtyOrdered(),
                'ProductPrice' => $this->formatPrice($this->_helper->getPriceInPaymentCurrency($orderItem->getPriceInclTax(), $order->getOrderCurrencyCode())),
                'ProductId' => $product->getSku(),
                'Id' => $index,
            ];
            ++$index;
        }

        $products[] = [
            'ProductName' => __('Shipping'),
            'ImageUrl' => null,
            'ProductItemsNum' => 1,
            'ProductPrice' => $this->formatPrice($this->_helper->getPriceInPaymentCurrency($order->getShippingInclTax(), $order->getOrderCurrencyCode())),
            'ProductId' => 'shipping',
            'Id' => $index,
        ];

        $requestCreatePayment = [
            'Version' => static::API_VERSION,
            'ImplementationInfo' => 'php',
            'SelectedPaySystemId' => $paySystemId,
            'Amount' => $this->formatPrice($this->_helper->getPriceInPaymentCurrency($order->getGrandTotal(), $order->getOrderCurrencyCode())),
            'Currency' => $this->_helper->getPaymentCurrency(),
            'AltAmount' => $this->formatPrice($order->getGrandTotal()),
            'AltCurrency' => $order->getOrderCurrencyCode(),
            'Language' => $this->_helper->getLanguage(),
            'UserEmail' => $order->getCustomerEmail(),
            'UserPhone' => $customerAddress->getTelephone(),
            'UserFullName' => $order->getCustomerName(),
            'UserAddress' => $this->_helper->getFormattedAddress($customerAddress),
            'Hold' => 'false',
            'CreatePaymentToken' => 'false',
            'MerchantCustomInfo' => $this->_helper->getOrderDescription($order),
            'MerchantInternalPaymentId' => $order->getIncrementId(),
            'MerchantInternalUserId' => $order->getCustomerId(),
            'ReturnUrl' => $this->_helper->getUrlBuilder()->getUrl('checkout/onepage/success'),
            'StatusUrl' => $this->_helper->getUrlBuilder()->getUrl('kaznachey/callback/index'),
            'PaymentInterval' => '0',
            'PartnershipGuid' => '',
            'Products' => $products,
        ];

        $this->_helper->getEventManager()->dispatch('kaznachey_create_payment_form', [
            'order' => $order,
            'createPaymentData' => &$requestCreatePayment,
        ]);

        return $this->createPayment($requestCreatePayment);
    }

    public function createPayment($request)
    {
        $request['MerchantGuid'] = $this->_merchantGuid;
        $signature = strtoupper($this->_merchantGuid . ';' .
            $this->formatPrice($request['Amount']) . ';' .
            $request['SelectedPaySystemId'] . ';' .
            $request['UserEmail'] . ';' .
            $request['UserPhone'] . ';' .
            $request['MerchantInternalUserId'] . ';' .
            $request['MerchantInternalPaymentId'] . ';' .
            $request['Language'] . ';' .
            $request['Currency'] . ';' .
            $request['CreatePaymentToken'] . ';' .
            $request['Hold'] . ';' .
            $request['PaymentInterval'] . ';' .
            $request['PartnershipGuid'] . ';' .
            $this->_merchantSecretKey . ';');
        $request['Signature'] = $this->hashSignature($signature);
        $response = $this->sendRequest(json_encode($request), 'Create');
        $response = $this->prepareResponse($response);
        if ($response) {
            $response['ExternalForm'] = isset($response['ExternalForm']) ? base64_decode($response['ExternalForm']) : '';
        }
        return $response;
    }

    protected function prepareResponse($response)
    {
        $data = json_decode($response, true);
        /*echo '<pre>ddededed';
        print_r($response);
        echo '</pre>';
        die();*/
        if ($data && isset($data['ErrorCode']) && $data['ErrorCode'] == 0) {
            return $data;
        } else {
            $errorCode = isset($data['ErrorCode']) ? $data['ErrorCode'] : isset($data['Error']) ? $data['Error'] : -1;
            $errorMessage = '';//isset($data['DebugMessage']) ? $data['DebugMessage'] : isset($data['ErrorMessage']) ? $data['ErrorMessage'] : '-';
            $this->_helper->getLogger()->error(__('api response error with code %1 and message %2', $errorCode, $errorMessage));
            return null;
        }
    }

    protected function sendRequest($jsonData, $uri)
    {
        $curl = curl_init();
        if (!$curl) {
            return false;
        }
        curl_setopt($curl, CURLOPT_URL, $this->_serverUrl . $uri);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            ['Expect: ', 'Content-Type: application/json; charset=UTF-8', 'Content-Length: '
                . strlen($jsonData)]);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, True);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}