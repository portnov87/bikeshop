<?php

namespace Kaznachey\Kaznachey\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Kaznachey\Kaznachey\Helper\Data as Helper;
use Kaznachey\Kaznachey\Model\Payment;
use Kaznachey\Kaznachey\Model\Api\KaznacheyApi;


class PaymentConfigProvider implements ConfigProviderInterface
{
    const CONFIG_NAMESPACE = 'kaznacheyPayment';

    /** @var Helper */
    protected $_helper;

    /** @var KaznacheyApi */
    protected $_api;

    public function __construct(Helper $helper, KaznacheyApi $api)
    {
        $this->_helper = $helper;
        $this->_api = $api;
    }

    public function getConfig()
    {
        $config = [
            'enabled' => $this->_helper->isEnabled(),
            'paymentCode' => Payment::METHOD_CODE,
        ];
        if ($config['enabled']) {
            $merchantInfo = $this->_api->getMerchantInfo();
            if (is_array($merchantInfo)
                && isset($merchantInfo['PaySystems'])
                && is_array($merchantInfo['PaySystems'])
                && count($merchantInfo['PaySystems'])
            ) {
                $paySystems = [];
                foreach ($merchantInfo['PaySystems'] as $item) {
                    $paySystems[] = [
                        'id' => $item['Id'],
                        'name' => $item['PaySystemName'],
                    ];
                }
                $config['paySystems'] = $paySystems;
                if (isset($merchantInfo['TermToUse'])) {
                    $config['termToUse'] = $merchantInfo['TermToUse'];
                }
                $config['urlPaymentForm'] = $this->_helper->getUrlBuilder()->getUrl('kaznachey/checkout/form');
            } else {
                $config['enabled'] = false;
            }
        }
        return [
            'payment' => [
                static::CONFIG_NAMESPACE => $config,
            ],
        ];
    }
}