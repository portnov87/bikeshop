<?php

namespace Kaznachey\Kaznachey\Model\System\Config;


class Languages extends AbstractOptions
{
    const ENGLISH = 'EN';
    const RUSSIAN = 'RU';

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => static::ENGLISH,
                'label' => __('EN - english'),
            ],
            [
                'value' => static::RUSSIAN,
                'label' => __('RU - russian'),
            ],
        ];
        return $options;
    }
}