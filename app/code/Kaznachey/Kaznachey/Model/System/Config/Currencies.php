<?php

namespace Kaznachey\Kaznachey\Model\System\Config;


class Currencies extends AbstractOptions
{
    const USD = 'USD';
    const EUR = 'EUR';
    const UAH = 'UAH';
    const RUB = 'RUB';

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => static::USD,
                'label' => __('USD'),
            ],
            [
                'value' => static::EUR,
                'label' => __('EUR'),
            ],
            [
                'value' => static::UAH,
                'label' => __('UAH'),
            ],
            [
                'value' => static::RUB,
                'label' => __('RUB'),
            ],
        ];
        return $options;
    }
}