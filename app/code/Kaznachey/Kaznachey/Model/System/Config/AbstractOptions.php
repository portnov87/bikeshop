<?php

namespace Kaznachey\Kaznachey\Model\System\Config;

use Magento\Framework\Data\OptionSourceInterface;


abstract class AbstractOptions implements OptionSourceInterface
{
    public function toArray()
    {
        return array_column($this->toOptionArray(), 'value');
    }
}