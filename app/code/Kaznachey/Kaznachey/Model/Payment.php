<?php

namespace Kaznachey\Kaznachey\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Method\Logger as PaymentLogger;
use Magento\Framework\UrlInterface;
use Kaznachey\Kaznachey\Model\Api\KaznacheyApi;
use Kaznachey\Kaznachey\Helper\Data as Helper;


class Payment extends \Magento\Payment\Model\Method\AbstractMethod
{
    const METHOD_CODE = 'kaznachey_payment';

    protected $_code = self::METHOD_CODE;

    /** @var  KaznacheyApi */
    protected $_api;

    /** @var  Helper */
    protected $_helper;

    /** @var \Magento\Framework\UrlInterface */
    protected $_urlBuilder;

    protected $_canCapture = true;
    protected $_canVoid = true;
    protected $_canUseForMultishipping = false;
    protected $_canUseInternal = false;
    protected $_isInitializeNeeded = true;
    protected $_isGateway = true;
    protected $_canAuthorize = false;
    protected $_canCapturePartial = false;
    protected $_canRefund = false;
    protected $_canRefundInvoicePartial = false;
    protected $_canUseCheckout = true;

    protected $_minOrderTotal = 0;
    protected $_supportedCurrencyCodes;

    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        PaymentHelper $paymentHelper,
        ScopeConfigInterface $scopeConfig,
        PaymentLogger $logger,
        UrlInterface $urlBuilder,
        KaznacheyApi $api,
        Helper $helper,
        array $data = array()
    )
    {
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory,
            $paymentHelper, $scopeConfig, $logger, null, null, $data);
        $this->_api = $api;
        $this->_helper = $helper;
        $this->_supportedCurrencyCodes = $api->getSupportedCurrencies();
        $this->_minOrderTotal = $this->getConfigData('min_order_total');
        $this->_urlBuilder = $urlBuilder;
    }

    public function canUseForCurrency($currencyCode)
    {
        return in_array($currencyCode, $this->_supportedCurrencyCodes);
    }

    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        return $this;
    }

    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (!$this->_helper->isEnabled()) {
            return false;
        }
        $this->_minOrderTotal = $this->getConfigData('min_order_total');
        if ($quote && $quote->getBaseGrandTotal() < $this->_minOrderTotal) {
            return false;
        }
        return parent::isAvailable($quote);
    }
}