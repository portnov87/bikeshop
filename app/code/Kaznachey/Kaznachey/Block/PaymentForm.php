<?php

namespace Kaznachey\Kaznachey\Block;

use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\Order;
use Kaznachey\Kaznachey\Model\Api\KaznacheyApi;
use Kaznachey\Kaznachey\Helper\Data as Helper;
use Magento\Framework\Exception\LocalizedException;


class PaymentForm extends Template
{
    /** @var null|Order */
    protected $_order = null;

    /** @var null|int */
    protected $_paySystemId = null;

    /* @var KaznacheyApi */
    protected $_api;

    /* @var Helper */
    protected $_helper;

    public function __construct(
        Template\Context $context,
        KaznacheyApi $api,
        Helper $helper,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_api = $api;
        $this->_helper = $helper;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        if ($this->_order === null) {
            throw new LocalizedException(__('Order is not set'));
        }
        return $this->_order;
    }

    public function setOrder(Order $order)
    {
        $this->_order = $order;
        return $this;
    }

    public function getPaySystemId()
    {
        if ($this->_paySystemId === null) {
            throw new LocalizedException(__('paySystemId is not set'));
        }
        return $this->_paySystemId;
    }

    public function setPaySystemId($paySystemId)
    {
        $this->_paySystemId = $paySystemId;
        return $this;
    }

    protected function _loadCache()
    {
        return false;
    }

    protected function _toHtml()
    {
        $paymentForm = $this->_api->createPaymentFromOrder($this->getOrder(), $this->getPaySystemId());
        if (is_array($paymentForm) && isset($paymentForm['ExternalForm'])) {
            return $paymentForm['ExternalForm'];
        }
        return null;
    }
}