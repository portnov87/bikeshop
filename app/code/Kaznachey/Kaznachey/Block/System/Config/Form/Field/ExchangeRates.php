<?php

namespace Kaznachey\Kaznachey\Block\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;


class ExchangeRates extends AbstractFieldArray
{
    /**
     * Grid columns
     *
     * @var array
     */
    protected $_columns = [];

    /**
     * Enable the "Add after" button or not
     *
     * @var bool
     */
    protected $_addAfter = true;

    /**
     * Label of add button
     *
     * @var string
     */
    protected $_addButtonLabel;


    /**
     * Currency List Renderer
     *
     * @var \Magento\Framework\View\Element\BlockInterface
     */
    protected $_currencyListRenderer = false;

    /**
     * Check if columns are defined, set template
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'currency', [
                'label' => __('Currency'),
                'renderer' => $this->getCurrencyListRenderer(),
            ]
        );
        $this->addColumn('value', ['label' => __('Text')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Render array cell for prototypeJS template
     *
     * @param string $columnName
     * @return string
     * @throws \Exception
     */
    public function renderCellTemplate($columnName)
    {
        if ($columnName == 'value') {
            $this->_columns[$columnName]['class'] = 'input-text required-entry';
        }
        return parent::renderCellTemplate($columnName);
    }

    /**
     * @return \Magento\Framework\View\Element\BlockInterface
     */
    protected function getCurrencyListRenderer()
    {
        if (!$this->_currencyListRenderer) {
            $this->_currencyListRenderer = $this->getLayout()->createBlock(
                '\Kaznachey\Kaznachey\Block\Adminhtml\Form\Field\CurrencyList',
                '',
                [
                    'data' => [
                        'is_render_to_js_template' => true
                    ]
                ]
            );
        }
        return $this->_currencyListRenderer;
    }

    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $currency = $row->getCurrency();
        $options = [];
        if ($currency) {
            $hash = $this->getCurrencyListRenderer()->calcOptionHash($currency);
            $options['option_' . $hash] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }
}