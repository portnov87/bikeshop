<?php

namespace Kaznachey\Kaznachey\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;
use Magento\Framework\View\Element\Context;
use Kaznachey\Kaznachey\Model\System\Config\Currencies;


class CurrencyList extends Select
{
    /**
     * Currencies
     *
     * @var Currencies
     */
    protected $currencies;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Currencies $currencies
     * @param array $data
     */
    public function __construct(Context $context, Currencies $currencies, array $data = [])
    {
        parent::__construct($context, $data);
        $this->currencies = $currencies;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $options = $this->currencies->toOptionArray();
            foreach ($options as $option) {
                $this->addOption($option['value'], $option['label']);
            }
        }
        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
