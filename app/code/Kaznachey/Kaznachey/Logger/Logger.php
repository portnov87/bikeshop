<?php

namespace Kaznachey\Kaznachey\Logger;


class Logger extends \Monolog\Logger
{
    public function addRecord($level, $message, array $context = [])
    {
        $message = __('Kaznachey Payment: %1', (string)$message);
        return parent::addRecord($level, $message, $context);
    }
}