var config = {
    map: {
        '*': {
            "Magento_Ui/js/form/element/post-code": 'TemplateMonster_ThemeOptions/js/form/element/post-code',
           // "Magento_Ui/js/form/element/country": 'TemplateMonster_ThemeOptions/js/form/element/country'
        }
    },
    paths: {
        "productListingGallery": "TemplateMonster_ThemeOptions/js/product-listing-gallery",
        "switchImage": "TemplateMonster_ThemeOptions/js/switch-image",
    }
};