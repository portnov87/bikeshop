<?php

namespace Webfresh\FlySms\Observer;

use Magento\Framework\Event\ObserverInterface;

use Magento\Framework\Event\Observer;


class CreateOrder implements ObserverInterface
{

    protected $_apiSmsfly;
    protected $_objectManager;

    /* public function __construct(
         \Magento\Framework\ObjectManager\ObjectManager $_objectManager
         //\Webfresh\FlySms\Helper\ApiFlysmsFactory $_apiSmsfly
     )
     {
         //$this->_apiSmsfly=$_apiSmsfly;
         $this->_objectManager = $_objectManager;
     }*/

//    public function __construct(
//        \TemplateMonster\ShopByBrand\Helper\Data $helper,
//        SoldBrandInterfaceFactory $soldBrandFactory,
//        SoldBrandRepositoryInterface $soldBrandRepository
//    ) {
//        $this->helper = $helper;
//        $this->soldBrandFactory = $soldBrandFactory;
//        $this->soldBrandRepository = $soldBrandRepository;
//    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        $address = $order->getShippingAddress();
        if ($address) {
            $phone = $address->getTelephone();
            $phone = str_replace(['-','(',')',' '],['','','',''],$phone);
            $orderIncrement=$order->getIncrementId();
            $text = 'Заказ №'.$orderIncrement.'. В роботі. Очикуйте SMS для отримання\сплати.';

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $_apiSmsfly = $objectManager->create('Webfresh\FlySms\Helper\ApiFlysms');

            $_apiSmsfly->sendSms($phone, $text);
        }

    }
}