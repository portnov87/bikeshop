<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 03.03.2020
 * Time: 14:16
 */

namespace Webfresh\FlySms\Helper;


/**
 * Data Helper
 *
 * @package Webfresh\ThemeOptions\Helper
 */
class ApiFlysms
{
    protected $urlApi='http://sms-fly.com/api/api.php';
    protected $login='380984001509';
    protected $password='jorjik';
    protected $logger;
    public function __construct(
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->logger=$logger;
    }

    public function sendSms($phone,$text)
    {
        $phone=str_replace([' ',')','(','-','+'],['','','','',''],$phone);

        $dataRequest 	 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $dataRequest 	.= "<request>";
        $dataRequest 	.= "<operation>SENDSMS</operation>";

        $dataRequest 	.= '<message start_time="AUTO" end_time="AUTO" lifetime="4" rate="120" desc="My first campaign" source="BIKESHOP.UA">';
        $dataRequest 	.= '<body>'.$text.'</body>';
        $dataRequest 	.= '<recipient>'.$phone.'</recipient>';
        $dataRequest 	.= '</message>';
        $dataRequest 	.= "</request>";

//        echo 'test';
//        die();
        $response = $this->sendRequest($dataRequest);
        /*echo '<pre>';
        print_r($response);
        echo '</pre>';*/

        $this->logger->debug(var_export($response, true));
    }

    private function sendRequest($postData)
    {
        $user = $this->login;
        $password = $this->password;

        if ($ch = curl_init()) {
            curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $password);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_URL, $this->urlApi);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Accept: text/xml"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        }
        return false;
    }

}

