define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'monobank',
                component: 'Webfresh_Monobank/js/view/payment/method-renderer/monobank'
            }
        );
        return Component.extend({});
    }
);