<?php
namespace Webfresh\Monobank\Model;


use Magento\Directory\Helper\Data as DirectoryHelper;

class PaymentMonobank  extends \Magento\Payment\Model\Method\AbstractMethod
{
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'monobank';


    public function canCapture()
    {

        return true;
    }
    /*public function canUseForCurrency($currencyCode)
    {
        return true;
        if (!in_array($currencyCode, $this->_supportedCurrencyCodes)) {
            return false;
        }
        return true;
    }

    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {

        $order = $payment->getOrder();

        try{

            return $this;

        }catch (\Exception $e){

            $this->debugData(['exception' => $e->getMessage()]);
            throw new \Magento\Framework\Validator\Exception(__('Payment cannot be processed at this time, please contact support'));
        }
    }

    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        return $this;
    }
*/
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null){

        return true;

    }
}