<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Webfresh\Bikeshop\Ui\DataProvider;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

/**
 * DataProvider for product edit form
 */
class PriceDataProvider extends AbstractDataProvider
{
    /**
     * @var PoolInterface
     */
    private $pool;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param PoolInterface $pool
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        //CollectionFactory $collectionFactory,
        //PoolInterface $pool,
        array $meta = [],
        array $data = []
    )
    {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        //$this->collection = $collectionFactory->create();
        //$this->pool = $pool;
    }


    public function getListProviders()
    {
        return [
            'usd',
            'uah'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $this->data = [
//            'file' => 'ggggygg2',
//            'providername' => 'ddddd1',
            'general' => [
                'file' => 'ggggygg',
                'providername' => 'ddddd'
            ],
            'config' => [
                'data' => [
                    'file'=>[
                      ['name'=>'','url'=>'']
                    ],
//                    'file' => 'wwwww',
                    'providername' => ''
                ]
            ]

        ];
//        array_replace_recursive(
//            $this->data,
//            [
//                'config' => [
//                    'data' => [
//                        'is_active' => 1,
//                        'include_in_menu' => 1,
//                        'return_session_messages_only' => 1,
//                        'use_config' => ['available_sort_by', 'default_sort_by']
//                    ]
//                ]
//            ]
//        );

        return $this->data;
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        return null;
    }

    public function addOrder($field, $direction)
    {
        return null;
    }

    public function count()
    {
        return 0;
    }

//
//    /**
//     * {@inheritdoc}
//     */
//    public function getData()
//    {
//        /** @var ModifierInterface $modifier */
//        foreach ($this->pool->getModifiersInstances() as $modifier) {
//            $this->data = $modifier->modifyData($this->data);
//        }
//
//        return $this->data;
//    }

//    /**
//     * {@inheritdoc}
//     */
//    public function getMeta()
//    {
//        $meta = parent::getMeta();
//
//        /** @var ModifierInterface $modifier */
//        foreach ($this->pool->getModifiersInstances() as $modifier) {
//            $meta = $modifier->modifyMeta($meta);
//        }
//
//        return $meta;
//    }
}
