<?php

namespace Webfresh\Bikeshop\Api\Data;

/**
 * Interface BrandInterface
 *
 * @package TemplateMonster\ShopByBrand\Api\Data
 */
interface ProvidersPriceInterface
{
    const ENTITY_ID = 'entity_id';
    const PROVIDER_CODE = 'provider_code';
    const ATTRIBUTE_ID = 'attribute_id';
    const ATTRIBUTE_VALUE_ID = 'attribute_value_id';
    const LINK = 'link';
    const STATUS = 'status';
    const TYPE = 'type';

    const LAST_PROCESS_DATE = 'last_process_date';
    const LAST_PROCESS_STATUS = 'last_process_status';
    /**
     * Get id.
     *
     * @return int
     */
    public function getId();

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setId($id);

    /**
     * Get id.
     *
     * @return int
     */
    public function getEntityId();

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setEntityId($id);


    /**
     * Get id.
     *
     * @return int
     */
    public function getLastProcessDate();

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setLastProcessDate($value);


    /**
     * Get id.
     *
     * @return int
     */
    public function getLastProcessStatus();

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setLastProcessStatus($value);



    /**
     * Get name.
     *
     * @return string
     */
    public function getProviderCode();

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setProviderCode($code);

    /**
     * Get url page.
     *
     * @return string
     */
    public function getAttributeId();

    /**
     * Set url page.
     *
     * @param string $urlPage
     *
     * @return $this
     */
    public function setAttributeId($attributeId);

    /**
     * Get page title.
     *
     * @return string
     */
    public function getAttributeValueId();

    /**
     * Set page title.
     *
     * @param string $pageTitle
     *
     * @return $this
     */
    public function setAttributeValueId($value);

    /**
     * Get logo.
     *
     * @return string
     */
    public function getLink();

    /**
     * Set logo.
     *
     * @param string $logo
     *
     * @return $this
     */
    public function setLink($link);

    /**
     * Get brand banner.
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set brand banner.
     *
     * @param string $brandBanner
     *
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get product banner.
     *
     * @return string
     */
    public function getType();

    /**
     * Set product banner.
     *
     * @param string $productBanner
     *
     * @return $this
     */
    public function setType($type);

}