<?php

namespace Webfresh\Bikeshop\Api;

use Webfresh\Bikeshop\Api\Data\ProvidersPriceInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface BrandRepositoryInterface
 *
 * @package TemplateMonster\ShopByBrand\Api
 */
interface ProvidersPriceRepositoryInterface
{
    /**
     * Save brand.
     *
     * @param \TemplateMonster\ShopByBrand\Api\Data\BrandInterface $brand
     * @return \TemplateMonster\ShopByBrand\Api\Data\BrandInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(ProvidersPriceInterface $entity);

    /**
     * Retrieve brand.
     *
     * @param int $brandId
     * @return \TemplateMonster\ShopByBrand\Api\Data\BrandInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($entityId);

    /**
     * Retrieve brands matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return \TemplateMonster\ShopByBrand\Api\Data\BrandSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete brand.
     *
     * @param \TemplateMonster\ShopByBrand\Api\Data\BrandInterface $brand
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ProvidersPriceInterface $provider);

    /**
     * Delete brand by ID.
     *
     * @param int $brandId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($entityId);
}