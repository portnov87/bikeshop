<?php

namespace Webfresh\Bikeshop\Block;

use TemplateMonster\ThemeOptions\Helper\Data as ThemeOptionsHelper;
use Magento\Framework\View\Element\Template;

/**
 * Social links block.
 *
 * @method string getPosition()
 *
 * @package TemplateMonster\ThemeOptions\Block
 */
class HeaderPhones extends Template
{
    /**
     * @var string
     */
    protected $_template = 'header/header_phones.phtml';

    /**
     * @var array
     */
    protected $_positions = ['header', 'footer'];

    /**
     * @var ThemeOptionsHelper
     */
    protected $_helper;

    /**
     * SocialLinks constructor.
     *
     * @param ThemeOptionsHelper $helper
     * @param Template\Context   $context
     * @param array              $data
     */
//    public function __construct(
//        ThemeOptionsHelper $helper,
//        Template\Context $context,
//        array $data
//    )
//    {
//        $this->_helper = $helper;
//        parent::__construct($context, $data);
//    }

    /**
     * @inheritdoc
     */
    protected function _toHtml()
    {


        return parent::_toHtml();
    }


}