<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webfresh\Bikeshop\Block;

use Magento\Store\Model\ScopeInterface;

/**
 * Cms page content block
 */
class Page extends \Magento\Cms\Block\Page
{

    /**
     * Prepare global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $page = $this->getPage();
        $this->_addBreadcrumbs($page);
        $this->pageConfig->addBodyClass('cms-' . $page->getIdentifier());
        $metaTitle = $page->getMetaTitle();
        if ($metaTitle!='')
        {
            $metaTitle=$page->getTitle().' – интернет-магазин велотоваров BikeShop';
        }
        $this->pageConfig->getTitle()->set($metaTitle ? $metaTitle : $page->getTitle());
        $this->pageConfig->setKeywords($page->getMetaKeywords());
        $metaDescription=$page->getMetaDescription();
        if ($metaDescription!='')
        {
            $metaDescription=$page->getTitle().' *** BikeShop - Наверное, лучший магазин для велосипедистов! Телефон горячей линии: 0-800-300-180. Доставка товаров по всей территории Украины.';
        }
        $this->pageConfig->setDescription($metaDescription);//$page->getMetaDescription());

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            // Setting empty page title if content heading is absent
            $cmsTitle = $page->getContentHeading() ?: ' ';
            $pageMainTitle->setPageTitle($this->escapeHtml($cmsTitle));
        }
        return parent::_prepareLayout();
    }


}
