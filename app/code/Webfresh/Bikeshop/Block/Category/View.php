<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
//namespace Magento\Catalog\Block\Category;

namespace Webfresh\Bikeshop\Block\Category;

/**
 * Class View
 * @package Magento\Catalog\Block\Category
 */
class View extends \Magento\Catalog\Block\Category\View
{

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->getLayout()->createBlock('Magento\Catalog\Block\Breadcrumbs');

        $category = $this->getCurrentCategory();
        if ($category) {
            $title = $category->getMetaTitle();
            $products=$category->getProductCollection()
                ->addFieldToFilter('price', array('neq' => 0))
                ->addFieldToFilter('status',1)
                ->addAttributeToSort('price','ASC');
            $minPrice=0;
            foreach ($products as $pr)
            {
                $minPrice=round($pr->getFinalPrice(),2);
//                echo $pr->getFinalPrice()."<br/>";
//                die();
                break;
            }

            //round($products->getMinPrice(),2);
            //->addAttributeToSort('price', 'ASC');

            if ($title!='') {
                $title = $category->getName().' от '.$minPrice.' грн – купить в интернет-магазине BikeShop: цены, отзывы | Доставка по Киеву и всей Украине';
            }
            if ($title) {

                $this->pageConfig->getTitle()->set($title);
            }
            $description = $category->getMetaDescription();
            if ($description!='') {

                $description='Купить '.$category->getName().' от '.$minPrice.' грн ➡️BikeShop - Наверное, лучший магазин для велосипедистов  ✔ Широкий выбор 🚲 Качественных велосипедов, 🧤Надежных велоаксессуаров и ⚙️Велозапчастей известных мировых производителей🤑 Низкие цены ✔ Бесплатная доставка велосипедов';
                //$category->getName().' от '.$minPrice.' грн – купить в интернет-магазине BikeShop: цены, отзывы | Доставка по Киеву и всей Украине';
                $this->pageConfig->setDescription($description);
            }
            $keywords = $category->getMetaKeywords();
            if ($keywords) {
                $this->pageConfig->setKeywords($keywords);
            }
            if ($this->_categoryHelper->canUseCanonicalTag()) {
                $this->pageConfig->addRemotePageAsset(
                    $category->getUrl(),
                    'canonical',
                    ['attributes' => ['rel' => 'canonical']]
                );
            }

            $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
            if ($pageMainTitle) {
                $pageMainTitle->setPageTitle($this->getCurrentCategory()->getName());
            }
        }

        return $this;
    }

}
