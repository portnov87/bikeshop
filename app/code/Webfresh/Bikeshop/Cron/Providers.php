<?php
namespace Webfresh\Bikeshop\Cron;

use \Psr\Log\LoggerInterface;

use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;


class Providers {

    protected $logger;

    protected $dir;
    protected $helperBikeshopProcessing;

    protected $helperBikeshop;
    protected $providersPriceRepository;
    protected $scopeConfig;
    protected $searchCriteriaBuilder;
    protected $_productRepository;
    protected $state;
    protected $csv;


    public function __construct(LoggerInterface $logger,
                                Filesystem $filesystem,
                                \Magento\Catalog\Model\ProductRepository $productRepository,
                                \Magento\Framework\File\Csv $csv,
                                \Magento\Framework\Filesystem\DirectoryList $directoryList,
                                //\Magento\Framework\App\State $state,
                                \Webfresh\Bikeshop\Helper\Data $helperBikeshop,
                                \Webfresh\Bikeshop\Api\ProvidersPriceRepositoryInterface $providersPriceRepository,
                                \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
                                \Webfresh\Bikeshop\Helper\Processing $helperBikeshopProcessing,
                                \Magento\Framework\Stdlib\DateTime\DateTime $date
    )
    {


        $this->_date=$date;

        $this->helperBikeshopProcessing = $helperBikeshopProcessing;
        $this->providersPriceRepository=$providersPriceRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;

        //$this->state = $state;
        $this->_productRepository = $productRepository;
        $this->csv = $csv;
        $this->helperBikeshop=$helperBikeshop;

    }

    /**
     * Write to system.log
     *
     * @return void
     */

    public function execute() {

        //$this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMIN);
        $searchCriteria = $this->searchCriteriaBuilder
            //->addFilter('provider_code', 'plu', 'eq')
            ->addFilter('status', 1, 'eq')
            ->create();
        $items = $this->providersPriceRepository
            ->getList($searchCriteria)
            ->getItems();

        $helperBikeshopProcessing=$this->helperBikeshopProcessing;

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

        $connection = $resource->getConnection();


        foreach ($items as $item)
        {
            try {
            $_provider=$this->providersPriceRepository->getById($item['entity_id']);
            $_provider->setLastProcessStatus(0);
            $_provider->save();
            //$this->providersPriceRepository->save($_provider);
            $result=$helperBikeshopProcessing->processProvidersById($item['entity_id']);

            if ($result)
                $_provider->setLastProcessStatus(1);
            else
                $_provider->setLastProcessStatus(0);


            $_date=$this->_date->date();//gmtDate();
            $date=date('Y-m-d H:i:s');

            //echo '$date = '.$_date." ".$date." \r\n";
            //$_provider->setData('last_process_date',$_date);//
            $_provider->setLastProcessDate($date);
            //$_provider->save();
            $this->providersPriceRepository->save($_provider);

            $connection->update(
                'bikeshop_providers_price',
                ['last_process_date' => $date],
                [
                    'entity_id = ?' => $item['entity_id'],
                ]
            );

            } catch (\Exception $e) {
                echo '<pre>';
                print_r($data);
                echo '</pre>';
                echo $e->getMessage() . "\r\n";

            }

        }


    }





}