<?php

/**
 *
 */

namespace Webfresh\Bikeshop\Plugin;

use Magento\Framework\Exception\NoSuchEntityException;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Reflection\DataObjectProcessor;


//use Magento\Customer\Helper\View as CustomerViewHelper;

/**
 *
 */

class EmailNotification
{

    /**#@-*/
    private $customerRegistry;
    private $storeManager;
    /**
     * @var DataObjectProcessor
     */
    protected $dataProcessor;

    /**
     * @var CustomerViewHelper
     */
    protected $customerViewHelper;

    public function __construct(
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        StoreManagerInterface $storeManager,
        DataObjectProcessor $dataProcessor,
        \Magento\Customer\Helper\ViewFactory $customerViewHelper

    )

    {
        $this->dataProcessor = $dataProcessor;
        $this->customerViewHelper = $customerViewHelper->create();
        $this->customerRegistry = $customerRegistry;
        $this->storeManager = $storeManager;
    }


//    public function aroundNewAccount(
//        \Magento\Customer\Model\EmailNotification $subject,
//                                     callable $proceed
//    )
    public function aroundNewAccount(
        \Magento\Customer\Model\EmailNotification $subject,
        \Closure $proceed,
        CustomerInterface $customer,
        $type = \Magento\Customer\Model\EmailNotification::NEW_ACCOUNT_EMAIL_REGISTERED,
        $backUrl = '',
        $storeId = 0,
        $sendemailStoreId = null)
    {
        return;
    }

    public function aroundCredentialsChanged(
        \Magento\Customer\Model\EmailNotification $subject,
        \Closure $proceed,
        $origCustomerEmail,
        $isPasswordChanged = false)
    {
        return;
    }
    public function aroundEmailAndPasswordChanged(
        \Magento\Customer\Model\EmailNotification $subject,
        \Closure $proceed,
        $email)
    {
        return;
    }

    public function aroundEmailChanged(
        \Magento\Customer\Model\EmailNotification $subject,
        \Closure $proceed,
        $email)
    {
        return;
    }





}