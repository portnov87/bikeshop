<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpMassUpload
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webfresh\Bikeshop\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\View\Layout\Argument\UpdaterInterface;
use Magento\Eav\Model\Config;


/**
 * Install Data script
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * EAV setup factory
     *
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;
    private $_attributeRepository;
    private $_attributeOptionManagement;
    private $_option;
    private $_attributeOptionLabel;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        Config $eavConfig,
        \Magento\Eav\Model\AttributeRepository $attributeRepository,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Model\Entity\Attribute\OptionLabelFactory $attributeOptionLabel,
        \Magento\Eav\Model\Entity\Attribute\OptionFactory $option
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;

        $this->eavConfig = $eavConfig;

        /* These are optional and used only to populate the attribute with some mock options */
        $this->_attributeRepository = $attributeRepository;
        $this->_attributeOptionManagement = $attributeOptionManagement;
        $this->_option = $option;
        $this->_attributeOptionLabel = $attributeOptionLabel;


    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            $eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'telephone', ['type' => 'varchar', 'label' => 'Telephone', 'input' => 'text', 'required' => false, 'visible' => true, 'user_defined' => true, 'position' => 999, 'system' => 0,]);
            $attribute = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'telephone');
            // used_in_forms are of these types you can use forms key according to your need ['adminhtml_checkout','adminhtml_customer','adminhtml_customer_address','customer_account_edit','customer_address_edit','customer_register_address', 'customer_account_create']
            $attribute->setData('used_in_forms', ['adminhtml_customer',
                'adminhtml_checkout',
                'customer_account_create',
                'customer_account_edit']);
            $attribute->save();

            $eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'vamshop_id', ['type' => 'int', 'label' => 'Telephone', 'input' => 'text', 'required' => false, 'visible' => true, 'user_defined' => true, 'position' => 999, 'system' => 0,]);
            $attribute = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'vamshop_id');
            // used_in_forms are of these types you can use forms key according to your need ['adminhtml_checkout','adminhtml_customer','adminhtml_customer_address','customer_account_edit','customer_address_edit','customer_register_address', 'customer_account_create']
            $attribute->setData('used_in_forms', ['adminhtml_customer',
                'adminhtml_checkout',
                'customer_account_create',
                'customer_account_edit'
            ]);
            $attribute->save();

        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {


            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'code_provider',
                [
                    'type' => 'int',
                    'frontend' => '',
                    'label' => 'Code of provider',
                    'input' => 'select',
                    'class' => '',
                    'group' => 'General',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'backend' => '',
                    'source' => '',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
                    'attribute_set_id' => '4',// product default
                    'note' => 'Code of provider'
                ]
            );

            $mockOptions = [
                'Гривна',
                'Доллар',
                'Рос. рубль',
                'USD Veloplaneta',
                'USD BBB',
                'USD Euroline',
                'Sportsummit ГРН',
                'USD Sporttop',
                'EUR Market',
                'USD Merida',
                'USD Scott',
                'USD Vysota',
                'PLU грн',
                'USD Tatu',
                'GRN SportDevice',
                'USD Avtomoto',
                'USD Kopyl',
                'USD Kore',
                'USD BMC',
                'USD Leader',
                'USD Giant',
                'USD Extrem',
                'USD Author',
                'USD Garbaruk',
                'USD Obod',
                'USD Odessa',
                'EUR Abus',
                'USD Kink',
                'Veloportal ГРН',
                'USD Smoove',
                'EUR EfettoMariposa',
                'Trailmech UAH'
            ];

            $attributeId = $this->_attributeRepository->get(
                \Magento\Catalog\Model\Product::ENTITY,
                'code_provider'
            )->getAttributeId();
//            echo $attributeId;
//            die();

            foreach ($mockOptions as $label) {

                $option = $this->_option->create();
                $labelObj = $this->_attributeOptionLabel->create();

                $labelObj->setStoreId(0);
                $labelObj->setLabel($label);
                $option->setLabel($labelObj);
                $option->setStoreLabels([$labelObj]);
                $option->setSortOrder(0);
                $option->setIsDefault(false);
                $option->setAttributeId($attributeId)
                    ->getResource()
                    ->save($option);
                $this->_attributeOptionManagement->add(\Magento\Catalog\Model\Product::ENTITY, 'code_provider', $option);
            }

        }


        if (version_compare($context->getVersion(), '1.0.5', '<')) {


            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'status_custom_stock',
                [
                    'type' => 'int',
                    'frontend' => '',
                    'label' => 'Stock status',
                    'input' => 'select',
                    'class' => '',
                    'group' => 'General',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'backend' => '',
                    'source' => '',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
                    'attribute_set_id' => '4',// product default
                    'note' => 'Code of provider'
                ]
            );

            $mockOptions = [
                'Есть на складе',
                'Есть в офисе',
                'Нет на складе',
                'Есть, срок доставки 2-14 дней',
                'Наличие уточняйте',
                'Снято с производства',
                'Под азказ 2-3 дня',
                'Есть, но отгрузка с 08.02',
                'Ожидается  поставка'
            ];

            $attributeId = $this->_attributeRepository->get(
                \Magento\Catalog\Model\Product::ENTITY,
                'status_custom_stock'
            )->getAttributeId();

            foreach ($mockOptions as $label) {

                $option = $this->_option->create();
                $labelObj = $this->_attributeOptionLabel->create();

                $labelObj->setStoreId(0);
                $labelObj->setLabel($label);
                $option->setLabel($labelObj);
                $option->setStoreLabels([$labelObj]);
                $option->setSortOrder(0);
                $option->setIsDefault(false);
                $option->setAttributeId($attributeId)
                    ->getResource()
                    ->save($option);
                $this->_attributeOptionManagement->add(\Magento\Catalog\Model\Product::ENTITY, 'status_custom_stock', $option);
            }

        }


        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'qty_custom_stock',
                [
                    'type' => 'int',
                    'frontend' => '',
                    'label' => 'Количество на локальном складе',
                    'input' => 'text',
                    'class' => '',
                    'group' => 'General',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'backend' => '',
                    'source' => '',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
                    'attribute_set_id' => '4',// product default

                ]
            );

            /*$mockOptions = [
                'Есть на складе',
                'Есть в офисе',
                'Нет на складе',
                'Есть, срок доставки 2-14 дней',
                'Наличие уточняйте',
                'Снято с производства',
                'Под азказ 2-3 дня',
                'Есть, но отгрузка с 08.02',
                'Ожидается  поставка'
            ];

            $attributeId = $this->_attributeRepository->get(
                \Magento\Catalog\Model\Product::ENTITY,
                'status_custom_stock'
            )->getAttributeId();

            foreach ($mockOptions as $label) {

                $option = $this->_option->create();
                $labelObj = $this->_attributeOptionLabel->create();

                $labelObj->setStoreId(0);
                $labelObj->setLabel($label);
                $option->setLabel($labelObj);
                $option->setStoreLabels([$labelObj]);
                $option->setSortOrder(0);
                $option->setIsDefault(false);
                $option->setAttributeId($attributeId)
                    ->getResource()
                    ->save($option);
                $this->_attributeOptionManagement->add(\Magento\Catalog\Model\Product::ENTITY, 'status_custom_stock', $option);
            }*/

        }




    }


}
