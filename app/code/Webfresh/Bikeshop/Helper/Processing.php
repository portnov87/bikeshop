<?php

namespace Webfresh\Bikeshop\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\HTTP\Client\Curl;

/**
 * Data Helper
 *
 * @package Webfresh\ThemeOptions\Helper
 */
class Processing extends AbstractHelper
{


    protected $attributeOptionManagement;
    protected $providersPriceRepository;

    /**
     * @var Curl
     */
    protected $_curl;
    /**
     * @var \Magento\Framework\Xml\Parser
     */
    private $parser;

    protected $_productRepository;
    protected $helperBikeshop;
    protected $collectionFactory;
    protected $_stockRegistryInterface;
    /**
     * Data constructor.
     *
     * @param \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroupColl
     * @param Context $context
     */
    public function __construct(
        Context $context,
        AttributeOptionManagementInterface $attributeOptionManagement,
        \Webfresh\Bikeshop\Api\ProvidersPriceRepositoryInterface $providersPriceRepository,
        \Magento\Framework\Xml\Parser $parser,
        Curl $curl,
        \Magento\Catalog\Model\ProductRepositoryFactory $productRepository,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $StockRegistryInterface
    )
    {
        $this->_stockRegistryInterface=$StockRegistryInterface;
        $this->_productRepository = $productRepository;
        $this->_curl = $curl;
        $this->parser = $parser;
        $this->providersPriceRepository = $providersPriceRepository;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->collectionFactory = $collectionFactory;
        $this->helperBikeshop=$helperBikeshop;

        parent::__construct($context);

    }
    public function getProductsByProvider($codeProvider)
    {
        $attributeCode='code_provider';
        $attributeCodeValue=false;
        $options = $this->attributeOptionManagement->getItems(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode
        );
        foreach ($options as $option) {
            if (trim($codeProvider != '')) {
                if (trim(strtoupper($option->getLabel())) == trim(strtoupper($codeProvider))) {
                    $attributeCodeValue= $option->getValue();
                }
            }
        }
        $productCollection=false;
        if ($attributeCodeValue) {
            // Use factory to create a new product collection
            $productCollection = $this->collectionFactory->create();
            /** Apply filters here */
            $productCollection->addAttributeToSelect('*');
            $productCollection->addFieldToFilter('code_provider', $attributeCodeValue);
        }
        return $productCollection;
    }

    public function clearStocks($codeProvider)
    {
        $i=0;
        $productCollection=$this->getProductsByProvider($codeProvider);
        $attribute_code_custom_stock = 'status_custom_stock';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
        //$codeStockValue = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Нет на складе');

        $codeStockValueOfice = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть в офисе');
        $codeStockValueNoSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Нет на складе');

        $codeStockValueYesSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть на складе');

        foreach ($productCollection as $product)
        {
            $i++;
            $stockQty= $StockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
            $StatusCustomStock=$product->getStatusCustomStock();
            switch ($StatusCustomStock)
            {
                case $codeStockValueOfice:

                    $qty_custom_stock=$product->getQtyCustomStock();
                    if  (!empty($qty_custom_stock)&&($qty_custom_stock > 0)) {
                        continue;


                    }
                    $product->setStatusCustomStock($codeStockValueNoSklad);
                    $product->setStockData(['qty' => 0, 'is_in_stock' => false]);
                    $product->save();

                    break;
                default:

                    if  ($stockQty > 0) {
                        $product->setStatusCustomStock($codeStockValueNoSklad);
                        $product->setStockData(['qty' => 0, 'is_in_stock' => false]);
                        //$product->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => false]);
                        $product->save();

                        /*$sku=$product->getSku();
                        $stockRegistry = $this->_stockRegistryInterface;//$objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface');
                        $stockItem = $stockRegistry->getStockItemBySku($sku);
                        $stockItem->setQty(0);
                        $stockItem->setIsInStock(false);
                        $stockRegistry->updateStockItemBySku($sku, $stockItem);*/
                    }
                    break;
            }

        }
        return count($productCollection);

    }

    public function processProvidersById($providerId)
    {
        $provider = $this->providersPriceRepository->getById($providerId);
        if ($provider) {
            $providerCode = $provider->getProviderCode();
            $link = $provider->getLink();


            if ($link != '') {
                $this->clearStocks($providerCode);
                echo "clearStocks ".$providerCode."\r\n\r\n";
                $function = 'processProvider_' . $providerCode;
                return $this->{$function}($provider);
            }
        }
        return false;

    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->create()->get($sku);
    }


    /**
     * Get data from TM Api
     *
     * @param $url
     * @return string
     */
    protected function parserUrl($url)
    {
        $ch = $this->_curl;
        //$ch->setHeaders(array("Content-Type: application/json"));
        $ch->get($url);

        return $ch->getBody();
    }


    private function getUrl($link)
    {

        $bodyXml = $this->parserUrl($link);

        if (!empty($bodyXml)) {

            $parsedArray = $this->parser->loadXML($bodyXml)->xmlToArray();

            return $parsedArray;
        }
        return false;


    }

    private function updateProduct($sku, $is_in_stock, $qty, $price=false)
    {
        try {
            $product = $this->getProductBySku($sku);
            if ($product) {
                $attribute_code_custom_stock = 'status_custom_stock';
                if ($price)
                    $product->setPrice($price);
                $qty_custom_stock=$product->getQtyCustomStock();
                if ((!$is_in_stock)&&($qty_custom_stock>0))
                {
                    $is_in_stock=true;
                    $codeStockValueOfice = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть в офисе');
                    $product->setStatusCustomStock($codeStockValueOfice);
                }
                elseif ($is_in_stock) {
                    $codeStockValueYesSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть на складе');
                    $product->setStatusCustomStock($codeStockValueYesSklad);
                }
//                else
//                    $codeStockValueNoSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Нет на складе');
//                    $codeStockValueYesSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть на складе');
                if ($is_in_stock) {
                    if ($qty>0)
                        $product->setStockData(['qty' => $qty, 'is_in_stock' => 1]);
                    else
                        $product->setStockData(['qty' => $qty, 'is_in_stock' => 0]);
                }
                else
                    $product->setStockData(['qty' => $qty, 'is_in_stock' => 0]);

                //$product->setQuantityAndStockStatus(['qty' => $qty, 'is_in_stock' => $is_in_stock]);

                $product->save();

                $stockItem = $this->_stockRegistryInterface->getStockItemBySku($sku);
                $stockItem->setQty($qty);
                if ($is_in_stock)
                    $stockItem->setData('is_in_stock', 1);
                else
                    $stockItem->setData('is_in_stock', 0);
                //$stockItem->setIsInStock($is_in_stock);
                $this->_stockRegistryInterface->updateStockItemBySku($sku, $stockItem);

                 echo 'sku = ' . $sku . ' $price=' . $price . ' $qty=' . $qty . ' is_in_stock='.$is_in_stock."\r\n\r\n";
                return true;//$this->_productRepository->create()->save($product);
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            //continue;
            return false;
        }
        catch (\Exception $e) {
            return false;
        }

        return false;
    }

    private function processProvider_plu($provider)
    {
        $link = $provider->getLink();

        $ArrayBody = $this->getUrl($link);
        if ($ArrayBody) {

            if (isset($ArrayBody['stock'])) {

                $items = (isset($ArrayBody['stock']['_value']['goods']['item']) ? $ArrayBody['stock']['_value']['goods']['item'] : []);
                foreach ($items as $item) {

                    $value = $item['_value'];
                    $sku = $value['code'];
                    if (!empty($sku)) {
                        $is_in_stock = ($value['is_in_stock'] > 0 ? true : false);
                        $qty = $value['is_in_stock'];
                        $price = $value['price'];

                        if ($is_in_stock)
                            $this->updateProduct($sku, $is_in_stock, $qty, $price);

                    }
                }
                return true;
            }
        }
        return false;

    }

    private function processProvider_obod($provider)
    {
        $link = $provider->getLink();
        $ArrayBody = $this->getUrl($link);

        if ($ArrayBody) {
            if (isset($ArrayBody['yml_catalog'])) {

                $offers = $ArrayBody['yml_catalog']['_value']['shop']['offers']['offer'];//['_value'];
                foreach ($offers as $key => $offer) {

                    $value = $offer['_value'];
                    $is_in_stock = $offer['_attribute']['available'];


                    $sku = $value['vendorCode'];
                    $qty = $value['stock_quantity'];
                    $price = $value['price'];
                    //echo $sku.' $qty='.$qty.' $price='.$price."\r\n\r\n";
                    if ($is_in_stock)
                     $this->updateProduct($sku, $is_in_stock, $qty, $price);
                }
                return true;
            }


        }
        return false;
    }

    private function processProvider_veloplaneta($provider)
    {
        $link = $provider->getLink();

        $ArrayBody = $this->getUrl($link);



        if ($ArrayBody) {
            if (isset($ArrayBody['yml_catalog'])) {

                $offers = $ArrayBody['yml_catalog']['_value']['shop']['offers']['offer'];

                foreach ($offers as $key => $offer) {

                    $value = $offer['_value'];
                    if (!$offer['_attribute']['available'])
                        $is_in_stock = false;
                    elseif ($offer['_attribute']['available']=='false')
                        $is_in_stock = false;
                    else
                        $is_in_stock = true;



                    $sku = $value['vendorCode'];
                    $qty = (int)$value['stock_quantity'];

                    $price=0;
                    if (is_array($value['price'])){
                        foreach ($value['price'] as $pr)
                        {


                            if ($pr['_attribute']['name']=='Розн. со скидкой, грн')
                            {
                                $price=$pr['_value'];
                                break;
                            }
                            if ($pr['_attribute']['name']=='Цена розн. ₴')
                            {
                                $price=$pr['_value'];
                                break;
                            }
                            if ($pr['_attribute']['name']=='Цена розн. ₴, %')
                            {
                                $price=$pr['_value'];
                                break;
                            }

                        }
                    }
                    else
                        $price = $value['price'];

                   // echo ' sku '.$sku.' is_in_stock='.$is_in_stock.' $qty = '.$qty." price = ".$price."\r\n\r\n";

//                    if ($is_in_stock=='true')
//                        $is_in_stock=true;
//                    else
//                        $is_in_stock=false;


                    //if ($sku=='SKD-74-04') {
                        if ($price > 0) {
                            //if ($is_in_stock) {
                            if ($qty>0)
                                $is_in_stock=true;


                             //   echo 'updateProduct';
                                $this->updateProduct($sku, $is_in_stock, $qty, $price);
                            //}

//                            echo $sku.' $is_in_stock='.$is_in_stock.' $qty='.$qty.' $price='.$price."\r\n";
//                            echo '<pre>$offer';
//                            print_r($offer);
//                            echo '</pre>';


                        }
                    //}
                }
                return true;
            }


        }
        return false;
    }

    private function processProvider_sportsummit($provider)
    {
//        echo  'processProvider_sportsummit';
//        die();
        $link = $provider->getLink();

        //  echo '$link'.$link;die();
        $ArrayBody = $this->getUrl($link);
//        echo '<pre>';
//        print_r($ArrayBody);
//        echo '</pre>';
//        die();
        if ($ArrayBody) {
            if (isset($ArrayBody['Товары'])) {


//                foreach ($ArrayBody['Товары']['_value']['item'] as $key => $_offer) {
//                    echo '<pre>';
//                    print_r($_offer);
//                    echo '</pre>'.$key;
//                    die();
//                }
                $offers = $ArrayBody['Товары']['_value']['item'];//['offers']['offer'];//['_value'];
                foreach ($offers as $key => $offer) {


                    if (isset($offer['_value']))
                        $value = $offer['_value'];
                    else $value=$offer;
                    $is_in_stock =($value['nalichie']=='N'?false:true);// $offer['_attribute']['available'];

                    $sku = $value['artikul'];
                    $qty = $value['Balance'];//($value['Balance']>0);//($value['nalichie']=='N'?false:true);
                    $price = (isset($value['BASEАкционная'])?$value['BASEАкционная']:$value['BASE']);
                    //$value['BASE'];
                    if ($is_in_stock)
                    $this->updateProduct($sku, $is_in_stock, $qty, $price);
                }
                return true;
            }


        }
        return false;
    }

    private function processProvider_vysota($provider)
    {
        $link = $provider->getLink();

        $ArrayBody = $this->getUrl($link);

        if ($ArrayBody) {
            if (isset($ArrayBody['DATAPACKET'])) {


                $offers = $ArrayBody['DATAPACKET']['PRODUCT'];//['item'];//['offers']['offer'];//['_value'];
                foreach ($offers as $key => $offer) {

                    $is_in_stock =($offer['STOCK']>0?true:false);// $offer['_attribute']['available'];

                    $sku = $offer['ARTICLE'];
                    $qty = $offer['STOCK'];//($value['Balance']>0);//($value['nalichie']=='N'?false:true);

//                    echo '<pre>$offer';
//                    print_r($offer);
//                    echo '</pre>';
                    $price=false;
                    if (isset($offer['PRICE_RRC_UAH'])) {
                        $price = $offer['PRICE_RRC_UAH'];
                        //$value['BASE'];
                    }
                    if ($is_in_stock)
                        $this->updateProduct($sku, $is_in_stock, $qty, $price);

                }
                return true;
            }


        }
        return false;
    }

    private function processProvider_veloportal($provider)
    {
        $link = $provider->getLink();

        $ArrayBody = $this->getUrl($link);

        if ($ArrayBody) {
            if (isset($ArrayBody['catalog'])) {


                $items = (isset($ArrayBody['catalog']['goods']['item']) ? $ArrayBody['catalog']['goods']['item'] : []);

                foreach ($items as $key => $offer) {
                    $is_in_stock =($offer['is_in_stock']>0?true:false);// $offer['_attribute']['available'];

                    $sku = $offer['article'];
                    $qty = ($offer['is_in_stock']>0?10:0);

                    $price = $offer['price_r'];

                    if ($is_in_stock)
                    $this->updateProduct($sku, $is_in_stock, $qty, $price);
                }
                return true;
            }

        }
        return false;
    }

    private function processProvider_scott($provider)
    {
        $link = $provider->getLink();

        $ArrayBody = $this->getUrl($link);

//        echo '<pre>';
//        print_r($ArrayBody);
//        echo '</pre>';die();
        if ($ArrayBody) {
            if (isset($ArrayBody['price'])) {

                $items = (isset($ArrayBody['price']['items']['item']) ? $ArrayBody['price']['items']['item'] : []);

                foreach ($items as $key => $offer) {
                    //$is_in_stock=(($offer['stock'])?(int)$offer['stock']:0);
                    //$qty=(isset($offer['stock'])?(int)$offer['stock']:0);
                    $qty=99;//(isset($offer['stock'])?(int)$offer['stock']:0);
                    //шт
                    if ($offer['stock']=='В наличии')
                    {
                        $is_in_stock=true;
                    }elseif ($offer['stock']=='Под заказ'){

                        $is_in_stock=false;
                    }
                    else
                        $is_in_stock=true;

                    //$is_in_stock=($qty>0?true:false);

                    //$is_in_stock =($offer['is_in_stock']>0?true:false);// $offer['_attribute']['available'];

                    $sku = $offer['code'];
                    //$qty = ($offer['is_in_stock']>0?10:0);

                    $price = (isset($offer['price_specialRUAH'])?$offer['price_specialRUAH']:$offer['priceRUAH']);
                    //
                    if ($is_in_stock)
                        $this->updateProduct($sku, $is_in_stock, $qty, $price);
                }
                return true;
            }

        }
        return false;
    }

    private function processProvider_abus($provider)
    {
        $link = $provider->getLink();

        $ArrayBody = $this->getUrl($link);
//        echo '<pre>';
//        print_r($ArrayBody);
//        echo '</pre>';die();
        if ($ArrayBody) {
            if (isset($ArrayBody['List'])) {

//                echo '<pre>';
//                print_r($ArrayBody['List']['Item']);
//                echo '</pre>';die();
                $items = (isset($ArrayBody['List']['Item']) ? $ArrayBody['List']['Item'] : []);

                foreach ($items as $key => $offer) {
                    $is_in_stock =($offer['Balance']>0?true:false);// $offer['_attribute']['available'];

                    $sku = $offer['Article'];
                    $qty = $offer['Balance'];//($offer['Balance']>0?10:0);

                    $price = $offer['PriceUAH'];
                    if ($is_in_stock)
                    $this->updateProduct($sku, $is_in_stock, $qty, $price);
                }
                return true;
            }

        }
        return false;

    }

    private function processProvider_kopyl($provider)
    {



        // Инициализация данных для запроса
        // ссылка для получения файла
        $url = $provider->getLink();//"https://kopylbros.com/dealer/automation/2.0/products_info.csv";
        if (!empty($url)) {

            // логин к аккануту на kopylbros.com
            $username = "info@bikeshop.com.ua";
            // пароль к аккануту на kopylbros.com
            $password = "238D96RG";

            // формирование CURL-запроса
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            // выполнение запроса
            $result_data = curl_exec($ch);
            $result_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // проверка результата выполнения запроса.
            if ($result_code == 200) { //  Код 200 будет получен в случае успешного получения данных

                // разделение данных по строкам
                $csv_lines = explode("\n", $result_data);
                if (!is_array($csv_lines)) {
                    return false;

                }

                $result = array();
                // разбор каждой строки CSV
                foreach ($csv_lines as $line) {
                    $result = str_getcsv($line);

                    $is_in_stock = ($result[9] > 0 ? true : false);// $offer['_attribute']['available'];

                    $sku = $result[0];
                    $qty = ($result[9] > 0 ? $result[9] : 0);

                    $price = $result[12];
                    if ($is_in_stock)
                    $this->updateProduct($sku, $is_in_stock, $qty, $price);
                }


//                echo '<pre>$result';
//                print_r($result);
//                echo '</pre>';
//                die();
                /*foreach ($result as $res) {

//                - Артикул
//                - Остаток по складу
//                - Цена опт, $
//- Цена розница, $
//- Цена розница, грн
                    $is_in_stock = ($offer['Остаток по складу'] > 0 ? true : false);// $offer['_attribute']['available'];

                    $sku = $offer['Артикул'];
                    $qty = ($offer['Остаток по складу'] > 0 ? 10 : 0);

                    $price = $offer['Цена розница, грн'];

                    $this->updateProduct($sku, $is_in_stock, $qty, $price);
                }
*/
                // массив с разобранными данными лежит в $result
                //var_dump($result);
                return true;


            } else {
                return false;
                // обработка ошибок
//            echo "Cannot get result.<br>\n";
//            echo "HTTP response code: <b>$result_code</b><br>\n";
//            echo "Response data: <b>$result_data</b><br>\n";
//            exit();
            }

        }

        return false;
       /* $link = $provider->getLink();

        $ArrayBody = $this->getUrl($link);
//        echo '<pre>';
//        print_r($ArrayBody);
//        echo '</pre>';die();
        if ($ArrayBody) {
            if (isset($ArrayBody['price'])) {

//                echo '<pre>';
//                print_r($ArrayBody['price']['items']);
//                echo '</pre>';die();
                $items = (isset($ArrayBody['price']['items']['item']) ? $ArrayBody['catalog']['goods']['item'] : []);

                foreach ($items as $key => $offer) {
                    $is_in_stock =($offer['is_in_stock']>0?true:false);// $offer['_attribute']['available'];

                    $sku = $offer['article'];
                    $qty = ($offer['is_in_stock']>0?10:0);

                    $price = $offer['price_r'];

                    $this->updateProduct($sku, $is_in_stock, $qty, $price);
                }
                return true;
            }

        }
        return  false;*/
    }

}
