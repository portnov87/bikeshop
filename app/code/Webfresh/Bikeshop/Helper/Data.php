<?php

namespace Webfresh\Bikeshop\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Framework\App\Helper\AbstractHelper;
/**
 * Data Helper
 *
 * @package Webfresh\ThemeOptions\Helper
 */
class Data extends AbstractHelper
{


    protected $attributeOptionManagement;
    //protected  $providersPriceRepository;

    /**
     * Data constructor.
     *
     * @param \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroupColl
     * @param Context $context
     */
    public function __construct(

        Context $context,
        AttributeOptionManagementInterface $attributeOptionManagement
        //Webfresh\Bikeshop\Api\ProvidersPriceRepositoryInterface $providersPriceRepository
    )
    {
        //$this->providersPriceRepository=$providersPriceRepository;
        $this->attributeOptionManagement = $attributeOptionManagement;
        parent::__construct($context);

    }

    /**
     * @param $attributeCode
     * @param $attribute_label
     * @return bool|string
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    public
    function getAttributeValueByLabel($attributeCode, $attribute_label)
    {
        $options = $this->attributeOptionManagement->getItems(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode
        );
        foreach ($options as $option) {
            if (trim($attribute_label != '')) {
                if (trim(strtoupper($option->getLabel())) == trim(strtoupper($attribute_label))) {
                    return $option->getValue();
                }
            }
        }

        return false;

    }
    /**
     * @param $attributeCode
     * @param $attribute_label
     * @return bool|string
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    public
    function getAttributeLabelByValue($attributeCode, $attribute_value)
    {
        $options = $this->attributeOptionManagement->getItems(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode
        );
        foreach ($options as $option) {
            if (trim($attribute_value != '')) {
                if (trim(strtoupper($option->getValue())) == trim(strtoupper($attribute_value))) {
                    return $option->getLabel();
                }
            }
        }

        return false;

    }


}
