<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 25.12.2019
 * Time: 15:53
 */


namespace Webfresh\Bikeshop\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\UrlRewrite\Model\UrlRewriteFactory;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Webkul Marketplace CustomerRegisterSuccessObserver Observer.
 */
class RedirectStore implements ObserverInterface
{
    protected $storeManager;
    protected $customerSession;
    protected $resultRedirect;
    protected $resultFactory;
    protected $currentCustomer;

    protected $orderFactory;

    protected $logger;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;


    protected $resultPageFactory;
    protected $retailerRepository;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        ResultFactory $resultFactory,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Sales\Model\OrderFactory $orderModel,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
    )
    {
        $this->resultRedirectFactory=$redirectFactory;

        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession = $session;
        $this->storeManager = $storeManager;
        $this->resultFactory = $resultFactory;
        $this->responseFactory = $responseFactory;

        $this->currentCustomer = $currentCustomer;

        $this->_messageManager = $messageManager;
        $this->orderFactory = $orderModel;
        $this->logger = $logger;
    }


    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    protected function _buildUrl($route = '', $params = [])
    {
        /** @var \Magento\Framework\UrlInterface $urlBuilder */
        $urlBuilder = $this->_objectManager->create(\Magento\Framework\UrlInterface::class);
        return $urlBuilder->getUrl($route, $params);
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $url = $_SERVER['REQUEST_URI'];

        if ((strpos($url, 'admin')===false)&&
            (strpos($url, 'social')===false)&&
            (strpos($url, 'quickView')===false)&&
            (strpos($url, 'checkout')===false)&&
            (strpos($url, 'customer/section/load')===false)&&
            (strpos($url, '/rest/')===false)&&
            (strpos($url, '/catalogsearch')===false)&&
            (strpos($url, 'uenc')===false)&&

            (strpos($url, 'ajax')===false)) {
            if (strpos($url, 'index.php') !== false) {

                if ($url == '/index.php') {
                    $new_url = str_replace('/index.php', '/', $url);
                } else {

                    $new_url = str_replace('/index.php', '', $url);

                }
                if ($new_url != $url) {
                     $this->responseFactory->create()->setRedirect($new_url)->sendResponse();
                }
            }

            if (strpos($url, '.html/') !== false) {
                $new_url = str_replace('.html/', '.html', $url);
                if ($new_url != $url) {
                   $this->responseFactory->create()->setRedirect($new_url)->sendResponse();
                }
            }
            $new_url=strtolower($url);
            if ($new_url!=$url)
            {
                    $this->responseFactory->create()->setRedirect($new_url)->sendResponse();

            }

        }


        return $this;

    }
}