<?php

namespace Webfresh\Bikeshop\Observer;

use Magento\Framework\Event\ObserverInterface;

class CreateOrderObserver implements ObserverInterface
{
    protected $state;
    protected $_orderFactory;
    protected $helperBikeshop;
    protected $_productRepository;

    public function __construct(

        \Magento\Framework\App\State $state,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop,
        \Magento\Catalog\Model\ProductRepository $productRepository
    )
    {
        $this->_orderFactory = $orderFactory;
        $this->helperBikeshop=$helperBikeshop;
        $this->_productRepository = $productRepository;
        $this->state = $state;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data = $observer->getData();

        $order = $data['order'];
        $quote = $data['quote'];
        $attribute_code_custom_stock = 'status_custom_stock';
        $cntProducts=0;
        $codeStockValueOfice = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть в офисе');
        $codeStockValueNoSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Нет на складе');
        $codeStockValueYesSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть на складе');


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');

        $orderItems = $order->getItems();
        foreach ($orderItems as $item) {
            $qtyOrder=$item->getQtyOrdered();
            $product=$item->getProduct();
            if ($product) {

                $stockQty = $StockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());

                $qtyCustomStock = $product->getQtyCustomStock();
                $qtyCustomStockNew = $qtyCustomStock - $qtyOrder;
                if ($qtyCustomStockNew <= 0) {
                    $product->setQtyCustomStock(0);
                    if ($stockQty <= 2000) {
                        $product->setStatusCustomStock($codeStockValueNoSklad);

                        $product->setStockData(['qty' => 0, 'is_in_stock' => false]);
                        //$product->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => false]);

                    } else {
                        $product->setStatusCustomStock($codeStockValueYesSklad);
                    }

                } else {
                    $product->setQtyCustomStock($qtyCustomStockNew);
                    $product->setStatusCustomStock($codeStockValueOfice);
                }

                $this->_productRepository->save($product);
            }


        }


    }



}