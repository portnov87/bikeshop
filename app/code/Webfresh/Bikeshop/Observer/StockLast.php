<?php

namespace Webfresh\Bikeshop\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Catalog\Block\Product\ProductList\Toolbar as CoreToolbar;

class StockLast implements ObserverInterface
{
    protected $scopeConfig;
    protected $_storeManager;
    protected $coreToolbar;

    public function __construct(ScopeConfigInterface $scopeConfig, StoreManagerInterface $storeManager, CoreToolbar $toolbar)
    {
        $this->scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->coreToolbar = $toolbar;
    }

    public
    function execute(Observer $observer)
    {
        $collection = $observer->getEvent()->getData('collection');
        try {
            $websiteId = 0;
            $stockId = 'stock_id';
            $collection->getSelect()->joinLeft(
                array('_inv' => $collection->getResource()->getTable('cataloginventory_stock_status')),
                "_inv.product_id = e.entity_id and _inv.website_id=$websiteId",
                array('stock_status')
            );

            $collection->getSelect()->joinLeft(
                array('_localqty' => $collection->getResource()->getTable('catalog_product_entity_int')),
                "_localqty.entity_id = e.entity_id AND _localqty.attribute_id=361",
                array('value')
            );

            //361

            $collection->addExpressionAttributeToSelect('local_stock', 'IF(_localqty.value IS NULL,0,_localqty.value)', array());
            $collection->addExpressionAttributeToSelect('in_stock', 'IFNULL(_inv.stock_status,0)', array());
            $collection->getSelect()->reset('order');


            $collection->getSelect()->order('in_stock DESC');
            $collection->getSelect()->order('local_stock DESC');
            $collection->getSelect()->order('_inv.qty DESC');
            //code for Filter Price Low to High or High to Low with stock filter.

            if ($this->coreToolbar->getCurrentOrder() == 'price') {

                $direction = $this->coreToolbar->getCurrentDirection();
                $collection->getSelect()->order("min_price $direction");
            }
            $collection->getSelect()->group('e.entity_id');

        } catch (\Exception $e) {
        }
        return $this;
    }
}