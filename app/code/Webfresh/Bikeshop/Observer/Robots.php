<?php

namespace Webfresh\Bikeshop\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;

class Robots implements ObserverInterface
{

    protected $request;

    protected $layoutFactory;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Page\Config $layoutFactory
    )
    {
        $this->request = $request;
        $this->layoutFactory = $layoutFactory;
    }


    public function execute(Observer $observer)
    {

        $fullActionName = $this->request->getFullActionName();

        $url=$this->request->getOriginalPathInfo();
        $url=$_SERVER['REQUEST_URI'];;

        if (
            (strpos($url, 'product_list_mode')!==false)
        ||(strpos($url, 'product_list_limit')!==false)
            ||(strpos($url, 'product_list_order')!==false)
        ) {
            $this->layoutFactory->setRobots('NOINDEX,NOFOLLOW');
        }
        else if (
            (strpos($url, 'p=')!==false)
        ) {
            $this->layoutFactory->setRobots('NOINDEX,FOLLOW');
        }


//        if ($fullActionName == "catalog_product_view") {
//            $this->layoutFactory->setRobots('NOINDEX,NOFOLLOW');
//        }
    }
}