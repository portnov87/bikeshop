<?php

namespace Webfresh\Bikeshop\Controller\Adminhtml\Providers;

use Webfresh\Bikeshop\Api\ProvidersPriceRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

/**
 * Delete brand action.
 *
 * @package TemplateMonster\ShopByBrand\Controller\Adminhtml\Index
 */
class Delete extends Action
{
    const ADMIN_RESOURCE = 'Webfresh_Bikeshop::provider_delete';

    /**
     * @var BrandRepositoryInterface
     */
    protected $_providerRepository;

    /**
     * Delete constructor.
     *
     * @param BrandRepositoryInterface $brandRepository
     * @param Action\Context           $context
     */
    public function __construct(
        ProvidersPriceRepositoryInterface $providerRepository,
        Action\Context $context
    )
    {
        $this->_providerRepository = $providerRepository;
        parent::__construct($context);
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');

        try {
            $this->_providerRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(
                __('Provider has been successfully deleted.')
            );
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('There is an unknown error occurred while deleting the item.')
            );
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('*/*');

        return $resultRedirect;
    }
}
