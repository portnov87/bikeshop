<?php

namespace Webfresh\Bikeshop\Controller\Adminhtml\Providers;

use Webfresh\Bikeshop\Api\ProvidersPriceRepositoryInterface;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry as CoreRegistry;

/**
 * Brand edit action.
 *
 * @package TemplateMonster\ShopByBrand\Controller\Adminhtml\Index
 */
class Edit extends Action
{
    const ADMIN_RESOURCE = 'Webfresh_Bikeshop::providers_save';

    /**
     * @var BrandRepositoryInterface
     */
    protected $_providersRepository;

    /**
     * @var CoreRegistry
     */
    protected $_coreRegistry;

    public function __construct(
        ProvidersPriceRepositoryInterface $providersRepository,
        CoreRegistry $coreRegistry,
        Context $context
    )
    {
        $this->_providersRepository = $providersRepository;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('provider_id');

        if ($id) {
            try {
                $model = $this->_providersRepository->getById($id);
                $this->_coreRegistry->register('provider', $model);
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('This brand no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        // 5. Build edit form
        $resultPage = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Provider') : __('New Provider'),
            $id ? __('Edit Provider') : __('New Provider')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Provider'));
        if (isset($model)) {
            $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Provider'));
        }

        return $resultPage;
    }

    /**
     * Init page.
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Webfresh_Bikeshop::provider')
            ->addBreadcrumb(__('Provider'), __('Provider'))
            ->addBreadcrumb(__('Provider'), __('Provider'));

        return $resultPage;
    }
}
