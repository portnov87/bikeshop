<?php

namespace Webfresh\Bikeshop\Controller\Adminhtml\Providers;

use Magento\Backend\App\Action;

/**
 * New brand action.
 */
class NewAction extends Action
{
    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
