<?php

namespace Webfresh\Bikeshop\Controller\Adminhtml\Providers;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Webfresh_Bikeshop::providersprice';

    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $this->initPage($resultPage)->getConfig()->getTitle()->prepend(__('Providers'));

        $dataPersistor = $this->_objectManager->get('Magento\Framework\App\Request\DataPersistorInterface');
        $dataPersistor->clear('brand');


        return $resultPage;
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Webfresh_Bikeshop::providersprice')
            ->addBreadcrumb(__('Providers'), __('Providers'))
            ->addBreadcrumb(__('Providers'), __('Providers'));

        return $resultPage;
    }

}