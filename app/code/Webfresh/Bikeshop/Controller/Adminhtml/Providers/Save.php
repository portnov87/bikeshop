<?php
/**
 * Copyright © 2015 TemplateMonster. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webfresh\Bikeshop\Controller\Adminhtml\Providers;

use Magento\Framework\Exception\LocalizedException;
use TemplateMonster\ShopByBrand\Api\Data\BrandInterface;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Webfresh_Bikeshop::providers_save';

    /**
     * @var \TemplateMonster\ShopByBrand\Api\Data\BrandInterfaceFactory
     */
    protected $_provider;

    /**
     * @var \TemplateMonster\ShopByBrand\Model\ProvidersPriceRepository
     */
    protected $_providerRepository;

    /**
     * @var DataPersistorInterface
     */
    protected $_dataPersistor;


    protected $_cacheTypeList;

    protected $helperBikeshop;
    protected $_attributeRepository;

    public function __construct(
        \Webfresh\Bikeshop\Api\Data\ProvidersPriceInterfaceFactory $provider,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Webfresh\Bikeshop\Model\ProvidersPriceRepository $providerRepository,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Backend\App\Action\Context $context,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop,
        \Magento\Eav\Model\AttributeRepository $attributeRepository
    ) {
        $this->_provider = $provider;
        $this->_dataPersistor = $dataPersistor;
        $this->_providerRepository = $providerRepository;

        $this->helperBikeshop=$helperBikeshop;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_attributeRepository = $attributeRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_provider->create();
            $id = $this->getRequest()->getParam('entity_id');
            if ($id) {
                $model->load($id);
            }
            //Prepare image to save
//            $data = $this->imagePreprocessing($data);
            //$data = $this->_filterCategoryPostData($data);

            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            $model->addData($data);

            $providerId=$data['attribute_value_id'];
            $model->setAttributeValueId($providerId);//addData($data);
            $attributeId = $this->_attributeRepository->get(
                \Magento\Catalog\Model\Product::ENTITY,
                'code_provider'
            )->getAttributeId();

            $model->setAttributeId($attributeId);


            //$attributeCode=
            $_attributeCode='code_provider';

            //$providerName=$this->helperBikeshop->getAttributeLabelByValue($_attributeCode, $providerId);
            //$model->setProviderCode($providerName);

//            $this->_eventManager->dispatch(
//                'brand_prepare_save',
//                ['brand' => $model, 'request' => $this->getRequest()]
//            );

            try {
                //$model->save();
                $this->_providerRepository->save($model);
                $this->_cacheTypeList->invalidate('full_page');
                $this->messageManager->addSuccessMessage(__('You saved the provider.'));
                $this->_dataPersistor->clear('provider');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId(), '_current' => true]);
                }

                return $resultRedirect->setPath('bikeshop/providers/index');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {

                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the provider.'));
            }

            $this->_dataPersistor->set('provider', $data);

            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }

        return $resultRedirect->setPath('bikeshop/providers/index');
    }


}
