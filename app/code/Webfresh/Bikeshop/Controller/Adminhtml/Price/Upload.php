<?php

namespace Webfresh\Bikeshop\Controller\Adminhtml\Price;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Upload extends Action
{

    protected $_uploaderFactory;
    protected $_varDirectory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $_uploaderFactory,
        array $data = []
    )
    {
        $this->_uploaderFactory = $_uploaderFactory;
        $this->_varDirectory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }


    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $result = $this->saveFile();//uploader->saveFileToTmpDir('banner_image');
            if (count($result)) {
                $result['cookie'] = [
                    'name' => $this->_getSession()->getName(),
                    'value' => $this->_getSession()->getSessionId(),
                    'lifetime' => $this->_getSession()->getCookieLifetime(),
                    'path' => $this->_getSession()->getCookiePath(),
                    'domain' => $this->_getSession()->getCookieDomain(),
                ];
            }
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }


    /**
     * Save Image or CSV file to var/importexport folder
     */
    public function saveFile()
    {
        $result=[];

        if(isset($_FILES['file']['name'])) {
            $uploader = $this->_uploaderFactory->create(['fileId' => 'file']);
            $workingDir = $this->_varDirectory->getAbsolutePath('var');
            $result = $uploader->save($workingDir);

        }
        return $result;
    }



}