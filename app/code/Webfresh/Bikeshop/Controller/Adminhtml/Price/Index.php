<?php

namespace Webfresh\Bikeshop\Controller\Adminhtml\Price;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Webfresh_Bikeshop::brand';

    protected $csv;
    protected $_productRepository;
    protected $collectionFactory;
    protected $productStockRepository;
    protected $productStockRegistry;
    protected $helperBikeshop;
    protected $_stockRegistryInterface;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $_uploaderFactory,
        \Magento\Framework\File\Csv $csv,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\CatalogInventory\Api\StockRepositoryInterface $productStockRepository,
        \Magento\CatalogInventory\Model\StockRegistry $productStockRegistry,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop,
        \Magento\CatalogInventory\Api\StockRegistryInterface $StockRegistryInterface
        //array $data = []
    )
    {
        $this->csv = $csv;
        $this->_stockRegistryInterface=$StockRegistryInterface;
        $this->_productRepository = $productRepository;
        $this->collectionFactory = $collectionFactory;
        $this->productStockRepository = $productStockRepository;

        $this->helperBikeshop=$helperBikeshop;

        $this->productStockRegistry =  $productStockRegistry;
        parent::__construct($context);
    }



    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $data = $this->getRequest()->getPostValue();
        if ($data)
        {

            $resultProcess=$this->process($data);
            $clear_products=$resultProcess['clear_products'];
            $count_products=$resultProcess['count_products'];

            $this->messageManager->addSuccessMessage(
                __('All Count of provider ='.$clear_products.'.  Update price and stock = '.$count_products)
            );

//            echo '<pre>';
//            print_r($data);
//            echo '</pre>';
//            die();
        }

        return $resultPage;
    }

    public function getProductsByProvider($codeProvider)
    {
        // Use factory to create a new product collection
        $productCollection = $this->collectionFactory->create();
        /** Apply filters here */
        $productCollection->addAttributeToSelect('*');
        $productCollection->addFieldToFilter('code_provider',$codeProvider);
        // Don't have to do this
        // $productCollection->load();


        return $productCollection;
        //return $this->_productRepository->get($sku);

    }


    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }

    public function processLocalStock($data)
    {

        $file = $data['file'];
        $filepath = $file[0]['path'].'/'.$file[0]['file'];

        $csvData = $this->csv->getData($filepath);
        $attribute_code = 'code_provider';
        $attribute_code_custom_stock = 'status_custom_stock';

        //$countClear=$this->clearStocks($codeProvider);

        $cntProducts=0;
        $codeStockValueOfice = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть в офисе');
        $codeStockValueNoSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Нет на складе');

        $codeStockValueYesSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть на складе');



        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');



        foreach ($csvData as $row => $data) {
            try {
                if ($row > 0) {

                    //Start your work
                    $sku = $data[0];
                    if ($sku!='') {
                        $price = (isset($data[1])?$data[1]:false);
                        $stock=(isset($data[2])?$data[2]:false);


                        $product = $this->getProductBySku($sku);
                        if ($product) {
                            $store = 0;
                            $product->setStoreId($store);
                            if ($price)
                                $product->setPrice($price);

                            if ($stock) {
                                $stockQty= $StockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
                                if ($stock>0) {
                                    $product->setStatusCustomStock($codeStockValueOfice);

                                    //if ($stockQty==0) {
                                        $product->setStockData(['qty' => $stock , 'is_in_stock' => true]);

                                    $stockItem = $this->_stockRegistryInterface->getStockItemBySku($sku);
                                    $stockItem->setQty($stock);

                                        $stockItem->setData('is_in_stock', 1);

                                    $this->_stockRegistryInterface->updateStockItemBySku($sku, $stockItem);



                                    //$product->setQuantityAndStockStatus(['qty' => 2000, 'is_in_stock' => true]);
                                    //}
                                    $product->setQtyCustomStock($stock);
                                }
                                else {
                                    $product->setStatusCustomStock($codeStockValueNoSklad);
                                    $product->setQtyCustomStock(0);


                                    $product->setStockData(['qty' => 0, 'is_in_stock' => false]);

                                    $stockItem = $this->_stockRegistryInterface->getStockItemBySku($sku);
                                    $stockItem->setQty(0);
                                    $stockItem->setData('is_in_stock', false);
                                    $this->_stockRegistryInterface->updateStockItemBySku($sku, $stockItem);



                                }

//                                $product->setStockData(['qty' => 0, 'is_in_stock' => false]);
//                                $product->setQuantityAndStockStatus(['qty' => $stock, 'is_in_stock' => false]);

//                                $product->setStockData(['qty' => $stock, 'is_in_stock' => true]);
//                                $product->setQuantityAndStockStatus(['qty' => $stock, 'is_in_stock' => true]);
                            }


//                            else{
//
//                                $product->setStatusCustomStock($codeStockValueNoSklad);
//
//                                $product->setStockData(['qty' => 0, 'is_in_stock' => false]);
//                                $product->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => false]);
//                            }


                            $product->save();
                            $this->_productRepository->save($product);
                            $cntProducts++;

                        }
                    }

                }
            }
            catch (\Magento\Framework\Exception\NoSuchEntityException $e)
            {
                continue;
            }

        }

        return ['clear_products'=>0,'count_products'=>$cntProducts];
    }

    public function processSupplier($data)
    {
        $file = $data['file'];
        $filepath = $file[0]['path'].'/'.$file[0]['file'];

        $csvData = $this->csv->getData($filepath);
        $attribute_code = 'code_provider';
        $attribute_code_custom_stock = 'status_custom_stock';
        $codeProvider=$data['code_provider'];
        $countClear=$this->clearStocks($codeProvider);

        $cntProducts=0;
        $codeStockValueOfice = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть в офисе');
        $codeStockValueNoSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Нет на складе');

        $codeStockValueYesSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть на складе');

        foreach ($csvData as $row => $data) {
            try {
                //if ($row > 0) {

                    //Start your work
                    $sku = $data[0];
                    if (!empty($sku)) {
                        $price = (isset($data[1])?$data[1]:false);
                        //$stock=(isset($data[2])?$data[2]:false);

                        $product = $this->getProductBySku($sku);
                        if ($product) {
                            //$StatusCustomStock=$product->getStatusCustomStock();
                            $store = 0;
                            //$product->setStoreId($store);
                            if ($price)
                                $product->setPrice($price);


                            $product->setStatusCustomStock($codeStockValueYesSklad);
                            $qty=9999;
                            $product->setStockData(['qty' => $qty, 'is_in_stock' => true]);
                            //$product->setQuantityAndStockStatus(['qty' => $qty, 'is_in_stock' => true]);
                            $product->save();


                            $stockItem = $this->_stockRegistryInterface->getStockItemBySku($sku);
                            $stockItem->setQty(2000);

                            $stockItem->setData('is_in_stock', 1);

                            $this->_stockRegistryInterface->updateStockItemBySku($sku, $stockItem);





//                            if ($stock) {
//
//
//                                if ($stock>0) {
//                                    $product->setStatusCustomStock($codeStockValueOfice);
//                                    $product->setStockData(['qty' => $stock, 'is_in_stock' => true]);
//                                    $product->setQuantityAndStockStatus(['qty' => $stock, 'is_in_stock' => true]);
//                                }
//                                else {
//                                    $product->setStatusCustomStock($codeStockValueNoSklad);
//                                    $product->setStockData(['qty' => 0, 'is_in_stock' => false]);
//                                    $product->setQuantityAndStockStatus(['qty' => $stock, 'is_in_stock' => false]);
//                                }
//
////                                $product->setStockData(['qty' => $stock, 'is_in_stock' => true]);
////                                $product->setQuantityAndStockStatus(['qty' => $stock, 'is_in_stock' => true]);
//                            }


//                            else{
//
//                                $product->setStatusCustomStock($codeStockValueNoSklad);
//
//                                $product->setStockData(['qty' => 0, 'is_in_stock' => false]);
//                                $product->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => false]);
//                            }


                           // $product->save();
                           // $this->_productRepository->save($product);
                            $cntProducts++;

                        }
                    }

               // }
            }
            catch (\Magento\Framework\Exception\NoSuchEntityException $e)
            {
                continue;
            }

        }

        return ['clear_products'=>$countClear,'count_products'=>$cntProducts];
    }

    public function process($data)
    {
        $codeProvider=$data['code_provider'];

        $typeUpload=$data['type_file'];

        switch ($typeUpload)
        {
            case 'local':
                return $this->processLocalStock($data);
                break;
            case 'supplier':
                return $this->processSupplier($data);
                break;
        }

    }

    public function clearStocks($codeProvider)
    {
        $i=0;
        $productCollection=$this->getProductsByProvider($codeProvider);
        $attribute_code_custom_stock = 'status_custom_stock';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
        //$codeStockValue = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Нет на складе');

        $codeStockValueOfice = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть в офисе');
        $codeStockValueNoSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Нет на складе');

        $codeStockValueYesSklad = $this->helperBikeshop->getAttributeValueByLabel($attribute_code_custom_stock, 'Есть на складе');



        foreach ($productCollection as $product)
        {
            $i++;
            $stockQty= $StockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
            $StatusCustomStock=$product->getStatusCustomStock();

            switch ($StatusCustomStock)
            {
                case $codeStockValueOfice:
                    continue;
                break;
                default:

                    if  ($stockQty > 0) {
                        $product->setStatusCustomStock($codeStockValueNoSklad);
                        $product->setStockData(['qty' => 0, 'is_in_stock' => false]);
                        //$product->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => false]);
                        $product->save();
                    }
                    break;

            }


            //$this->_productRepository->save($product);

        }
        return count($productCollection);

    }


}