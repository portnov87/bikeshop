<?php

namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;


class ChangeImage extends Command
{

    /**
     * @var DirectoryList
     */
    protected $directory;
    protected $name;

    protected $_productRepository;

    protected $csv;
    protected $dirList;

    protected $state;
    protected $helperBikeshop;

    public function __construct(
        //$name = null,
        Filesystem $filesystem,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\State $state,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop
        //\Magento\Framework\App\ResourceConnection $resource
        //\Magento\Catalog\Model\ProductFactory $productFactory,
    )
    {
        $this->directory = $filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $this->state = $state;
        $this->dirList = $directoryList->getPath(DirectoryList::VAR_DIR);
        $this->_productRepository = $productRepository;
        $this->csv = $csv;
        $this->helperBikeshop = $helperBikeshop;
        //$this->resource = $resource;
        //$this->_productFactory = $productFactory;
        parent::__construct();
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }


    protected function configure()
    {

        $this->setName('products:changeimage')
            ->setDescription('products:changeimage');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

        $file = 'mainimg.csv';//pub/media/import/
        $connection = $resource->getConnection();
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMIN);
        $rows = 0;
        $delimeter = ',';

/*
        $select = $connection->select()->from(
            'catalog_product_entity_varchar',
            ['value']
        )->where(
            'attribute_id IN (85,84,86) AND entity_id=' . $productIdMagento
        );
        $mainImgOld = $connection->fetchCol($select);//, [':customer_id' => $object->getCustomerId()]);
*/


        $csvData = $this->csv->getData($file);
        //$attribute_code = 'code_provider';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directoryList = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
        $media = $directoryList->getPath('media');


        foreach ($csvData as $row => $data) {
            try {
                if ($row > 0) {
                    //Start your work
                    $productIdsOld = $data[0];
                    $skuOld = $data[1];
                    $mainimg=$data[2];
                    if ($skuOld!='')
                        $sku=$skuOld;
                    else
                        $sku=$productIdsOld;

                    //if ($sku=='5303-080') {

                        $product = $this->getProductBySku($sku);
                        if ($product) {
                            $productIdMagento = $product->getId();

                            $select = $connection->select()->from(
                                'catalog_product_entity_varchar',
                                ['value']
                            )->where(
                                'attribute_id IN (85,84,86) AND entity_id=' . $productIdMagento
                            );
                            $mainImgOld = $connection->fetchCol($select);//, [':customer_id' => $object->getCustomerId()]);
                            if (!empty($mainImgOld)) {


                                $first = substr($mainimg, 0, 1);
                                $second = substr($mainimg, 1, 1);
                                $mainimg=str_replace('  ','_',$mainimg);
                                $mainimg=str_replace(' ','_',$mainimg);
                                $mainimg=str_replace('[','',$mainimg);
                                $mainimg=str_replace(']','',$mainimg);

                                $path = '/' . $first . '/' . $second . '/' . $mainimg;

                                if (file_exists($media.'/catalog/product'.$path)) {
//                                    echo $media . '/catalog/product' . $path . "\r\n\r\n";
//                                    die();
                                    //echo '$path '.$path."\r\n";

                                    $connection->update(
                                        'catalog_product_entity_varchar',
                                        ['value' => $path],
                                        [
                                            'entity_id = ?' => $productIdMagento,
                                            'attribute_id IN (?)' => [85, 84, 86]
                                        ]
                                    );
                                    echo $row . ' $sku=' . $sku . ' $productIdMagento=' . $productIdMagento . " path=" . $path . "\r\n";
                                    //echo "update productId=".$productIdMagento.' $path='.$path."\r\n";

                                }

//                                if ($sku=='5019400361') {
//                                    echo '<pre>'.$sku.' $mainimg='.$mainimg;
//                                    print_r($path);
//                                    echo '</pre>';
//                                    die();
//                                }
//                                $connection->update(
//                                    'catalog_product_entity_media_gallery_value',
//                                    ['position' => $i],
//                                    ['value_id=?' => (int)$_im['value_id']]
//                                );

                            }

/*
                            $select = $connection->select()->from(
                                'catalog_product_entity_media_gallery_value',
                                ['value_id', 'position']
                            )->where(
                                'entity_id=' . $productIdMagento
                            )->order('record_id ASC');
                            $images = $connection->fetchAll($select);
                            $i=1;
                            foreach ($images as $_im)
                            {

                                $connection->update(
                                    'catalog_product_entity_media_gallery_value',
                                    ['position' => $i],
                                    ['value_id=?' => (int)$_im['value_id']]
                                );
                                $i++;
                            }


*/
                            //$this->_productRepository->save($product);
                            //echo $row . ' $sku=' . $sku . ' $productIdMagento=' . $productIdMagento . " path=" . $path . "\r\n";
                            continue;
                        }
                   //}

                }
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                continue;
            } catch (\Exception $e) {
                echo '<pre>';
                print_r($data);
                echo '</pre>';
                echo $e->getMessage() . "\r\n";

            }

        }


    }

}