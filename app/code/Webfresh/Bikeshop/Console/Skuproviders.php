<?php
namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;


class Skuproviders extends Command
{

    /**
     * @var DirectoryList
     */
    protected $directory;
    protected $name;

    protected $_productRepository;

    protected $csv;
    protected $dirList;

    protected $state;
    protected $helperBikeshop;

    public function __construct(
        //$name = null,
        Filesystem $filesystem,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\State $state,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop
        //\Magento\Catalog\Model\ProductFactory $productFactory,
    )
    {
        $this->directory = $filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $this->state = $state;
        $this->dirList=$directoryList->getPath(DirectoryList::VAR_DIR);
        $this->_productRepository = $productRepository;
        $this->csv = $csv;
        $this->helperBikeshop=$helperBikeshop;
        //$this->_productFactory = $productFactory;
        parent::__construct();
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }


    protected function configure()
    {

        $this->setName('products:providers')
            ->setDescription('products:providers');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $file='sku_provider.csv';//pub/media/import/

        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMIN);
        $rows = 0;
        $delimeter=';';


        $csvData = $this->csv->getData($file);
        $attribute_code = 'code_provider';

        foreach ($csvData as $row => $data) {
            try {
                if ($row > 0) {
                    //Start your work
                    $sku = $data[0];
                    $codeProvider = (string)$data[1];
                    $product = $this->getProductBySku($sku);
                    if ($product) {
                        if (($product->getCodeProvider()=='')||($product->getCodeProvider()==0)) {

                            $codeProviderValue = $this->helperBikeshop->getAttributeValueByLabel('code_provider', $codeProvider);

                            $product->setCodeProvider($codeProviderValue);


                            $this->_productRepository->save($product);
                            echo $row.' '.$sku."\r\n";
                            continue;
                        }

                    }
                    echo 'exist = '.$row.' '.$sku."\r\n";



                }
            }
            catch (\Magento\Framework\Exception\NoSuchEntityException $e)
            {
                continue;
            }
            catch (\Exception $e)
            {
                echo '<pre>';
                print_r($data);
                echo '</pre>';
                echo $e->getMessage()."\r\n";

            }

        }
        die();

    }

}