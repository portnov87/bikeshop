<?php

namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

use Magento\Framework\App\Filesystem\DirectoryList;

use Magento\Catalog\Api\Data\CategoryTreeInterface;
use Magento\Catalog\Api\CategoryManagementInterface;
use Magento\Framework\Exception\NoSuchEntityException;


class ExportCatProd extends Command
{

    /**
     * @var CategoryManagementInterface
     */
    private $categoryManagement;

    private $_productCollectionFactory;
    private $categoryLinkManagement;
    private $productRepository;

    /** @var \Magento\Framework\App\State **/
    private $state;
    public function __construct(
        //$name = null,
        CategoryManagementInterface $categoryManagement,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagementInterface,
        \Magento\Catalog\Api\ProductRepositoryInterface\Proxy $productRepository,
        \Magento\Framework\App\State $state,
        \Magento\Framework\Filesystem $filesystem
        //\Magento\Catalog\Model\ProductFactory $productFactory,
    )
    {
        $this->categoryManagement = $categoryManagement;

        $this->_productCollectionFactory = $productCollectionFactory;

        $this->categoryLinkManagement = $categoryLinkManagementInterface;
        //$this->_productFactory = $productFactory;
        $this->state = $state;
        $this->productRepository = $productRepository;


        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct();
    }


    public function loadMyProduct($sku)
    {

        return $this->productRepository->get($sku);
    }
    /**
     * @param int $categoryId
     * @return CategoryTreeInterface|null
     */
    public function getCategoryData( $categoryId)//: ?CategoryTreeInterface
    {
        try {
            $getSubCategory = $this->categoryManagement->getTree($categoryId,5);
        } catch (NoSuchEntityException $e) {
            //$this->logger->error("Category not found", [$e]);
            $getSubCategory = null;
        }

        return $getSubCategory;
    }


    /**
     * @param int $categoryId
     * @return array
     */
    public function getSubCategoryByParentID(int $categoryId): array
    {
        $categoryData = [];

        $getSubCategory = $this->getCategoryData($categoryId);
        foreach ($getSubCategory->getChildrenData() as $category) {
            //$categoryData[$category->getId()]=$category->getId();
            if ($category->getIsActive()>0) {
                $categoryData[$category->getId()] = [
                    'name' => $category->getName(),
                    'url' => $category->getUrl()
                ];
                if (count($category->getChildrenData())) {

                    //$getSubCategoryLevelDown = $this->getCategoryData($category->getId());
                    foreach ($category->getChildrenData() as $subcategory) {
                        if ($subcategory->getIsActive() > 0) {
                            $categoryData[$subcategory->getId()] = [
                                'name' => $subcategory->getName(),
                                'url' => $subcategory->getUrl()
                            ];

                            if (count($subcategory->getChildrenData())) {

                                //$_getSubCategoryLevelDown = $this->getCategoryData($subcategory->getId());
                                foreach ($subcategory->getChildrenData() as $_subcategory) {
                                    if ($_subcategory->getIsActive() > 0) {
                                        $categoryData[$_subcategory->getId()] = [
                                            'name' => $_subcategory->getName(),
                                            'url' => $_subcategory->getUrl()
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $categoryData;
    }


    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        //$collection->addCategoriesFilter(['in' => $ids]);
        return $collection;
    }

    protected function configure()
    {

        $this->setName('products:exportcatprod')
            ->setDescription('products:exportcatprod');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

        /*$productSkus=['2502069662', 'GA150000075', 'GA150000076', 'GA150000077', '11.6818.035.013', '11.6818.035.014', '11.6818.035.011', '11.6818.035.006', '11.6818.035.000', '11.6818.035.001', '11.6818.035.002', '11.6818.035.003', '11.6818.035.004', '11.6818.035.007', '11.6818.035.005', '11.6818.035.009', '11.6818.035.016', '11.6818.035.017', '11.6818.035.019', '11.6818.035.012', '11.6818.035.008', '11.6818.035.010', '11.6818.035.015', '11.6818.000.000', '11.6815.007.010', '11.6815.006.010', '11.6818.021.010', '11.6818.003.010', '11.6818.022.020', '11.6818.036.000', '00.6815.066.030', '00.6815.066.020', '11.6818.037.000', '11.6818.037.001', '11.6818.037.002', '11.6818.037.003', '11.6818.037.004', '11.6815.016.010', '11.6815.016.020', '11.6815.022.010', '11.6815.022.020', '11.6815.022.030', '11.6815.022.040', '11.6815.025.010', '11.6815.025.030', '11.6815.025.020', '11.6815.030.010', '11.6815.026.030', '11.6815.026.050', '11.6815.026.010', '11.6815.026.040', '11.6815.026.060', '11.6815.026.020', '11.6815.026.070', '11.6815.026.080', '11.6818.048.000', '11.6815.027.010', '11.6818.013.000', '11.6818.013.010', '11.6818.014.000', '11.6818.031.005', '11.6818.031.000', '11.6818.031.004', '11.6818.031.002', '11.6818.031.001', '11.6815.028.010', '2073069129', '2073074324', '2073074335', '2073074346', '11.6818.049.000', '00.6818.029.000', '00.6818.006.000', 'DSE-33-84'];
        $targetCategory=2588;
        $i=1;
        foreach ($productSkus as $sku)
        {
            $categoryIds = [
                $targetCategory
            ];
            echo $i.' '.$sku."\r\n";
            $this->categoryLinkManagement->assignProductToCategories(
                $sku,
                $categoryIds
            );
            $i++;
        }
*/

//        $products=$this->getProductCollection();
//
//        foreach ($products as $prod)
//        {
//
//        }

        $date=date('Y-m-d_H:i:s');
        $filepath = 'export/categories'.$date.'.csv';
        $this->directory->create('export');
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();

        $header = ['Id', 'Name', 'Url'];
        $stream->writeCsv($header);

        $parentid=2;
        $categories=$this->getSubCategoryByParentID($parentid);
        echo '$categories '.count($categories);
//        echo '<pre>$categories';
//        print_r($categories);
//        echo '</pre>';
        foreach ($categories as $key=>$cat)
        {
            $data = [];
            $data[] = $key;//$cat->getId();
            $data[] = $cat['name'];//->getName();
            $data[] = $cat['url'];//$_product->getSku();
            //$data[] = $_product->getProductUrl();
            $stream->writeCsv($data);
        }

        /*
        $date=date('Y-m-d_H:i:s');
        $filepath = 'export/productsku'.$date.'.csv';
        $this->directory->create('export');
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();

        $header = ['Id', 'Name', 'Sku','Url'];
        $stream->writeCsv($header);

        $collection = $this->getProductCollection();
        foreach ($collection as $_product) {
            $data = [];
            $data[] = $_product->getId();
            $data[] = $_product->getName();
            $data[] = $_product->getSku();
            $data[] = $_product->getProductUrl();
            $stream->writeCsv($data);
        }

*/



        /*

        $productSkus=['BB916173', '8716683046380', 'T1100', 'T1000-1', 'T1856', 'T2650', 'T2675', 'T2500', '121012', '121002', '100501', '100550', '121023', '121011', '111303', '11101', '991710', '991716', '31000', 'GA930503', '3204101200', '3204101400', '121026', '0121011A', '121006', 'TA.T2660', 'TA.T2800', 'T2780', 'T2900', 'GA560000012', 'GA560000011', 'GA560000014', 'GA560000013', 'GA930501', 'GA930506', '162001', '130201', '“1000', '“2675', '“2500', 'T2400', 'T2180', 'T2590', '“2910', '“2915', 'T2930', 'T2931', 'T1932', 'T1990.04', 'T1707', 'T1708', 'T1710', 'T1711', 'T1867.05', '31009', '130201', '3517021', '121901', '1020008', 'GA560000019', '7079860', '164001', '165005', '172001', '1014042', '1014043', '145106', '1014233', '121028', '163001', '171003', '1027519', '121028', '163001', '171003', '182001', '171007', '191001', '121102', '180602', '190301', '200401', '1014306', '1027300', '180601', '171010', '191004', '192001', 'COM-89-74', 'GUR-67-23', 'ACC-96-46', 'EXB-85-41', 'ADP-33-39', 'EXB-41-80', 'EXB-10-31', 'EXB-07-52', 'FIX-85-64', 'FIX-22-28', 'HALB-12-61', 'HALB-97-91', 'COM-20-43', 'QRE-34-85', 'EXB-19-07'];
        $targetCategory=2565;
        $i=1;
        foreach ($productSkus as $sku)
        {
            $categoryIds = [
                $targetCategory
            ];

            try {
                $product = $this->loadMyProduct($sku);
                echo $i . ' ' . $sku . "\r\n";
                $this->categoryLinkManagement->assignProductToCategories(
                    $sku,
                    $categoryIds
                );
                $i++;
            } catch (NoSuchEntityException $e) {
                continue;
            }
        }


        $i=1;*/
        /*$parentid=1624;
//        $categories=$this->getSubCategoryByParentID($parentid);
//        foreach ($categories as $catId)
//        {
            $products=$this->getProductCollectionByCategories([$parentid]);
            foreach ($products as $prod)
            {
                $categoryIds = [
                    2369//$parentid
                ];
                echo $i.' '.$prod->getSku()."\r\n";
                $this->categoryLinkManagement->assignProductToCategories(
                    $prod->getSku(),
                    $categoryIds
                );
                $i++;
            }

        //}
        */
       /* $i=1;

        $parentid=1615;
        $categories=$this->getSubCategoryByParentID($parentid);
        foreach ($categories as $catId)
        {
            $products=$this->getProductCollectionByCategories([$catId]);
            foreach ($products as $prod)
            {

                $categoryIds = [
                    $parentid
                ];
                echo $i.' '.$prod->getSku()."\r\n";
                $this->categoryLinkManagement->assignProductToCategories(
                    $prod->getSku(),
                    $categoryIds
                );
                $i++;
            }

        }*/


        //die();

    }

}