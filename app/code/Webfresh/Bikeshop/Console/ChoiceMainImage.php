<?php

namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;


class ChoiceMainImage extends Command
{

    /**
     * @var DirectoryList
     */
    protected $directory;
    protected $name;

    protected $_productRepository;

    protected $csv;
    protected $dirList;

    protected $state;
    protected $helperBikeshop;

    public function __construct(
        //$name = null,
        Filesystem $filesystem,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\State $state,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop
        //\Magento\Framework\App\ResourceConnection $resource
        //\Magento\Catalog\Model\ProductFactory $productFactory,
    )
    {
        $this->directory = $filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $this->state = $state;
        $this->dirList = $directoryList->getPath(DirectoryList::VAR_DIR);
        $this->_productRepository = $productRepository;
        $this->csv = $csv;
        $this->helperBikeshop = $helperBikeshop;
        //$this->resource = $resource;
        //$this->_productFactory = $productFactory;
        parent::__construct();
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }


    protected function configure()
    {

        $this->setName('products:changemainimage')
            ->setDescription('products:changemainimage');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {





        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');



        $file = 'mainimg.csv';//pub/media/import/
        $connection = $resource->getConnection();
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMIN);
        $rows = 0;
        $delimeter = ',';

        $result1 = $connection->fetchAll("SELECT mg.value,pre.entity_id FROM catalog_product_entity_media_gallery_value mgv
INNER JOIN catalog_product_entity pre ON (mgv.entity_id=pre.entity_id)
INNER JOIN catalog_product_entity_media_gallery mg  ON (mgv.value_id=mg.value_id)
LEFT JOIN catalog_product_entity_varchar prvar ON (mg.value=prvar.value and prvar.attribute_id=84 and prvar.store_id=0)
WHERE mgv.position=1 AND prvar.value IS NULL AND mg.media_type='image' GROUP BY pre.entity_id LIMIT 5000");



/*
 * SELECT mg.value,pre.entity_id FROM catalog_product_entity_media_gallery_value mgv
INNER JOIN catalog_product_entity pre ON (mgv.entity_id=pre.entity_id)
INNER JOIN catalog_product_entity_media_gallery mg  ON (mgv.value_id=mg.value_id)
LEFT JOIN catalog_product_entity_varchar prvar ON (mg.value=prvar.value and prvar.attribute_id=84 and prvar.store_id=0)
 */

//
//        $select = $connection->select()->from(
//            'catalog_product_entity_varchar',
//            ['value']
//        )->where(
//            'attribute_id IN (85,84,86) AND entity_id=' . $productIdMagento
//        );
//        $mainImgOld = $connection->fetchCol($select);//, [':customer_id' => $object->getCustomerId()]);


        //$csvData = $this->csv->getData($file);
        //$attribute_code = 'code_provider';

        foreach ($result1 as $row => $data) {
            try {

//                echo '<pre>$data';
//                print_r($data);
//                echo '</pre>';
//                die();
                //if ($row > 0) {
                    //Start your work
                    $productId = $data['entity_id'];
                    $image= $data['value'];


                    //if ($sku=='5303-080') {

                $select = $connection->select()->from(
                    'catalog_product_entity_varchar',
                    ['value']
                )->where(
                    'attribute_id IN (85,84,86) AND entity_id=' . $productId
                );
                $mainImgOld = $connection->fetchCol($select);//, [':customer_id' => $object->getCustomerId()]);
                if (empty($mainImgOld)) {


                    $connection
                        ->insert(
                            'catalog_product_entity_varchar',
                            array(
                                'value' => $image,
                                'attribute_id' => 84,
                                'entity_id' => $productId
                            )
                        );


                    $connection
                        ->insert(
                            'catalog_product_entity_varchar',
                            array(
                                'value' => $image,
                                'attribute_id' => 85,
                                'entity_id' => $productId
                            )
                        );
                    $connection
                        ->insert(
                            'catalog_product_entity_varchar',
                            array(
                                'value' => $image,
                                'attribute_id' => 86,
                                'entity_id' => $productId
                            )
                        );
                }else{


                    $connection->update(
                        'catalog_product_entity_varchar',
                        ['value' => $image],
                        [
                            'entity_id = ?' => $productId,
                            'attribute_id IN (?)' => [85, 84, 86]
                        ]
                    );

                }

                echo $productId.' '.$image;


            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                continue;
            } catch (\Exception $e) {
                echo '<pre>';
                print_r($data);
                echo '</pre>';
                echo $e->getMessage() . "\r\n";

            }

        }


    }

}