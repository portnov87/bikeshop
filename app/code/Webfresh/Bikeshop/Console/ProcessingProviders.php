<?php

namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;


class ProcessingProviders extends Command
{

    /**
     * @var DirectoryList
     */
    protected $directory;
    protected $name;

    protected $_productRepository;

    protected $csv;
    protected $dirList;

    protected $state;
    protected $helperBikeshop;
    protected $providersPriceRepository;
    protected $searchCriteriaBuilder;
    protected $helperBikeshopProcessing;
    protected $_date;

    public function __construct(
        Filesystem $filesystem,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\State $state,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop,
        \Webfresh\Bikeshop\Api\ProvidersPriceRepositoryInterface $providersPriceRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Webfresh\Bikeshop\Helper\Processing $helperBikeshopProcessing,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    )
    {
        $this->_date = $date;

        $this->helperBikeshopProcessing = $helperBikeshopProcessing;
        $this->providersPriceRepository = $providersPriceRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;


        $this->state = $state;
        $this->_productRepository = $productRepository;
        $this->csv = $csv;
        $this->helperBikeshop = $helperBikeshop;


        parent::__construct();
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }


    protected function configure()
    {

        $this->setName('providers:processing')
            ->setDescription('providers:processing');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMIN);
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('provider_code', 'veloplaneta', 'eq')
            ->addFilter('status', 1, 'eq')
            ->create();
        $items = $this->providersPriceRepository
            ->getList($searchCriteria)
            ->getItems();

        $helperBikeshopProcessing = $this->helperBikeshopProcessing;
        foreach ($items as $item) {
            try {
                $_provider = $this->providersPriceRepository->getById($item['entity_id']);
                $_provider->setLastProcessStatus(0);
                $_provider->save();
                //$this->providersPriceRepository->save($_provider);

                echo 'providersPrice  =  ' . $item['entity_id'] . "\r\n";
                $result = $helperBikeshopProcessing->processProvidersById($item['entity_id']);

                if ($result)
                    $_provider->setLastProcessStatus(1);
                else
                    $_provider->setLastProcessStatus(0);


                $_date = $this->_date->date();//gmtDate();
                $date = date('Y-m-d H:i:s');

                //echo '$date = '.$_date." ".$date." \r\n";
                //$_provider->setData('last_process_date',$_date);//
                $_provider->setLastProcessDate($date);
                $_provider->save();


                $connection->update(
                    'bikeshop_providers_price',
                    ['last_process_date' => $date],
                    [
                        'entity_id = ?' => $item['entity_id']
                    ]
                );


            } catch (\Exception $e) {

                echo $item['entity_id'].' '.$e->getMessage() . "\r\n";

            }
            echo 'finish '.$item['entity_id']."\r\n\r\n";
            //$this->providersPriceRepository->save($_provider);
        }

        //die();

    }

}