<?php

namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;



use Magento\Catalog\Api\Data\CategoryTreeInterface;
use Magento\Catalog\Api\CategoryManagementInterface;
use Magento\Framework\Exception\NoSuchEntityException;


class AttributeManufacturer extends Command
{

    /**
     * @var CategoryManagementInterface
     */
    private $categoryManagement;

    private $_productCollectionFactory;
    private $categoryLinkManagement;
    private $productRepository;

    /** @var \Magento\Framework\App\State **/
    private $state;
    public function __construct(
        //$name = null,
        CategoryManagementInterface $categoryManagement,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagementInterface,
        \Magento\Catalog\Api\ProductRepositoryInterface\Proxy $productRepository,
        \Magento\Framework\App\State $state
        //\Magento\Catalog\Model\ProductFactory $productFactory,
    )
    {
        $this->categoryManagement = $categoryManagement;

        $this->_productCollectionFactory = $productCollectionFactory;

        $this->categoryLinkManagement = $categoryLinkManagementInterface;
        //$this->_productFactory = $productFactory;
        $this->state = $state;
        $this->productRepository = $productRepository;
        parent::__construct();
    }
    protected function configure()
    {

        $this->setName('products:changeattrmanuf')
            ->setDescription('products:changeattrmanuf');
        parent::configure();
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

        $connection = $resource->getConnection();

        $rows = 0;

        $result1 = $connection->fetchAll("SELECT * FROM catalog_product_entity_int WHERE entity_id NOT IN (SELECT entity_id FROM catalog_product_entity_int WHERE attribute_id=141 
and entity_id IN (SELECT entity_id FROM catalog_product_entity_int WHERE attribute_id=141 and store_id=1) and store_id=0
GROUP BY entity_id) and attribute_id=141 and store_id=1 GROUP BY entity_id");


        $i=1;
        foreach ($result1 as $row => $data) {

            $entity_id=$data['entity_id'];
            $valueId=$data['value'];
            $storeId=0;
            $attribiuteId=141;


            $select = $connection->select()->from(
                'catalog_product_entity_int',
                ['value']
            )->where(
                'attribute_id = 141 AND entity_id=' . $entity_id.' AND store_id=0'
            );
            $mainImgOld = $connection->fetchCol($select);
            if (empty($mainImgOld)) {
                $connection
                    ->insert(
                        'catalog_product_entity_int',
                        array(
                            'entity_id' => $entity_id,
                            'attribute_id' => $attribiuteId,
                            'store_id' => $storeId,
                            'value' => $valueId
                        )
                    );
                echo $i.' insert '.$entity_id. ' $valueId='.$valueId."\r\n";
                $i++;
            }



        }

    }

}