<?php

namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\Registry;

class DeleteCategory extends Command
{


    protected $categoryFactory;
    protected $registry;

    public function __construct(
        CategoryFactory             $categoryFactory,
        Registry                    $registry
    ) {
        $this->categoryFactory      = $categoryFactory;
        $this->registry             = $registry;
        parent::__construct();
    }



    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }


    protected function configure()
    {

        $this->setName('category:delete')
            ->setDescription('category:delete');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {



        $categories = $this->categoryFactory->create()->getCollection();

        $this->registry->register("isSecureArea", true);
        foreach($categories as $category) {
            if(in_array($category->getId(),[
                1488,2451,2452,2453,2472,2473,2475,2476,2478,2479,2480,2481,2482,2483,2484,2485,2486,2487,2488,2489,2491,2492,2493,2494,2495,2496,2497,2498,2499,2500,2501,2502,2503,2504,2505,2506,2507,2508,2509,2510,2511,2512,2513,2519,2520,2521,2522,2523,2524,2525,2526,2527,2528,2529,2530,2531,2532,2533,2534,2535,2536,2537,2538,2539,2540,2541,2543,2544,2545,2546,2547,2548,2549,2550,2551,2552,2553,2554
            ])) {
                $category->delete();
            }
        }



    }

}