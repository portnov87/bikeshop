<?php
namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;


class Skuvamshop extends Command
{

    /**
     * @var DirectoryList
     */
    protected $directory;
    protected $name;

    protected $_productRepository;

    protected $csv;
    protected $dirList;

    protected $state;
    protected $helperBikeshop;

    public function __construct(
        //$name = null,
        Filesystem $filesystem,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\State $state,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop
        //\Magento\Catalog\Model\ProductFactory $productFactory,
    )
    {
        $this->directory = $filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $this->state = $state;
        $this->dirList=$directoryList->getPath(DirectoryList::VAR_DIR);
        $this->_productRepository = $productRepository;
        $this->csv = $csv;
        $this->helperBikeshop=$helperBikeshop;
        //$this->_productFactory = $productFactory;
        parent::__construct();
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }


    protected function configure()
    {

        $this->setName('products:skuvamshop')
            ->setDescription('products:skuvamshop');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $file='all_productsVam.csv';//pub/media/import/

        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMIN);
        $rows = 0;
        $delimeter=';';

        $noFounds=[];

        $csvData = $this->csv->getData($file);
        $attribute_code = 'code_provider';

        foreach ($csvData as $row => $data) {
            $sku = $data[0];
            try {
                if ($row > 0) {
                    //if ($sku=='AR00197') {
                        //Start your work
                        $product = $this->getProductBySku($sku);
                        if ($product) {

                        } else
                            $noFounds[] = ['sku'=>$sku];

//                        echo '$sku'.$sku."\r\n";
//                        die();
//                    }
                    //echo 'exist = '.$row.' '.$sku."\r\n";
                }
            }
            catch (\Magento\Framework\Exception\NoSuchEntityException $e)
            {
                $noFounds[] = ['sku'=>$sku];
            }
            catch (\Exception $e)
            {
                echo '<pre>';
                print_r($data);
                echo '</pre>';
                echo $e->getMessage()."\r\n";

            }

        }
        if (is_array($noFounds)) {
            if (count($noFounds) > 0) {
                $csv=$this->csv;
                $csv->setDelimiter($delimeter);
                $csv->setEnclosure("'");
                $fileNew = 'noFoundMagento.csv';
                $csv->saveData($fileNew, $noFounds);
            }
        }
        die();

    }

}