<?php

namespace Webfresh\Bikeshop\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;


class SelectMainImage extends Command
{

    /**
     * @var DirectoryList
     */
    protected $directory;
    protected $name;

    protected $_productRepository;

    protected $csv;
    protected $dirList;

    protected $state;
    protected $helperBikeshop;

    public function __construct(
        //$name = null,
        Filesystem $filesystem,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\State $state,
        \Webfresh\Bikeshop\Helper\Data $helperBikeshop
        //\Magento\Framework\App\ResourceConnection $resource
        //\Magento\Catalog\Model\ProductFactory $productFactory,
    )
    {
        $this->directory = $filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $this->state = $state;
        $this->dirList = $directoryList->getPath(DirectoryList::VAR_DIR);
        $this->_productRepository = $productRepository;
        $this->csv = $csv;
        $this->helperBikeshop = $helperBikeshop;
        //$this->resource = $resource;
        //$this->_productFactory = $productFactory;
        parent::__construct();
    }

    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }


    protected function configure()
    {

        $this->setName('products:selectmainimage')
            ->setDescription('products:selectmainimage');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {





        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');



        $file = 'mainimg.csv';//pub/media/import/
        $connection = $resource->getConnection();
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMIN);
        $rows = 0;
        $delimeter = ',';

        $result1 = $connection->fetchAll("SELECT mpv.entity_id,mg.value, prven.sku FROM catalog_product_entity_media_gallery_value mpv 
LEFT JOIN catalog_product_entity_varchar prvar ON (mpv.entity_id=prvar.entity_id and prvar.attribute_id=84)
LEFT JOIN catalog_product_entity prven ON (mpv.entity_id=prven.entity_id)
LEFT JOIN catalog_product_entity_media_gallery mg ON (mpv.value_id=mg.value_id)
WHERE prvar.value IS NULL GROUP BY mpv.entity_id ORDER BY mpv.position DESC  
");


        $csvData = $this->csv->getData($file);
        //$attribute_code = 'code_provider';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directoryList = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
        $media = $directoryList->getPath('media');


        foreach ($result1 as $row => $data) {
            try {

                    $productId = $data['entity_id'];
                $productSku = $data['sku'];
                    $imageMagento= $image=$data['value'];




                foreach ($csvData as $_row => $_data) {
                    $productIdsOld = $_data[0];
                    $skuOld = $_data[1];
                    if (!empty($skuOld))
                        $skuMag=$skuOld;
                    else $skuMag=$productIdsOld;

                    if ($skuMag==$productSku)
                    {
                        $productMainImg = $_data[2];

                        $_imageMagento=explode('/',$imageMagento);
                        echo '<pre>$imageMagento';
                        print_r($_imageMagento);
                        echo '</pre>';
                        //break;
                        if (isset($_imageMagento[3]))
                        {

                            $_mainimg=$_imageMagento[3];
                            if ($productMainImg==$_mainimg) {

                                $first = substr($_mainimg, 0, 1);
                                $second = substr($_mainimg, 1, 1);
                                $mainimg = str_replace('  ', '_', $_mainimg);
                                $mainimg = str_replace(' ', '_', $mainimg);
                                $mainimg = str_replace('[', '', $mainimg);
                                $mainimg = str_replace(']', '', $mainimg);

                                $path = '/' . $first . '/' . $second . '/' . $mainimg;

                                $image = $path;


//                                echo '<pre>$data';
//                                print_r($data);
//                                echo '</pre>';
                                echo '$image = '.$productSku. ' '.$productId.' '.$image.' '.$imageMagento."\r\n";
                                //die();
//                            $select = $connection->select()->from(
//                                'catalog_product_entity_media_gallery',
//                                ['value']
//                            )->where(
//                                "value= '".$path."' AND attribute_id=87"// . $productId
//                            );
//                            $mainImgOld = $connection->fetchCol($select);

                            }


                        }


                        break;
                    }
                }

                //continue;





                    //if ($sku=='5303-080') {

                $select = $connection->select()->from(
                    'catalog_product_entity_varchar',
                    ['value']
                )->where(
                    'attribute_id IN (85,84,86) AND entity_id=' . $productId
                );
                $mainImgOld = $connection->fetchCol($select);//, [':customer_id' => $object->getCustomerId()]);
                if (empty($mainImgOld)) {


                    $connection
                        ->insert(
                            'catalog_product_entity_varchar',
                            array(
                                'value' => $image,
                                'attribute_id' => 84,
                                'entity_id' => $productId
                            )
                        );


                    $connection
                        ->insert(
                            'catalog_product_entity_varchar',
                            array(
                                'value' => $image,
                                'attribute_id' => 85,
                                'entity_id' => $productId
                            )
                        );
                    $connection
                        ->insert(
                            'catalog_product_entity_varchar',
                            array(
                                'value' => $image,
                                'attribute_id' => 86,
                                'entity_id' => $productId
                            )
                        );
                }else{


                    $connection->update(
                        'catalog_product_entity_varchar',
                        ['value' => $image],
                        [
                            'entity_id = ?' => $productId,
                            'attribute_id IN (?)' => [85, 84, 86]
                        ]
                    );

                }

                echo 'no find old = '.$productId.' '.$image."\r\n";


            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                continue;
            } catch (\Exception $e) {
                echo '<pre>';
                print_r($data);
                echo '</pre>';
                echo $e->getMessage() . "\r\n";

            }

        }


    }

}