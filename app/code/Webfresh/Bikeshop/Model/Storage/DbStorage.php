<?php
/**
 * Copyright © 2017 Toan Nguyen. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Webfresh\Bikeshop\Model\Storage;

use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Service\V1\Data\UrlRewriteFactory;

class DbStorage extends \Magento\UrlRewrite\Model\Storage\DbStorage
{
    /**
     * Save new url rewrites and remove old if exist. Template method
     *
     * @param \Magento\UrlRewrite\Service\V1\Data\UrlRewrite[] $urls
     *
     * @return void
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    protected function doReplace($urls)
    {
        foreach ($this->createFilterDataBasedOnUrls($urls) as $type => $urlData) {
            $urlData[UrlRewrite::ENTITY_TYPE] = $type;
            $this->deleteByData($urlData);
        }
        $data = [];

        $data = [];
        $storeId_requestPaths = [];
        foreach ($urls as $url) {
            $storeId = $url->getStoreId();
            $requestPath = $url->getRequestPath();
            // Skip if is exist in the database

            $connection = $this->connection;
            $select = $connection->select()->from(
                'url_rewrite',
                ['entity_id']
            );
            $select->where(
                'store_id = ?',
                $storeId
            );

//            $select->where(
//                'store_id = ? or request_path = ?',
//                $customerGroupId
//            );

            $select->where(
                'request_path = ?',
                $requestPath
            );

            $exists = $this->connection->fetchAll($select);

          // $sql = "SELECT * FROM url_rewrite where store_id ='$storeId' and request_path = '$requestPath'";
//            $exists = $this->connection->fetchAll($sql);

            if ($exists) {

                if (count($exists) > 0) {

                    continue;
                }
            }

//            echo '<pre>$exists';
//            print_r($exists);
//            echo '</pre>';
//            echo "<br/><br/> select = ". $select."<br/><br/>";
//            die();
            //continue;

            $storeId_requestPaths[] = $storeId . '-' . $requestPath;
            $data[] = $url->toArray();
        }


        // Remove duplication data;
        $n = count($storeId_requestPaths);
        for ($i = 0; $i < $n - 1; $i++) {
            for ($j = $i + 1; $j < $n; $j++) {
                if ($storeId_requestPaths[$i] == $storeId_requestPaths[$j]) {
                    unset($data[$j]);
                }
            }
        }
//        foreach ($urls as $url) {
//            $urlArray = $url->toArray();
//            $urlPath = $urlArray['request_path'];
//            $storeId = $urlArray['store_id'];
//            $dataKey = $storeId . '..' . $urlPath;
//            $data[$dataKey] = $urlArray;
//        }

        $this->insertMultiple($data);
    }
}