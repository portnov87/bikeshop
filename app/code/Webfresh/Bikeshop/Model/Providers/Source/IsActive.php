<?php

namespace Webfresh\Bikeshop\Model\ProvidersPrice\Source;

use Magento\Framework\Data\OptionSourceInterface;

class IsActive implements OptionSourceInterface
{


    protected $providersPrice;

    /**
     * IsActive constructor.
     * @param \TemplateMonster\ShopByBrand\Model\ProvidersPrice $brand
     */
    public function __construct(\Webfresh\Bikeshop\Model\ProvidersPrice $providersPrice)
    {
        $this->providersPrice = $providersPrice;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        //$availableOptions = $this->brand->getAvailableStatuses();
        $options = [];
        //foreach ($availableOptions as $key => $value) {
        $options[] = [
            'label' => 'Не Активно',
            'value' => 0,
        ];
        $options[] = [
            'label' => 'Активно',
            'value' => 1,
        ];
        //}
        return $options;
    }

}