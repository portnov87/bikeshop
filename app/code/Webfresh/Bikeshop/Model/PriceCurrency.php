<?php
namespace Webfresh\Bikeshop\Model;

class PriceCurrency extends \Magento\Directory\Model\PriceCurrency
{
    const DEFAULT_PRECISION = 0;
    public function round($price)
    {
        $precision=0;
        $price=round($price, self::DEFAULT_PRECISION);

        return $price;
    }
    public function convertAndRound($amount, $scope = null, $currency = null, $precision = self::DEFAULT_PRECISION)
    {
        $precision=0;
        $price=parent::convertAndRound($amount, $scope, $currency, $precision);

        return $price;
    }

    /**
     * {@inheritdoc}
     */
    public function format(
        $amount,
        $includeContainer = true,
        $precision = self::DEFAULT_PRECISION,
        $scope = null,
        $currency = null
    ) {
        $precision=0;
        $price=$this->getCurrency($scope, $currency)
            ->formatPrecision($amount, $precision, [], $includeContainer);
        return $price;
    }
}