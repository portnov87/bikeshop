<?php

namespace Webfresh\Bikeshop\Model\Source;

use Magento\Eav\Api\AttributeOptionManagementInterface;


class Types implements \Magento\Framework\Option\ArrayInterface
{

    protected $attributeOptionManagement;

    public function __construct(
        AttributeOptionManagementInterface $attributeOptionManagement
    )
    {
        $this->attributeOptionManagement = $attributeOptionManagement;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];

        $result[] = ['value' => 'xml', 'label' => __('XML')];
        $result[] = ['value' => 'cvs', 'label' => __('CSV')];
        //$result[] = ['value' => 'cvs', 'label' => __('CSV')];


        return $result;

    }

}