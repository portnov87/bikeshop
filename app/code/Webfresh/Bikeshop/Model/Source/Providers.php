<?php

namespace Webfresh\Bikeshop\Model\Source;

use Magento\Eav\Api\AttributeOptionManagementInterface;


class Providers implements \Magento\Framework\Option\ArrayInterface
{

    protected $attributeOptionManagement;

    public function __construct(
        AttributeOptionManagementInterface $attributeOptionManagement
    )
    {
        $this->attributeOptionManagement = $attributeOptionManagement;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
        $attributeCode='code_provider';
        $options = $this->attributeOptionManagement->getItems(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode
        );
        foreach ($options as $option) {

            $result[] = ['value' => $option->getValue(), 'label' => __($option->getLabel())];

        }

        return $result;

    }

}