<?php

namespace Webfresh\Bikeshop\Model\Source;

use Magento\Eav\Api\AttributeOptionManagementInterface;


class TypefilePrice implements \Magento\Framework\Option\ArrayInterface
{

    protected $attributeOptionManagement;

    public function __construct(
        AttributeOptionManagementInterface $attributeOptionManagement
    )
    {
        $this->attributeOptionManagement = $attributeOptionManagement;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];

        $result[] = ['value' => 'supplier', 'label' => 'Обновление от поставщика'];
        $result[] = ['value' => 'local', 'label' => 'Обновление локального склада'];

        return $result;

    }

}