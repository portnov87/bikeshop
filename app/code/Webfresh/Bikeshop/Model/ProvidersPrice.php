<?php

namespace Webfresh\Bikeshop\Model;

use Webfresh\Bikeshop\Api\Data\ProvidersPriceInterface;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\UrlInterface;

class ProvidersPrice extends AbstractModel implements ProvidersPriceInterface, IdentityInterface
{
    const CACHE_TAG = 'providersPrice';

    /**
     * @var string
     */
    protected $_cacheTag = 'providersPrice';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'providersPrice';

    /**
     * @var \TemplateMonster\ShopByBrand\Helper\Data
     */
    protected $_helper;

    /**
     * @var
     */
    protected $_urlBuilder;

    /**
     * Brand constructor.
     *
     * @param ShopByBrandHelper     $helper
     * @param Context               $context
     * @param Registry              $registry
     * @param UrlInterface          $urlBuilder
     * @param AbstractResource|null $resource
     * @param AbstractDb|null       $resourceCollection
     * @param array                 $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        UrlInterface $urlBuilder,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->_urlBuilder = $urlBuilder;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webfresh\Bikeshop\Model\ResourceModel\ProvidersPrice');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }


    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    public function getProviderCode()
    {
        return $this->getData(self::PROVIDER_CODE);
    }

    public function getAttributeId()
    {
        return $this->getData(self::ATTRIBUTE_ID);
    }

    public function getAttributeValueId()
    {
        return $this->getData(self::ATTRIBUTE_VALUE_ID);
    }


    public function getLink()
    {
        return $this->getData(self::LINK);
    }
    public function setLink($link)
    {
        return $this->setData(self::LINK, $link);
    }


    public function getLastProcessStatus()
    {
        return $this->getData(self::LAST_PROCESS_STATUS);
    }
    public function setLastProcessStatus($value)
    {
        return $this->setData(self::LAST_PROCESS_STATUS, $value);
    }

    public function getLastProcessDate()
    {
        return $this->getData(self::LAST_PROCESS_DATE);
    }
    public function setLastProcessDate($value)
    {
        return $this->setData(self::LAST_PROCESS_DATE, $value);
    }






    public function getType()
    {
        return $this->getData(self::TYPE);
    }
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    public function setEntityId($id)
    {
        return $this->setData(self::ENTITY_ID,$id);
    }

    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    public function setProviderCode($providerCode)
    {
        return $this->setData(self::PROVIDER_CODE, $providerCode);
    }

    public function setAttributeId($attributeId)
    {
        return $this->setData(self::ATTRIBUTE_ID, $attributeId);
    }


    public function setAttributeValueId($attributeValueId)
    {
        return $this->setData(self::ATTRIBUTE_VALUE_ID, $attributeValueId);
    }

    public function isStatus($status)
    {
        return $this->getStatus() === $status;
    }


}