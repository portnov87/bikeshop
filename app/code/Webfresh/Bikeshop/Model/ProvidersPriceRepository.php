<?php
/**
 *
 * Copyright © 2015 TemplateMonster. All rights reserved.
 * See COPYING.txt for license details.
 *
 */

namespace Webfresh\Bikeshop\Model;

use Webfresh\Bikeshop\Api\Data;
use Webfresh\Bikeshop\Api\ProvidersPriceRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Webfresh\Bikeshop\Model\ResourceModel\ProvidersPrice as ResourceProvidersPrice;
use Webfresh\Bikeshop\Model\ResourceModel\ProvidersPrice\CollectionFactory as ProvidersPriceCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class ProvidersPriceRepository implements  ProvidersPriceRepositoryInterface
{

    /**
     * @var ResourceBrand
     */
    protected $resource;

    /**
     * @var BrandFactory
     */
    protected $providersPriceFactory;

    /**
     * @var BrandCollectionFactory
     */
    protected $providersPriceCollectionFactory;

    /**
     * @var Data\BrandSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var Data\BrandInterfaceFactory
     */
    protected $dataProvidersPriceFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;


    /**
     * BrandRepository constructor.
     * @param ResourceBrand $resource
     * @param BrandFactory $brandFactory
     * @param Data\BrandInterfaceFactory $dataBrandFactory
     * @param BrandCollectionFactory $brandCollectionFactory
     * @param Data\BrandSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceProvidersPrice $resource,
        ProvidersPriceFactory $providersPriceFactory,
        Data\ProvidersPriceInterfaceFactory $dataProvidersPriceFactory,
        ProvidersPriceCollectionFactory $providersPriceCollectionFactory,
        Data\ProvidersPriceSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->providersPriceFactory = $providersPriceFactory;
        $this->providersPriceCollectionFactory = $providersPriceCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataProvidersPriceFactory = $dataProvidersPriceFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Data\BrandInterface $brand
     * @return Data\BrandInterface
     * @throws CouldNotSaveException
     */
    public function save(\Webfresh\Bikeshop\Api\Data\ProvidersPriceInterface $providersPrice)
    {
//        if (empty($brand->getStoreId())) {
//            $storeId = $this->storeManager->getStore()->getId();
//            $brand->setStoreId($storeId);
//        }
        try {
            //$brand->setUrlPage(strtolower($brand->getUrlPage()));
            $this->resource->save($providersPrice);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the provider: %1',
                $exception->getMessage()
            ));
        }
        return $providersPrice;
    }

    /**
     * @param int $brandId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($providerId)
    {
        $provider = $this->providersPriceFactory->create();
        $this->resource->load($provider,$providerId);
        if (!$provider->getId()) {
            throw new NoSuchEntityException(__('providersPrice with id "%1" does not exist.', $providerId));
        }
        return $provider;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $collection = $this->providersPriceCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $brands = [];
        /** @var ProvidersPrice $brandModel */
        foreach ($collection as $brandModel) {
            $brandData = $this->dataProvidersPriceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $brandData,
                $brandModel->getData(),
                'Webfresh\Bikeshop\Api\Data\ProvidersPriceInterface'
            );
            $brands[] = $this->dataObjectProcessor->buildOutputDataArray(
                $brandData,
                'Webfresh\Bikeshop\Api\Data\ProvidersPriceInterface'
            );
        }
        $searchResults->setItems($brands);
        return $searchResults;
    }

    /**
     * @param Data\BrandInterface $brand
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Webfresh\Bikeshop\Api\Data\ProvidersPriceInterface $providersPrice)
    {
        try {
            $this->resource->delete($providersPrice);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the providersPrice: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param int $brandId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($providersPriceId)
    {
        return $this->delete($this->getById($providersPriceId));
    }

}