define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Ui/js/form/element/abstract',
    'mage/url'
], function ($, ko, Component, quote, Abstract, url) {
    'use strict';


    return Abstract.extend({

//        selectedDepartment: ko.observable(''),
 //       selectedCity: ko.observable(''),
        selectedMethod: ko.computed(function () {
            var method = quote.shippingMethod();
            var selectedMethod = method != null ? method.carrier_code+'_'+method.method_code : null;
            console.log('selectedMethod carrier_postcode_international '+selectedMethod);
            return selectedMethod;
        }, this)
    });
});