define([], function () {
    "use strict";
    return function (AbstractInput) {
        return AbstractInput.extend({
            initialize: function () {
                //console.log(this);
                //call parent function
                this._super();
                //load only if inputMask is specified
                //console.log(this);
                if (this.inputMask) {
                    //alert('mask telefon');
                    require([
                        'Magento_Ui/js/lib/view/utils/async',
                        'Webfresh_Checkout/js/vendor/imask/inputmask.min'
                    ], this.inputMaskHandle.bind(this));
                }

                return this;
            },
            inputMaskHandle: function ($) {
                //wait for element rendering
                $.async(
                    'input',
                    this,
                    this.afterElemenRender.bind(this)
                );
            },
            afterElemenRender: function (input) {
                new IMask(input, this.inputMask);
            }
        });
    };
});
