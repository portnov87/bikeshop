/*var config = {
    map: {
        '*': {}
    },
    paths: {
        "mask":        "Webfresh_Checkout/js/jquery.maskedinput.min"
    },
    shim: {
        "mask":  ["jquery"]
    }
};
*/

var config = {
    config: {
        mixins: {
            'Magento_Ui/js/form/element/abstract': {
                'Webfresh_Checkout/js/input-mask': true
            }
        }
    }
};