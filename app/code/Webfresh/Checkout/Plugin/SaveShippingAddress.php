<?php

namespace Webfresh\Checkout\Plugin;

class SaveShippingAddress
{

    protected $_request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->_request = $request;
    }

    public function beforeSaveAddressInformation($subject, $cartId, $addressInformation)
    {

        $shippingAddress = $addressInformation->getShippingAddress();

        $billingAddress = $addressInformation->getBillingAddress();
        $extension_attributes = $shippingAddress->getExtensionAttributes();
        $city = $extension_attributes->getCarrierCity();


        $address = '-';
        //echo 'getShippingMethod = '.$shippingAddress->getShippingMethod();
        //carrier_address
        //newposhtahome
        switch ($addressInformation->getShippingMethodCode()) {

            case 'newposhta':
                $_address = $extension_attributes->getCarrierDepartment();
                if ($_address != '') {
                    $address = $_address;
                }

                break;
            case 'flatrate':
            case 'newposhtahome':
                $_address = $extension_attributes->getCarrierAddress();
                if ($_address != '') {
                    $address = $_address;
                }
                break;
            case 'ukrposhta':
                $_address = $extension_attributes->getCarrierAddressUkrposhta();//CarrierAddress();
                if ($_address != '') {
                    $address = $_address;
                }
                break;
            case 'justin':
                $city = $extension_attributes->getCarrierCityJustin();
                //carrier_city_justin
                //carrier_department_justin

                $_address = $extension_attributes->getCarrierDepartmentJustin();

                if ($_address != '') {
                    $address = $_address;
                }
                break;
            case 'international':
                $_address = $extension_attributes->getCarrierPostcodeInternational();

                if ($_address != '') {
                    $address = $_address;
                }
                break;

        }

        if ($city != '') {
            $shippingAddress->setCity($city);
            $billingAddress->setCity($city);
        } else {
            $shippingAddress->setCity('Киев');
            $billingAddress->setCity('Киев');
        }

        $shippingAddress->setStreet([$address]);
        $billingAddress->setStreet([$address]);

//        echo 'getShippingMethodCode = '.$addressInformation->getShippingMethodCode().' $address'.$address.' '.$city."<br/>\r\n\r\n";
//        die();
        $shippingAddress->setEmail($addressInformation->getEmail());

        return null;
    }
}