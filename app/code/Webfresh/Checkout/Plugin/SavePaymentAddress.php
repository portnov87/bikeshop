<?php

namespace Webfresh\Checkout\Plugin;

class SavePaymentAddress
{


    public function beforeSavePaymentInformationn($subject, $cartId, $paymentMethod,$addressInformation)
    {

        $addressInformation->setCountryId('UA');
        $addressInformation->setRegion('-');
        $extension_attributes=$addressInformation->getExtensionAttributes();
        $city='Киев';
        $street='-';

        if ($extension_attributes){
            if ($extension_attributes->getCarrierCity()!='') {
                $city=$extension_attributes->getCarrierCity();
                //$addressInformation->setCity($extension_attributes->getCarrierCity());
            }
            if ($extension_attributes->getCarrierDepartment()!='') {
                $street = $extension_attributes->getCarrierDepartment();
            }
            if ($extension_attributes->getCarrierAddress()!='') {
                $street = $extension_attributes->getCarrierAddress();
            }
            if ($extension_attributes->getCarrierAddressUkrposhta()!='') {
                $street = $extension_attributes->getCarrierAddressUkrposhta();
            }
            if ($extension_attributes->getCarrierDepartmentJustin()!='') {
                $street = $extension_attributes->getCarrierDepartmentJustin();
            }
            if ($extension_attributes->getCarrierPostcodeInternational()!='') {
                $street = $extension_attributes->getCarrierPostcodeInternational();
            }

            if ($extension_attributes->getCarrierCityJustin()!='') {
                $city = $extension_attributes->getCarrierCityJustin();
            }


        }

        $addressInformation->setCity($city);
        $addressInformation->setStreet([$street]);

        /*
        $billingAddress = $addressInformation->getBillingAddress();
        $extension_attributes = $billingAddress->getExtensionAttributes();

        $city = $extension_attributes->getCarrierCity();
        $address=$extension_attributes->getCarrierAddress();

        if ($city != '') {
            $billingAddress->setCity($city);
        }

        carrier_address


        $address = $extension_attributes->getCarrierDepartment();
        if ($address != '') {
            $billingAddress->setStreet([$address]);
        }

        $billingAddress->setEmail($addressInformation->getEmail());
*/

        return null;
    }
}