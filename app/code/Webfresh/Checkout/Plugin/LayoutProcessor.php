<?php
namespace Webfresh\Checkout\Plugin;

class LayoutProcessor
{
    public function afterProcess($subject, $jsLayout)
    {
//        echo '<pre>';
//        print_r($jsLayout);
//        echo '</pre>';
//        die();
        foreach ($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                 ['payment']['children']['payments-list']['children'] as &$child)
        {
            //$child['children']['form-fields']['children']['telephone']['validation'] = ['required-entry' => false];
            $child['children']['form-fields']['children']['street']['validation'] = ['required-entry' => false];

            $child['children']['form-fields']['children']['street']['children'][0]['validation'] = [
                'required-entry' => false,
                'min_text_length'=>0
            ];
            unset($child['children']['form-fields']['children']['postcode']);


            //unset($child['children']['form-fields']['children']['street']);
        }

        return $jsLayout;
    }
}