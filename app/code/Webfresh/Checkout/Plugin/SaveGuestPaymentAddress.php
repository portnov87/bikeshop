<?php

namespace Webfresh\Checkout\Plugin;

class SaveGuestPaymentAddress
{


    public function beforeSavePaymentInformation($subject,$cartId, $email, $paymentMethod,$addressInformation)
    {

        $addressInformation->setCountryId('UA');
        $addressInformation->setRegion('-');
        $extension_attributes=$addressInformation->getExtensionAttributes();
        $city='Киев';
        $street='-';

        if ($extension_attributes){
            if ($extension_attributes->getCarrierCity()!='') {
                $city=$extension_attributes->getCarrierCity();
                //$addressInformation->setCity($extension_attributes->getCarrierCity());
            }
            if ($extension_attributes->getCarrierDepartment()!='') {
                $street = $extension_attributes->getCarrierDepartment();
            }
            if ($extension_attributes->getCarrierAddress()!='') {
                $street = $extension_attributes->getCarrierAddress();
            }

            if ($extension_attributes->getCarrierAddressUkrposhta()!='') {
                $street = $extension_attributes->getCarrierAddressUkrposhta();
            }
            if ($extension_attributes->getCarrierDepartmentJustin()!='') {
                $street = $extension_attributes->getCarrierDepartmentJustin();
            }
            if ($extension_attributes->getCarrierPostcodeInternational()!='') {
                $street = $extension_attributes->getCarrierPostcodeInternational();
            }
            if ($extension_attributes->getCarrierCityJustin()!='') {
                $city = $extension_attributes->getCarrierCityJustin();
            }


        }

        $addressInformation->setCity($city);
        $addressInformation->setStreet([$street]);

        //$extension_attributes->getCarrierDepartment()]);
//        carrier_city
//        extension_attributes
        /*$billingAddress = $addressInformation;//->getBillingAddress();
        $extension_attributes = $addressInformation->getExtensionAttributes();

        $city = $extension_attributes->getCarrierCity();

        $addressInformation->setCity('terer1');
        $addressInformation->setStreet(['terer2']);
        $addressInformation->setRegion('-');
        $addressInformation->setCountryId('-');
        //message: "Пожалуйста, проверьте адрес плательщика. Пожалуйста, введите улицу. Пожалуйста, введите город. Пожалуйста, введите край/область."

        if ($city != '') {
            $billingAddress->setCity($city);
        }


        $address = $extension_attributes->getCarrierDepartment();
        if ($address != '') {
            $billingAddress->setStreet([$address]);
        }

        $billingAddress->setCountryId('UA');
        $billingAddress->setRegion('-');
        $billingAddress->setEmail($email);
*/

        return null;
    }
}