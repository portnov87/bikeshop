<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webfresh\Checkout\Model;

/**
 * Cash on delivery payment method model
 *
 * @method \Magento\Quote\Api\Data\PaymentMethodExtensionInterface getExtensionAttributes()
 */
class Cashondelivery extends \Magento\OfflinePayments\Model\Cashondelivery
{

    public function getSubtotal()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cartObj = $objectManager->get('\Magento\Checkout\Model\Cart');
        $subTotal = $cartObj->getQuote()->getSubtotal(); //Current Cart Subtotal
        return $subTotal;
    }
        /**
     * Check whether payment method can be used
     *
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (!$this->isActive($quote ? $quote->getStoreId() : null)) {
            return false;
        }

        $subTotal=$this->getSubtotal();
        if ($subTotal<300)
            return false;

        return parent::isAvailable($quote);
//
//
//        $checkResult = new DataObject();
//        $checkResult->setData('is_available', true);
//
//        // for future use in observers
//        $this->_eventManager->dispatch(
//            'payment_method_is_active',
//            [
//                'result' => $checkResult,
//                'method_instance' => $this,
//                'quote' => $quote
//            ]
//        );
//
//        return $checkResult->getData('is_available');
    }

}
