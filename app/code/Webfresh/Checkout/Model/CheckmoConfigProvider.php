<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webfresh\Checkout\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Escaper;
use Magento\Payment\Helper\Data as PaymentHelper;

class CheckmoConfigProvider extends \Magento\OfflinePayments\Model\CheckmoConfigProvider
{

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return $this->method->isAvailable() ? [
            'payment' => [
                'checkmo' => [
                    'mailingAddress' => $this->getMailingAddress(),
                    'payableTo' => $this->getPayableTo(),
                    'additionInfo'=>$this->getAdditionInfo(),
                ],
            ],
        ] : [];
    }
    public function getSubtotal()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cartObj = $objectManager->get('\Magento\Checkout\Model\Cart');
        $subTotal = $cartObj->getQuote()->getSubtotal(); //Current Cart Subtotal
        return $subTotal;
    }
    protected function getAdditionInfo()
    {

        $subTotal=$this->getSubtotal();
        if ($subTotal>=5000)
        {
            return 'Доступен с ограничениями (внесение 10% стоимости заказа на транспортные расходы)';
        }
        return false;
    }

}
