<?php

namespace Pmclain\OneClickCheckout\Model\QuoteBuilder;

use Magento\Customer\Model\Session;
use Magento\Quote\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\App\Request\Http;

class AddressBuilder
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AddressInterfaceFactory
     */
    protected $addressFactory;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;


    protected $request;
    /**
     * AddressBuilder constructor.
     * @param Session $session
     * @param AddressInterfaceFactory $addressFactory
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        Session $session,
        AddressInterfaceFactory $addressFactory,
        AddressRepositoryInterface $addressRepository,
        Http $request
    ) {
        $this->session = $session;
        $this->addressFactory = $addressFactory;
        $this->addressRepository = $addressRepository;
        $this->request = $request;
    }

    /**
     * @return \Magento\Quote\Model\Quote\Address
     */
    public function getShippingAddress()
    {
        /** @var \Magento\Quote\Model\Quote\Address $quoteAddress */
        $quoteAddress = $this->addressFactory->create();

        //$customerAddressId = $this->session->getCustomerDataObject()->getDefaultShipping();
        //$customerAddress = $this->addressRepository->getById($customerAddressId);
        $quoteAddress->setAddressType('shipping');
        $quoteAddress->setLastName('-');
        $quoteAddress->setFirstName('-');
        $quoteAddress->setCity('-');
        $quoteAddress->setStreet(['-']);
        $quoteAddress->setEmail('no-replay@bikeshop.com.ua');

        $params = $this->request->getParams();
        if (isset($params['phone_one_click']))
            $quoteAddress->setTelephone($params['phone_one_click']);
        else $quoteAddress->setTelephone('-');

        //$quoteAddress->importCustomerAddressData($customerAddress);

        return $quoteAddress;
    }

    /**
     * @return \Magento\Quote\Model\Quote\Address
     */
    public function getBillingAddress()
    {
        /** @var \Magento\Quote\Model\Quote\Address $quoteAddress */
        $quoteAddress = $this->addressFactory->create();

        //$customerAddressId = $this->session->getCustomerDataObject()->getDefaultBilling();
        //$customerAddress = $this->addressRepository->getById($customerAddressId);
        $quoteAddress->setAddressType('billing');
        $quoteAddress->setLastName('-');
        $quoteAddress->setFirstName('-');
        $quoteAddress->setCity('-');
        $quoteAddress->setStreet(['-']);
        $quoteAddress->setCountryId('UA');
        $quoteAddress->setPostcode('-');
        $quoteAddress->setEmail('no-replay@bikeshop.com.ua');


        $params = $this->request->getParams();
        if (isset($params['phone_one_click']))
            $quoteAddress->setTelephone($params['phone_one_click']);
        else $quoteAddress->setTelephone('-');

        //$quoteAddress->importCustomerAddressData($customerAddress);

        return $quoteAddress;
    }
}
