<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);


require_once('/home/zhopa/public_html/bikeshop/protect.inc.php');

require('/home/zhopa/public_html/bikeshop/includes/application_top.php');


$userData = array("username" => "apiuser", "password" => "123123123q");


$ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/integration/admin/token");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));

$token = curl_exec($ch);
if (ini_get('allow_url_fopen')) {
    echo '<pre>$token';
    print_r($token);
    echo '</pre>';
}





  $query = "SELECT * FROM customers";
            $customers_query = vam_db_query($query);

            while ($customerVam = vam_db_fetch_array($customers_query)) {


                $customeridVam = $customerVam['customer_id'];

                $addresses=[];

                $email=$customerVam['customers_email_address'];
                $query = "SELECT * FROM address_book WHERE customer_id='$customeridVam'";
                $address_query = vam_db_query($query);
                $phone=$customerVam['customers_telephone'];
                $phone=str_replace(['-',' ',')','(','+'],['','','','',''],$phone);
                while ($addressVam = vam_db_fetch_array($address_query)) {


                    if (count($addresses)==0)
                    {
                        $defaultBilling=true;
                    }else $defaultBilling=false;

                    $addressNew=[
                        "firstname" => $addressVam['entry_firstname'],
                        "lastname" => $addressVam['entry_lastname'],
                        "countryId" => "UA",
                        "street" => [
                            $addressVam['entry_street_address']
                        ],
                        "telephone" => $phone,
                        "fax" => "-",
                        "postcode" => "-",
                        "city" => $addressVam['entry_city'],
                        "defaultBilling" => $defaultBilling
                    ];

                    $addresses[]=$addressNew;
                }
                
                $magentoCustomer=findCustomerByEmail($token,$email);



                $data = [
                    'email' =>$email,
                    'firstname' => $customerVam['customers_firstname'],
                    'lastname' => $customerVam['customers_lastname'],
                    'created' => $customerVam['customers_date_added'],
                    'telephone' => $phone,
                    'vamshop_id' => $customerVam['customers_id'],
                    'addresses' => $addresses
                ];

                if ($magentoCustomer){
                    $customerId=$magentoCustomer['id'];
                    $customerUpdate = updateCustomer($token,$customerId,$data);

                }else {

                    $customerNew = addCustomer($token, $data);
                }

            }


            function findCustomerByVamshop($token,$vamshopid){


                $requestUrl = 'http://dev.bikeshop.com.ua/index.php/rest/V1/customers/search?searchCriteria[filter_groups][0][filters][0][field]=vamshop_id&searchCriteria[filter_groups][0][filters][0][value]=%25'.$customername.'%25&searchCriteria[filter_groups][0][filters][0][condition_type]=like';

                $ch = curl_init($requestUrl);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

                $result = curl_exec($ch);

                $result = json_decode($result, 1);
                echo '<pre>findCustomerByVamshop';
                print_r($result);
                echo '</pre>';


            }


            function findCustomerByEmail($token,$email){

                $requestUrl = 'http://dev.bikeshop.com.ua/index.php/rest/V1/customers/search?searchCriteria[filter_groups][0][filters][0][field]=email&searchCriteria[filter_groups][0][filters][0][value]='.$email.'&searchCriteria[filter_groups][0][filters][0][condition_type]=like';

                $ch = curl_init($requestUrl);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

                $result = curl_exec($ch);

                $result = json_decode($result, 1);
                echo '<pre>findCustomerByEmail';
                print_r($result);
                echo '</pre>';


            }

            function updateCustomer($token,$customerId,$data)
            {
                $customerData = [
                    'customer' => [
                        'id' => $customerId,
                        "email" => $data['email'],
                        "firstname" => $data['firstname'],
                        "lastname" => $data['lastname'],
                        "storeId" => 1,
                        "websiteId" => 1,
                        "groupId" => 1,
                        "customAttributes" => [
                            [
                                "attributeCode" => "telephone",
                                "value" => $data['telephone']
                            ],
                            [
                                "attributeCode" => "vamshop_id",
                                "value" => $data['vamshop_id']
                            ]
                        ],
                        "addresses" => $data['addresses']
                    ]
                ];

                $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/customers/".$customerId);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

                $result = curl_exec($ch);

                $result = json_decode($result, 1);
                echo '<pre>updateCustomer';
                print_r($result);
                echo '</pre>';
            }

            function addCustomer($token,$data)
            {


                $customerData = [
                    'customer' => [
                        "email" => $data['email'],
                        "firstname" => $data['firstname'],
                        "lastname" => $data['lastname'],
                        "storeId" => 1,
                        'created_at'=>$data['created'],
                        "websiteId" => 1,
                        "groupId" => 1,
                        "customAttributes" => [
                                [
                                    "attributeCode" => "telephone",
                                    "value" => $data['telephone']
                                ],
                                [
                                    "attributeCode" => "vamshop_id",
                                    "value" => $data['vamshop_id']
                                ]
                            ],
                        "addresses" => $data['addresses']//$addresses
                            /*[
                                [
                                    "firstname" => "ABC",
                                    "lastname" => "XYZ",
                                    "countryId" => "MY",
                                    "street" => [
                                        "No 545 Jalan balau 27/13",
                                        "sinar link Taman rinting"
                                    ],
                                    "company" => "Mahnazfood",
                                    "telephone" => "04040404040404",
                                    "fax" => "01010101101010101",
                                    "postcode" => "81750",
                                    "city" => "Masai",
                                    "defaultBilling" => true
                                ],
                                [
                                    "firstname" => "ABC",
                                    "lastname" => "XYZ",
                                    "countryId" => "MY",
                                    "street" => [
                                        "Colony#3 Block#126/F",
                                        "Address 2 "
                                    ],
                                    "company" => "Ashtech",
                                    "telephone" => "090078601",
                                    "fax" => "00000000000000",
                                    "postcode" => "45000",
                                    "city" => "Lahore",
                                    "defaultShipping" => true
                                ]
                            ]
                    ]*/,
                    "password" => "xMr43Zv3M4"
                ];

                $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/customers");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

                $result = curl_exec($ch);

                $result = json_decode($result, 1);
                echo '<pre>addCustomer';
                print_r($result);
                echo '</pre>';


            }
die();


$name = 'Детские велосипеды 2-4 года';

echo "test after product\r\n\r\n";


function processCategory($categories, $all_categories, $token)
{
    $catsMagento = [];
    foreach ($categories as $cat) {
        $parent_name = $cat['parent_category_name'];
        $categories_name = $cat['categories_name'];
        $parent_id = $cat['parent_id'];

        if ($parent_name != '')
            $name = $parent_name . '_' . $categories_name;
        else
            $name = $categories_name;

        $category_magento = 2;
        $cat = find_category_by_name($name, $categories_name, $all_categories['children_data'], $token);//['children_data']


        if (isset($cat['id'])) {
            $category_magento = $cat['id'];

        } else {


            $query = "SELECT 
                            cat_desc.categories_name,
                            cat_desc.categories_id,
                            categories.parent_id,
                            categories_par.parent_id as parent2,
                            cat_desc_par.categories_name as parent2_name
                            FROM categories                          
                            LEFT JOIN categories_description cat_desc ON (categories.categories_id=cat_desc.categories_id)
                            
                            LEFT JOIN categories categories_par ON (categories_par.categories_id=categories.parent_id)
                            LEFT JOIN categories_description cat_desc_par ON (categories_par.categories_id=cat_desc_par.categories_id)
                              WHERE cat_desc.categories_id='" . $parent_id . "'
                              LIMIT 1
                            ";
            $category_parent_query = vam_db_query($query);
            $parent_name_1 = '';//$parent_id=false;
            //echo  '$query'.$query."\r\n";
            $parent_categories_id = false;
            while ($cat_parent_2 = vam_db_fetch_array($category_parent_query)) {

                $parent_name_1 = $cat_parent_2['parent2_name'];
                $parent_id_2 = $cat_parent_2['parent2'];
                $parent_categories_id = $cat_parent_2['categories_id'];
            }

            if ($parent_name_1 != '')
                $cat_name = $parent_name_1 . '_' . $parent_name;
            else {
                $cat_name = $parent_name;
            }


            // echo 'cat name='.$cat_name.' parentname='.$parent_name." \r\n";
            $cat = find_category_by_name($cat_name, $parent_name, $all_categories['children_data'], $token);//,$parent_id,$parent_name);//['children_data']
            if ($cat) {

                echo 'add_new_category ' . $categories_name . ' ' . $cat['id'] . "\r\n";
                $category_magento_object = add_new_category($categories_name, $cat['id'], $token);//, $all_categories['children_data'], $token);
                if ($category_magento_object->id) {
                    $category_magento = $category_magento_object->id;
                    $all_categories = all_categories($token);
                } else {
                    $category_magento = 2;
                }


            } else {
                echo '$parent_categories_id ' . $parent_categories_id . ' ' . $categories_name . "\r\n";
                $category_magento_object = add_new_category($categories_name, $parent_categories_id, $token);


                if ($category_magento_object->id) {
                    $category_magento = $category_magento_object->id;
                    $all_categories = all_categories($token);
                } else {
                    $category_magento = 2;
                }
            }

        }

        $catsMagento[$category_magento] = $category_magento;
    }
    return $catsMagento;

}

//
//function get_all_categories_vam($category_id,$categories=array())
//{
//
//    $query = "SELECT
//                            cat_desc.categories_name,
//                            cat_desc.categories_id,
//                            categories.parent_id
//                            FROM categories
//                            LEFT JOIN categories_description cat_desc ON (categories.categories_id=cat_desc.categories_id)
//                            WHERE cat_desc.categories_id='" . $category_id . "'
//                            LIMIT 1";
//
//    $category_query = vam_db_query($query);
//
//    while ($cat_parent = vam_db_fetch_array($category_query))
//    {
//        if ($cat_parent['parent_id']!=0) {
//            echo '<pre>';
//            print_r($cat_parent);
//            echo '</pre>';
//
//        }else{
//            $categories[$category_id]=['name'=>$cat_parent['categories_name']];
//        }
//    }
//
//    return $categories;
//
//}

/*
$attributes_query = vam_db_query("SELECT c.*,ce.specification_name
                            FROM specifications c
                             lEFT JOIN specification_description ce ON (c.specifications_id=ce.specifications_id)
                            ");



$manufacturers_query = vam_db_query("SELECT man.manufacturers_name
                            FROM manufacturers man                            
                            ");

if (ob_get_level() == 0) ob_start();
$options_for_save = [];
while ($manufacturer = vam_db_fetch_array($manufacturers_query)) {
    $name = $manufacturer['manufacturers_name'];
    $options_for_save[] = ['label' => $name, 'storeLabels' => [
        [
            'storeId' => 0,
            'label' => $name
        ],
        [
            'storeId' => 1,
            'label' => $name
        ]
    ]];
}
//save_manufacturer($options_for_save, $token);

while ($attributes = vam_db_fetch_array($attributes_query)) {
    if ($attributes['specifications_id'] != '1') {
        $i++;
        //if ($i>210){

        $attr_set_id = 4;
        $group_id = 36;
        $specification_group_id = $attributes['specification_group_id'];





        $attribute_set_id = 4;
        $sql = "SELECT cg.specification_group_name
                            FROM specification_groups cg                             
                             WHERE cg.specification_group_id='" . $specification_group_id . "'
                            ";
        //LEFT JOIN specification_groups_to_categories gr_cat ON (spec_gr.specification_group_id=gr_cat.specification_group_id)
        //WHERE gr_cat.categories_id='" . $category_id . "'
        $group_query = vam_db_query($sql);
        //echo '$sql'.$sql;

        while ($group = vam_db_fetch_array($group_query)) {
            $name = $group['specification_group_name'];

            $attr_set_id = getMagentoAttributeSets($name, $token);
        }




        $group_id = find_exist_group('Спецификации', $attr_set_id, $token);
        //echo '$group_id'.$group_id;
        if (!$group_id) {
            if ($attr_set_id != 4) {

                $group_id = create_group_attr($attr_set_id, $token);
                // echo '$group_id' . $group_id;
                // die();

            }
        }
        $attr_name = $attributes['specification_name'];
        $code = str2url($attr_name);
        $code = str_replace('-', '_', $code);
        $exist_code = find_attribute_by_code($code, $token);
        //$exist_code_set=find_attribute_by_code_set($code,$attr_set_id, $token);
        //echo 'ex'.$exist_code_set;die();
        if ($attr_name != 'Производитель') {


            $options = getOptions($attributes['specifications_id']);
            $options_for_save = [];
            foreach ($options as $opt) {
                if ($opt['specification_value'] != '') {
                    $options_for_save[] = ['label' => $opt['specification_value'], 'storeLabels' => [
                        [
                            'storeId' => 0,
                            'label' => $opt['specification_value']
                        ],
                        [
                            'storeId' => 1,
                            'label' => $opt['specification_value']
                        ]
                    ]];
                }
            }

            $attribute_code = create_attribute($attr_name, $options_for_save, $code, $attr_set_id, $token);
            if ($attribute_code)//$attribute_id)
            {
                //$attribute_id
                assignattribute($attr_set_id, $attribute_code, $group_id, $token);
                echo '$attr_set_id ' . $attribute_code.' $attr_set_id='. $attr_set_id . "\r\n";
            }
            ob_flush();
            flush();
            sleep(1);
            if ($i >= 240) break;
        }
        // }
    }
}


function save_manufacturer($options_for_save, $token)
{

    $attr_name = 'Manufacturer';
    $code = 'manufacturer';
    $find_attribute = find_attribute($code, $token);
    if ($find_attribute) {
        return update_attribute($attr_name, $options_for_save, $code, $token, $find_attribute);
    }
    return false;
}


die();*/
$sql = "SELECT pr.products_id, 
            pr.products_model,
                              pr.products_price,
                              pr.products_image,
                              pr.manufacturers_id,
                              man.manufacturers_name,
                              pr.products_status,
                              prd.products_name,
                              prd.products_description,
                              prd.products_short_description,
                              category_desc.categories_id,
                              category.parent_id,
                              category_desc.categories_name
                            FROM products pr
                              LEFT JOIN products_description prd ON (pr.products_id = prd.products_id)
                              LEFT JOIN products_to_categories cat_prd ON (pr.products_id = cat_prd.products_id)
                              LEFT JOIN categories category ON (cat_prd.categories_id = category.categories_id)
                              LEFT JOIN categories_description category_desc ON (cat_prd.categories_id = category_desc.categories_id)
                              LEFT JOIN manufacturers man ON (pr.manufacturers_id=man.manufacturers_id)
                              WHERE products_status=1 AND pr.products_id=11054724
                              GROUP BY pr.products_id
                              ORDER BY pr.products_date_added DESC, pr.products_image ASC                           
                            ";
$products_query = vam_db_query($sql);
//LIMIT 43300,5000
//2742,

//AND prd.products_name='Кепка FOX FLEX 45 FLEXFIT HAT [BLK/WHT] , S/M'
//OR pr.products_model='SAD-32-53'
//LIMIT 2740,5000


/*while ($product = vam_db_fetch_array($products_query)) {
    echo '<pre>$product';
    print_R($product);
    echo '</pre>';
}
echo "test after product\r\n\r\n";*/

//LIMIT 35000,5000

//LIMIT 35000,5000
//LIMIT 7400,5000
//LIMIT 2000,5000
//categories_id
//LEFT JOIN products_to_categories cat_prd ON (pr.products_id = prd.products_id)
//LEFT JOIN products_description prd ON (pr.products_id = prd.products_id)

if (ob_get_level() == 0) ob_start();
$count_insert = $count_update = 0;
//
//echo $sql;
//die();

// manufacturer
$find_attribute_manuf = find_attribute('manufacturer', $token);
while ($product = vam_db_fetch_array($products_query)) {

    //die();

    $product_id = $product['products_id'];
    $parent_id = $product['parent_id'];
    $categories_id = $product['categories_id'];


    $categoriesVam = [];


    $sql = "SELECT 
                              category_desc.categories_id,
                              category.parent_id,
                              category_desc.categories_name,
                              category_desc_par.categories_name as parent_category_name
                            FROM products_to_categories cat_prd 
                              LEFT JOIN categories category ON (cat_prd.categories_id = category.categories_id)
                              LEFT JOIN categories_description category_desc ON (cat_prd.categories_id = category_desc.categories_id)
                              
                              LEFT JOIN categories as category_par ON (category.parent_id = category_par.categories_id)
                              LEFT JOIN categories_description category_desc_par ON (category_par.categories_id = category_desc_par.categories_id)
                              WHERE cat_prd.products_id='$product_id'
                              GROUP BY cat_prd.categories_id                              
                            ";
    // echo $sql;
//    die();
    $category_query = vam_db_query($sql);

    while ($__category = vam_db_fetch_array($category_query)) {
//        echo '<pre>$__category';
//        print_R($__category);
//        echo '</pre>';
//        die();
        $categories = vam_get_category_path($__category['categories_id']);

        $categoriesVam[$__category['categories_id']] = $categories;
//

//        [
//            'parent'=>$__category['parent_id'],
//            'parent_name'=>$__category['parent_category_name'],
//            'categories_name'=>$__category['categories_name']
//        ];


    }
    echo '<pre>$categoriesVam';
    print_R($categoriesVam);
    echo '</pre>';
    die();

    $manufacturer = $product['manufacturers_name'];
    $categories_name = $product['categories_name'];
    $image = $product['products_image'];

//    foreach ($categoriesVam as $catVam){
//        $parent_id=$catVam['parent_id'];
//    }
//    $category_parent_query = vam_db_query("SELECT
//                            cat_desc.categories_name
//                            FROM categories_description cat_desc
//                              WHERE  cat_desc.categories_id='" . $parent_id . "'
//                              LIMIT 1
//                            ");
//    $parent_name = '';//$parent_id=false;
//    while ($cat_parent = vam_db_fetch_array($category_parent_query)) {
//        $parent_name = $cat_parent['categories_name'];
//    }

    $categoriesMagento = processCategory($categoriesVam, $all_categories, $token);

    //echo '$category_magento111 ' . $category_magento . "\r\n";
    //echo "\r\n";
    echo '<pre>';
    print_r($categoriesMagento);
    echo '</pre>';

    echo '<pre>';
    print_r($product);
    echo '</pre>';

    echo '<pre>$categoriesVam';
    print_r($categoriesVam);
    echo '</pre>';

    die();

    if ($product['products_model'] != '') $sku = $product['products_model'];
    else $sku = $product['products_id'];
    $price = $product['products_price'];
    //$product['products_price']
    $price = str_replace(',', '.', $price);
    $price = round($price, 4);
    $product_name = $product['products_name'];
    /*$productByName = find_product_by_name($product_name);
    if ($productByName){

    }*/
    $data = [
        'sku' => $sku,
        'category_magento' => $category_magento,
        'product_url' => $category_magento,
        'products_price' => $price,
        'products_name' => $product_name,
        'products_status' => $product['products_status'],
        'products_description' => $product['products_description'],
        'products_short_description' => $product['products_short_description'],
    ];

    $attributes_for_save = [];
    $attributes_query = vam_db_query("SELECT prod_spec.specifications_id, prod_spec.specification, spec_desc.specification_name
                            FROM products_specifications prod_spec
                            LEFT JOIN specification_description spec_desc ON (prod_spec.specifications_id =spec_desc.specifications_id)
                              WHERE prod_spec.products_id='" . $product_id . "'
                            ");
    $da = 0;
    $spec_attr = [];


    while ($attribute = vam_db_fetch_array($attributes_query)) {
        $attr_name = $attribute['specification_name'];
        $specification = $attribute['specification'];
        $code = str2url($attr_name);
        $code = str_replace('-', '_', $code);

        $find_attribute = find_attribute($code, $token);

        if ($find_attribute) {
            if (count($find_attribute['options']) > 0) {

                foreach ($find_attribute['options'] as $option) {
                    if ($option['label'] == $specification) {
                        // echo $code.' '.$option['label'] .' '.$specification.' '.$option['value'].'<br/>';

                        if ($find_attribute['frontend_input'] == 'select') {
                            $_val = $option['value'];
                        } else {
                            $_val = [$option['value']];
                        }
                        $attributes_for_save[] = [
                            "attribute_code" => $code,
                            "value" => $_val
                        ];
                    }
                }
            }
            /*echo '<pre>$find_attribute';
            print_r($find_attribute);
            echo '</pre>';*/
        }
        $da++;
        $_spec_attr = [];
        $_spec_attr['option'] = $attribute;
        $_spec_attr['find_attr'] = $find_attribute;
        $spec_attr[] = $_spec_attr;

    }


    if ($find_attribute_manuf) {
        $exist_manufacturer = false;
        if (count($find_attribute_manuf['options']) > 0) {

            foreach ($find_attribute_manuf['options'] as $option) {
                if ($option['label'] == $manufacturer) {
                    $attributes_for_save[] = [
                        "attribute_code" => 'manufacturer',
                        "value" => $option['value']
                    ];
                    $exist_manufacturer = true;
                }
            }
        }


        if (!$exist_manufacturer) {

        }
    }
    $data['attributes_for_save'] = $attributes_for_save;

    /*
        if ($sku == '3203317 3980') {
            echo '<pre>' . $categories_id;
            print_r($data);
            echo '</pre>';
            die();
        }
      */  /*echo '<pre>' . $categories_id;
    print_r($data);
    echo '</pre>';
    echo '<pre>' . $categories_id;
    print_r($product);
    echo '</pre>';*/

    $category_id_vam = $categories_id;
    if ($new_product = create_product($data, $category_id_vam, $token)) {

        assign_product_category($sku, $category_magento, $token);
        $product_magento_id = $new_product;//->id;

        echo $product_magento_id . ' ' . $product_id . ' ' . $sku . ' ' . $image . ' ' . $token . "\r\n\r\n";
        $sku = urlencode($sku);
        remove_media($sku, $token);
        save_media($product_magento_id, $product_id, $sku, $image, $token);
        $count_insert++;

        echo ' count= ' . $count_insert . "\r\n";
    } else {
        /*echo '<pre>$spec_attr';
        print_r($spec_attr);
        echo '</pre>';*/
    }


    //break;
    ob_flush();
    flush();
    //sleep(1);
}


echo 'insert' . $count_insert;
ob_end_flush();

function getExtension1($filename)
{
    $img = explode(".", $filename);
    return end($img);
}


function downloadimg($url)
{
    if ($ch = curl_init()) {
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 5.1; rv:34.0) Gecko/20100101 Firefox/34.0'
            ]
        ];
        curl_setopt_array($ch, $options);
        $file = curl_exec($ch);

    } else {
        die("\r\nerror curl\r\n");
    }
    return $file;


}


function curl_get_contents($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);

    /*curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'User-Agent' => 'Mozilla/5.0 (Windows NT 5.1; rv:34.0) Gecko/20100101 Firefox/34.0'
    ]);*/
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

function save_img_magento($product_magento_id, $product_id, $sku, $image, $token, $type_m = "image")
{
    global $_objectManager;
    if ($image != '') {
        $path_media = '/home/zhopa/public_html/bikeshop/images/product_images/original_images/';
        //https://bikeshop.com.ua/images/product_images/original_images/';


        $url = '/V1/products/' . $sku . '/media';
        $img = $path_media . $image;
        echo 'imgpath ' . $img . "\r\n\r\n";
        echo '$url=' . $url . "\r\n\r\n";
        $imagedata = file_get_contents($img);
        if ($imagedata) {
            $size = getimagesize($img);

            $type = $size['mime'];
            $name = $image;
            $encoded_data = base64_encode($imagedata);
            $data = [
                'entry' => [
                    "mediaType" => $type_m,
                    "types" => ["image", "small_image", "thumbnail"],
                    "disabled" => false,
                    "content" => [
                        "base64_encoded_data" => $encoded_data,
                        "type" => $type,
                        "name" => $name
                    ]
                ]
            ];
            //


            /*
            $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest" . $url);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

            $result = curl_exec($ch);
            $result_json = json_decode($result, true);

            echo '<pre>$result_json';
            print_r($result);
            echo '</pre>';*/


            //die();
            return $result_json;
        }
        /*echo '<pre>$imagedata';
        print_r($imagedata);
        echo '</pre>';

        //echo '$imagedata '.$imagedata;
        die();*/
    }


    return false;
}

function remove_media($sku, $token)
{

    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/" . $sku . "/media");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
    $result = curl_exec($ch);
    $result_json = json_decode($result);
    foreach ($result_json as $img) {
        $idmedia = $img->id;
        //echo  "http://dev.bikeshop.com.ua/index.php/rest/V1/products/".$sku."/media/".$idmedia;die();
        $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/" . $sku . "/media/" . $idmedia);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        $result_del = json_decode($result);
        echo '<pre>$result_del';
        print_r($result_del);
        echo '</pre>';
    }
    $product_id = false;

}

function save_media($product_magento_id, $product_id, $sku, $image_n, $token)
{
    $result_json = save_img_magento($product_magento_id, $product_id, $sku, $image_n, $token);//,"thumbnail");
    /* echo '<pre>$result_json_magento image' . $image_n;
     print_r($result_json);
     echo '</pre>';
 */
    // берём доп картинки
    $images_query = vam_db_query("SELECT primg.image_name
                            FROM products_images primg
                            WHERE products_id='" . $product_id . "'
                            ");
    while ($image = vam_db_fetch_array($images_query)) {
        if ($image_n != $image['image_name']) {
            $result_js = save_img_magento($product_magento_id, $product_id, $sku, $image['image_name'], $token);
            /*echo '<pre>$result_json_magento image2';
            print_r($result_js);
            echo '</pre>';*/
        }
    }
    //if (!$result_json) return false;
    //return true;

}

function find_attribute($attribute_code, $token)
{

    //$url='/V1/products/attribute-sets/'.$attr_set_id.'/attributes';
    $url = '/V1/products/attributes/' . $attribute_code;
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest" . $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");


    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result, true);

    if (isset($result_json)) {
        if (!isset($result_json['message'])) {

            return $result_json;
        }
    }

    //die();
    //if (isset($result_json->attribute_group_id)) $group_id=$result_json->attribute_group_id;

    return false;

}

function assign_product_category($sku, $category_id, $token)
{
    $data = [
        'product_link' => [
            'sku' => $sku,
            'position' => 0,
            'category_id' => $category_id
        ]
    ];

    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/categories/" . $category_id . "/products");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);

}

function get_category($category_id, $token)
{
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/categories/" . $category_id);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_array = json_decode($result, true);
    return $result_array;
}


function getCategoryVam()
{
    $cats_import = [];
    $categories_bid = $categories_disable = array();
    $categories_query = vam_db_query("SELECT c.categories_id, 
      c.parent_id, cd.categories_name, c.categories_image,
                              c.categories_status,
                              cd.categories_description,
                              cd.categories_meta_title,
                              cd.categories_meta_description
                            FROM " . TABLE_CATEGORIES . " c
                              LEFT JOIN " . TABLE_CATEGORIES_DESCRIPTION . " cd ON (c.categories_id = cd.categories_id)
                            WHERE cd.language_id='1'
                            ORDER BY c.categories_id");
    while ($categories = vam_db_fetch_array($categories_query)) {

        //if ($categories['parent_id']=='0') {
        $cats_import[$categories['categories_id']] = [
            'id' => $categories['categories_id'],
            'name' => $categories['categories_name'],
            'parent_id' => $categories['parent_id'],
            'image' => $categories['categories_image'],
            'status' => $categories['categories_status'],
            'description' => $categories['categories_description'],
            'meta_title' => $categories['categories_meta_title'],
            'meta_description' => $categories['categories_meta_description'],
            'children' => []
        ];
        //$cats_import=tree($cats_import,$categories['parent_id']);

        //$cats_import[$categories['categories_id']]['children']
        /* }else{
             if (isset())
         }*/
    }

    return form_tree($cats_import);
}

function form_tree($mess)
{
    if (!is_array($mess)) {
        return false;
    }
    $tree = array();
    foreach ($mess as $value) {
        $tree[$value['parent_id']][] = $value;
    }
    return $tree;
}

function all_categories($token)
{
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/categories?rootCategoryId=2");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_array = json_decode($result, true);
    return $result_array;//with_parent($result_array,$parent);

}


function add_new_category($name, $parent_id, $token)
{
    /*echo '$name = ' . $name . ' $name_original = ' . $name_original . "\r\n\r\n";
           echo '<pre>$categories vam';
           print_r(getCategoryVam());
           echo '</pre>';*/

    //echo '$parent_id = ' . $parent_id.' $parent_name='.$parent_name."\r\n";
    echo '$name = ' . $name . ' $parent_id = ' . $parent_id . "\r\n\r\n";
    /*echo '<pre>$categories';
    print_r($categories);
    echo '</pre>';
    echo '<pre>$result';
    print_r($result);
    echo '</pre>';*/
    //die();
    $post_data = [];
    //$parent=$cats_import[$parent_id]['new_id'];
    $post_data['category'] = [
        'name' => $name,
        'parent_id' => $parent_id,//$cat['parent_id'],
        'is_active' => 1,
        'include_in_menu' => 1,
        //'custom_attributes' => $custom_attributes
    ];
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/categories");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);
    return $result_json;
}

function find_category_by_name($name, $name_original, $categories, $token, $result = false)
{
    /*echo '<pre>$categories';
    print_r($categories);
    echo '</pre>';
    */
    foreach ($categories as $category) {

        if ($category['name'] == $name_original) {
            $parent_category = get_category($category['parent_id'], $token);

            $parent_name = '';
            $new_name = $category['name'];
            if ($parent_category) {
                if ($parent_category['name'] != 'Default Category') {
                    $parent_name = $parent_category['name'];
                    $new_name = $parent_name . '_' . $category['name'];
                }
            }

            if ($new_name == $name) {

                return $category;
            }
        } elseif (count($category['children_data']) > 0) {
            $result = find_category_by_name($name, $name_original, $category['children_data'], $token, $result);


            if ($result) break;
        }
    }

    if (!$result) {


    }
    return $result;
}

function update_product($data, $sku, $token)
{

}

function find_product($sku, $token)
{
    $sku = urlencode($sku);
    //echo  'sku= '.$sku."\r\n";

    //'49243 5050');//3203317 3980
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/" . $sku);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
    $result = curl_exec($ch);
    $result_json = json_decode($result);
    $product_id = false;

    /*echo  '<pre>$result_json find product '.$sku;
    print_r($result_json);
    echo '</pre>';
    *///die();
    //return false;
    if (!isset($result_json->message)) {
        //echo 'id'.$result_json->id;
        return (int)$result_json->id;
    }
    return false;


}

function getattribute_set_id($category_id, $token)
{

    $attribute_set_id = 4;
    $sql = "SELECT spec_gr.specification_group_name
                            FROM specification_groups spec_gr 
                            LEFT JOIN specification_groups_to_categories gr_cat ON (spec_gr.specification_group_id=gr_cat.specification_group_id)
                             WHERE gr_cat.categories_id='" . $category_id . "'";
    $group_query = vam_db_query($sql);
    //echo '$sql'.$sql;

    while ($group = vam_db_fetch_array($group_query)) {
        if ($group) {
            if (isset($group['specification_group_name'])) {
                $name = $group['specification_group_name'];
                $attribute_set_id = getMagentoAttributeSets($name, $token);
                return $attribute_set_id;
                //echo $name.' '.$attribute_sets;
                //die();
                //$attribute_set_id = $group['magento_attr_set'];
            }
        }
    }

    //die();
    return $attribute_set_id;

}

function getMagentoAttributeSets($name, $token)
{
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/attribute-sets/sets/list?searchCriteria[filter_groups][0][filters][0][field]=attribute_set_name&searchCriteria[filter_groups][0][filters][0][condition_type]=eq&searchCriteria[filter_groups][0][filters][0][value]=someAttributeSet");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = [];
    $data['searchCriteria'] = [];
    //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);
    /*echo '<pre>$result_json attributesets';
    print_r($result_json);
    echo '</pre>';*/
    if ($result_json->items) {
        foreach ($result_json->items as $item) {
            if ($item->attribute_set_name == $name) {
                return $item->attribute_set_id;
            }
        }

        $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/attribute-sets");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = [];
        $data['attributeSet']['attribute_set_name'] = $name;
        $data['attributeSet']['entity_type_id'] = 4;
        $data['attributeSet']['sort_order'] = 3;
        $data['skeletonId'] = 4;
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

        $result = curl_exec($ch);
        $result_json = json_decode($result);
        /*echo  '<pre>$result_json attr create attr_set';
        print_r($result_json);
        echo '</pre>';*/

        if ($result_json->attribute_set_id) {
            return $result_json->attribute_set_id;
        }
        //products/attribute-sets

    }
    return 4;//$result_json;

}

function create_product($data, $category_id_vam, $token)
{
    $sku = $data['sku'];
    $find_product = find_product($sku, $token);
    //if ($find_product) {
    //  echo 'exist product' . $data['sku'] . "<br/>\r\n";
    //return false;
    //}

    $attribute_set_id = 4;
    $attribute_set_id = getattribute_set_id($category_id_vam, $token);
    $customAttributes = [];
    $customAttributes = $data['attributes_for_save'];
    $customAttributes[] =
        [
            "attribute_code" => "description",
            "value" => $data['products_description']
        ];
    $customAttributes[] = [
        "attribute_code" => "short_description",
        "value" => $data['products_short_description']
    ];

    if ($attribute_set_id == 28)
        $weight = 15;
    else $weight = 0.9;


    //$sku=urlencode($sku);
    $name = $data['products_name'];
    $data = [
        'product' => [
            'sku' => $sku,//$data['sku'],
            'name' => $data['products_name'],
            'type_id' => "simple",
            'visibility' => 4,
            'weight' => $weight,
            'attribute_set_id' => $attribute_set_id,
            'price' => $data['products_price'],//round($data['products_price'],2),
            'status' => $data['products_status'],
            'extensionAttributes' => [
                'stockItem' => [
                    'isInStock' => true,
                    'qty' => 1000
                ]
            ],
            "customAttributes" => $customAttributes
        ]
    ];

    /*$product_exist=getProductMagentoBySku($sku,$token);
    echo '<pre>$data';
    print_r($data);
    echo '</pre>';
    echo '<pre>$product_exist';
    print_r($product_exist);
    echo '</pre>';
    return;*/
    if ($find_product) {
        //echo 'update ' . $sku . ' ' . $name . "<br/>\r\n";
        //return false;
        $sku = urlencode($sku);

        $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/$sku");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

        $result = curl_exec($ch);
        $result_json = json_decode($result);

        echo 'update ' . $sku . ' ' . $name . "<br/>\r\n";
        $product_id = false;
        /*echo '<pre>';
        print_r($result_json);
        echo '</pre>';
        return false;*/
        if (isset($result_json->id)) $product_id = $result_json->id;
        return $product_id;
    } else {
        //echo "\r\n\r\nbegin create\r\n";
        //die();
        //return  false;
        $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

        $result = curl_exec($ch);
        $result_json = json_decode($result);

        echo 'created ' . $sku . ' ' . $name . "<br/>\r\n";
        $product_id = false;
        //echo "\r\nend create\r\n\r\n";
        if (isset($result_json->id))
            $product_id = $result_json->id;
        /*else {
            echo '<pre>';
            print_r($result_json);
            echo '</pre>';
            echo '<pre>$data';
            print_r($data);
            echo '</pre>';

        }*/


        /*echo '<pre>$data';
        print_r($data);
        echo '</pre>';
        echo 'created ' . $sku . ' ' . $name . "<br/>\r\n";
return false;*/
        return $product_id;
    }
}

function getProductMagentoBySku($sku, $data, $token)
{
    $url = "http://dev.bikeshop.com.ua/index.php/rest/V1/products/$sku";
    $ch = curl_init($url);//"http://dev.bikeshop.com.ua/index.php/rest/V1/products/$sku");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);
    return $result_json;
}

function updateProductMagentoBySku($sku, $data, $token)
{
    $url = "http://dev.bikeshop.com.ua/index.php/rest/V1/products/$sku";
    $ch = curl_init($url);//"http://dev.bikeshop.com.ua/index.php/rest/V1/products/$sku");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);
    return $result_json;
}

function assignattribute($attr_set_id, $attribute_code, $group_id, $token)
{
    $data = ['attributeSetId' => $attr_set_id, 'attributeGroupId' => $group_id, 'sortOrder' => 0, 'attributeCode' => $attribute_code];
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/attribute-sets/attributes");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);

    return $result_json;
}

function getOptions($specification_id)
{
    $result = [];
    $options_query = vam_db_query("SELECT c.*,ce.specification_value
                            FROM specification_values c
                             lEFT JOIN specification_values_description ce 
                             ON (c.specification_values_id=ce.specification_values_id)
                             WHERE c.specifications_id='" . $specification_id . "'
                            ");
    while ($options = vam_db_fetch_array($options_query)) {
        $result[] = $options;
    }
    return $result;
}

function find_exist_group($name, $attr_set_id, $token)
{
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/attribute-sets/groups/list?searchCriteria[filterGroups][0][filters][0][field]=attribute_group_name&searchCriteria[filterGroups][0][filters][0][value]=$name&searchCriteria[filterGroups][0][filters][1][field]=attribute_set_id&searchCriteria[filterGroups][0][filters][1][value]=$attr_set_id");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);

    if (isset($result_json->items)) {
        foreach ($result_json->items as $item) {
            if (($item->attribute_group_name == $name) && ($item->attribute_set_id == $attr_set_id))
                return $item->attribute_group_id;
        }
    }

    //die();
    //if (isset($result_json->attribute_group_id)) $group_id=$result_json->attribute_group_id;

    return false;
}

function create_group_attr($attr_set_id, $token)
{
    $group_id = false;
    $data = ['group' => [
        'attributeGroupName' => 'Спецификации',
        'attributeSetId' => $attr_set_id,
    ]];
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/attribute-sets/groups");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);

    if (isset($result_json->attribute_group_id)) $group_id = $result_json->attribute_group_id;

    return $group_id;
}

function find_attribute_by_code_set($attribute_code, $attr_set_id, $token)
{


    $url = '/V1/products/attribute-sets/' . $attr_set_id . '/attributes';
    ///V1/products/attributes?searchCriteria[filterGroups][0][filters][0][field]=attribute_code&searchCriteria[filterGroups][0][filters][0][value]='.$attribute_code;
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest" . $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);

    if (isset($result_json)) {
        foreach ($result_json as $item) {

            if (strpos($item->attribute_code, $attribute_code) !== false) {//$item->attribute_code==$attribute_code){

                return true;
            }

        }
    }

    //die();

    return false;
}

function find_attribute_by_code($attribute_code, $token)
{
    $url = '/V1/products/attributes?searchCriteria[filterGroups][0][filters][0][field]=attribute_code&searchCriteria[filterGroups][0][filters][0][value]=' . $attribute_code;
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest" . $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);

    if (isset($result_json->items)) {
        foreach ($result_json->items as $item) {
            if ($item->attribute_code == $attribute_code)
                return $attribute_code . time();
        }
    }


    return false;
}


function update_attribute($attr_name, $options_for_save, $code, $token, $attribute_find)
{
    $data = $attribute_find;
    $exists_options = $data['options'];//attribute->
    $options = [];//$options_for_save;

    foreach ($exists_options as $opt) {
        if (trim($opt['label']) != '') {
            $options[$opt['label']] = ['label' => $opt['label'], 'value' => $opt['value'],
                'storeLabels' => [
                    [
                        'storeId' => 0,
                        'label' => $opt['label']
                    ],
                    [
                        'storeId' => 1,
                        'label' => $opt['label']
                    ]
                ]];
        }

    }

    foreach ($options_for_save as $opt_for_save) {

        if (trim($opt_for_save['label']) != '')//&&($opt['label']!=$opt_for_save['label']))
        {
            if (!isset($options[$opt_for_save['label']])) {
                $options[] = ['label' => $opt_for_save['label'],
                    'storeLabels' => [
                        [
                            'storeId' => 0,
                            'label' => $opt_for_save['label']
                        ],
                        [
                            'storeId' => 1,
                            'label' => $opt_for_save['label']
                        ]
                    ]];
            }
        }
    }

    $options = array_values($options);

    $data['options'] = $options;
    $data_new = ['attribute' => $data];
    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/attributes/" . $code);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_new));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);

    if (isset($result_json->attribute_code)) $attribute_code = $result_json->attribute_code;
    return $attribute_code;

}

function create_attribute($attr_name, $options_for_save, $code, $attr_set_id, $token)
{


    $attribute_code = false;
    $attribute_find = find_attribute($code, $token);
    if ($attribute_find) {
        return update_attribute($attr_name, $options_for_save, $code, $token, $attribute_find);
        //return $attribute_code;
    }

    //return false;
    $data = ['attribute' => [
        'attribute_code' => $code,
        'is_visible' => true,
        'is_filterable' => true,
        'is_filterable_in_search' => true,
        'is_visible_on_front' => true,
        'used_in_product_listing' => true,
        'is_required' => false,
        'frontend_input' => 'multiselect',
        'frontend_labels' => [
            [
                'store_id' => 0,
                'label' => $attr_name,
            ],
            [
                'store_id' => 1,
                'label' => $attr_name,
            ]
        ],
        'options' => $options_for_save
    ]];


    $ch = curl_init("http://dev.bikeshop.com.ua/index.php/rest/V1/products/attributes");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

    $result = curl_exec($ch);
    $result_json = json_decode($result);
    echo "<pre>create_attribute";
    print_r($result_json);
    echo "</pre>";
    if (isset($result_json->attribute_code)) $attribute_code = $result_json->attribute_code;
    return $attribute_code;

}

function rus2translit($string)
{
    $converter = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v',
        'г' => 'g', 'д' => 'd', 'е' => 'e',
        'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'y', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u',
        'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
        'ь' => '', 'ы' => 'y', 'ъ' => '',
        'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

        'А' => 'A', 'Б' => 'B', 'В' => 'V',
        'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
        'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
        'И' => 'I', 'Й' => 'Y', 'К' => 'K',
        'Л' => 'L', 'М' => 'M', 'Н' => 'N',
        'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U',
        'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
        'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
        'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

function str2url($str)
{
    // переводим в транслит
    $str = trim($str);
    $str = rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '_', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "_");
    return $str;
}










